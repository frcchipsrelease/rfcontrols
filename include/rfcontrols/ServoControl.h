/*******************************************************************************
 *
 * File: ServoControl.h
 *
 * Control of a single Servo or a double Servo
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "frc/Servo.h"

#include "rfcontrols/PeriodicControl.h"

#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"

/*******************************************************************************
 *
 * Instances of this class can be created to control a single Servo.
 * 
 * This class is designed to be created from an XML element.
 * 
 ******************************************************************************/
class ServoControl : public PeriodicControl, public OIObserver
{
	public:
		enum {CMD_STEP_UP, CMD_STEP_DOWN, CMD_TOGGLE, CMD_STATE, CMD_SETPOINT_UP, CMD_SETPOINT_MID, CMD_SETPOINT_DOWN, CMD_VELOCITY};

		ServoControl(std::string control_name, tinyxml2::XMLElement *xml);
		~ServoControl(void);

  		void controlInit(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void publish(void);

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

	private:
		frc::Servo	*m_servo;

		float m_servo_target_position;
		float m_servo_step_size;
		float m_servo_max_up_position;
		float m_servo_max_down_position;
		float m_servo_setpoint_up_position;
		float m_servo_setpoint_mid_position;
		float m_servo_setpoint_down_position;

		float m_servo_target_velocity;
};

