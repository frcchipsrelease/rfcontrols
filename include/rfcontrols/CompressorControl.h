/*******************************************************************************
 *
 * File: CompressorControl.h
 *
 * This class is to control a compressor using a relay and pressure switch
 * without using the logic built into the PCM. Concerns about drawing too
 * much current through the PCM and fuse shared with the radio lead to
 * the creation of this class.
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "gsutilities/tinyxml2.h"

#include "rfcontrols/PeriodicControl.h"

#include "rfhardware/LimitSwitch.h"

#include "frc/Relay.h"

/*******************************************************************************
 *
 * This control is for a compressor that is connected with a pressure switch
 * and relay.
 *
 * XML Example:
 *   <control type="compressor" name="Compressor" [period="0.1"] [priority="0"]>
 *  	<digital_input   [module="1"] [port="1"] [...] />
 *  	<relay           [module="1"] [port="1"] [fuse="-1"] [...] />
 *   </compressor>
 *
 ******************************************************************************/
class CompressorControl: public PeriodicControl
{
	public:
		CompressorControl(std::string name, tinyxml2::XMLElement *xml = NULL);
		~CompressorControl();

		void publish(void);

	protected:
		void controlInit();
		void updateConfig();

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();

		void doPeriodic();

	private:
		frc::Relay *compressor_relay;
		LimitSwitch *compressor_switch;

		bool compressor_switch_invert;
		bool compressor_on;
};
