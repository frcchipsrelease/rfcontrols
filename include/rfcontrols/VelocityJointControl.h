/*******************************************************************************
 * 
 * File: VelocityJointControl.h
 * 
 * This is an example of how a motor with an internal encoder can be used to
 * control a flywheel with velocity control.
 *
 * Written by:
 *  FRC Team 324, Chips
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"

#include "rfutilities/MacroStep.h"

#include "rfhardware/Motor.h"

#include "gsutilities/DataLogger.h"
#include "gsutilities/tinyxml2.h"

/*******************************************************************************	
 * 
 * Create an instance of a velocity joint and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *
 * <control type="velocity_joint" [name="default"] [period="0.050"] [closed_loop="false"] 
 *     [percent_min="-1.0"] [percent_max="1.0"] [velocity_min="-50.0"] [velocity_max="50.0"] >
 * 
 *     <motor ... min_control="-1.0" max_control="1.0" >
 *     	   <pid ... />
 *     </motor>
 * 
 *     <oi name="closedLoop"          device="hid1" chan="8" closed="false" />
 *     <oi name="analogPower"         device="hid1" chan="1" scale="1.0" deadband="0.0" />
 *     <oi name="latchedPower"        device="hid1" chan="0" value="0.0" />
 *     <oi name="momentaryPower"      device="hid1" chan="1" value="0.1" />
 *     <oi name="adjustPower"         device="hid1" chan="2" value="0.1" />
 *     <oi name="analogVelocity"      device="hid1" chan="1" scale="1.0" deadband="0.0" />
 *     <oi name="latchedVelocity"     device="hid1" chan="0" value="0.0" />
 *     <oi name="momentaryVelocity"   device="hid1" chan="1" value="0.1" />
 *     <oi name="adjustVelocity"      device="hid1" chan="2" value="0.1" />
 * 
 * </control>
 ******************************************************************************/
class VelocityJointControl : public PeriodicControl
{
	public:
		VelocityJointControl(std::string control_name, tinyxml2::XMLElement *xml);
		~VelocityJointControl(void);

  		void controlInit(void);
		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();

		void doPeriodic();

		void publish(void);
		void logHeaders(void);
		void logData(void);

		void setClosedLoop(bool closed, bool pressed=true);
		bool isClosedLoop(void);

		void setPower(float value);
		void setLatchedPower(float value, bool pressed=true);
		void setMomentaryPower(float value, bool pressed=true);
		void adjustPower(float value, bool pressed=true);

		void setVelocity(float value);
		float getVelocity(void);
		bool isAtTarget(float tolerance);

		void setLatchedVelocity(float value, bool pressed=true);
		void setMomentaryVelocity(float value, bool pressed=true);
		void adjustVelocity(float value, bool pressed=true);

	private:
		bool hardware_is_ready;          // set to true if all needed components have been created

		bool is_closed_loop;    // initially read from config, can be changed by user input 

		Motor	*motor;
		float motor_percent_min;
		float motor_percent_max;
		float motor_percent_target;  // what the user asked for
		float motor_percent_command; // what this class told the motor
		float motor_percent_actual;  // what the motor is reporting

		float motor_velocity_min;
		float motor_velocity_max;
		float motor_velocity_target;
		float motor_velocity_command;
		float motor_velocity_actual;
		float motor_velocity_raw;

		gsu::DataLogger data_logger;
};

/*******************************************************************************	
 *
 * This Macro Step will set the analog power for an instance of the velocity 
 * joint control class.
 *
 * <step name="slow_up_power" ctrl_name="lift" type="SetPower" power="0.15" >
 *
 *   power = desired motor power: min = -1.0, max = +1.0, default = 0.0
 *
 * NOTE:    Setting the power when the joint is in closed loop mode will have
 *          no impact, this Macro Step only has meaning when the joint is
 *          in open loop mode.
 *
 * WARNING: The power will ramian at the specified value until something else
 *          sets it to a different value (like another macro step).
 *
 ******************************************************************************/
class MSVelJointSetPower : public MacroStepSequence
{
	public:
		MSVelJointSetPower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		VelocityJointControl *parent_control;
		float power;
};

/*******************************************************************************	
 *
 * This Macro Step will set the target position for an instance of the velocity 
 * joint control class.
 * 
 * <step name="score_position" ctrl_name="lift" type="SetVelocity" 
 *     position="23.0" tolerance="2.0" wait="true">
 * 
 *   position = the desired position of the control, default = 0.0
 * 
 *   tolerance = the allowable tolerance of the position, default 0.1
 * 
 *   wait = true if the macro should wait for the control to reach the target, false
 *          if it does not need to wait
 * 
 * NOTE:    Setting the position when the joint is in open loop mode will have
 *          no impact, this Macro Step only has meaning when the joint is
 *          in closed loop mode.
 * 
 ******************************************************************************/
class MSVelJointSetVelocity : public MacroStepSequence
{
	public:
		MSVelJointSetVelocity(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		VelocityJointControl *parent_control;
		float target_velocity;
		float target_tolerance;
		bool wait_for_position;
};
