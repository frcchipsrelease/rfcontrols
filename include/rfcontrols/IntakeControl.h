/*******************************************************************************
 *
 * File: IntakeControl.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"

#include "rfhardware/Motor.h"
#include "rfhardware/LimitSwitch.h"

/*******************************************************************************	
 * 
 * Create an instance of an open loop motor control and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="motor" [name="unnamed"] [max_cmd_delta="0.25"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *
 *      [<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<digital_input name="upper_limit" port="1" [normally_open="false"] />]
 * 		[<digital_input name="lower_limit" port="2" [normally_open="false"] />]
 *      [<oi name="analog"    	device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" 	device="pilot" chan="2" step="0.1" [invert="false"]/>]
 *      [<oi name="decrement" 	device="pilot" chan="3" step="0.1" [invert="false"]/>]
 *      [<oi name="stop"      	device="pilot" chan="4" [invert="false"]/>]
 *      [<oi name="momentary_a"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_b"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_c"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_d"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class IntakeControl : public PeriodicControl, public OIObserver
{
	public:
		enum {CMD_FORWARD=0, CMD_BACKWARD, CMD_STOP, CMD_STOW, CMD_DEPLOY };
		
		IntakeControl(std::string control_name, tinyxml2::XMLElement *xml);
		~IntakeControl(void);

  		void controlInit(void);
		void updateConfig(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void setDeployPower(float power);
		void setRollerPower(float power);

		void publish(void);

	private:		
        Motor* m_intake_motor;
        Motor* m_deploy_motor;

        LimitSwitch* m_stow_limit_sw;
		LimitSwitch* m_deploy_limit_sw;

		bool m_stow_limit_pressed;
		bool m_deploy_limit_pressed;

		float motor_min_control; // sdr what
		float motor_max_control;

        uint32_t m_intake_max_current;
        uint32_t m_deploy_max_current;

		float motor_forward_power;
		float motor_backward_power;

		float m_stow;
		float m_deploy;

		float motor_max_cmd_delta; // sdr what

		float intake_target_power;
		float deploy_target_power;
		float intake_command_power;
		float deploy_command_power;
};

/*******************************************************************************
 *
 * This Macro Step sets the power Motor PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn it off unless the
 *          provided value for power is 0.0.
 *
 *  Example XML:
 *
 *	<step name="lift_up" control="motor_1" type="SetPower"	power="0.5" >
 *  	<connect type="next" step="lift_up_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSIntakeRollerPower : public MacroStepSequence
{
	public:
    	MSIntakeRollerPower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		IntakeControl *m_parent_control;

		float m_intake_roller_power;
};


/*******************************************************************************
 *
 * This Macro Step sets the power Motor PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn it off unless the
 *          provided value for power is 0.0.
 *
 *  Example XML:
 *
 *	<step name="lift_up" control="motor_1" type="SetPower"	power="0.5" >
 *  	<connect type="next" step="lift_up_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSIntakeDeploy : public MacroStepSequence
{
	public:
	MSIntakeDeploy(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		IntakeControl *m_parent_control;

		double deploy_duration;
		double deploy_end_time;
		float deploy_power;

};
