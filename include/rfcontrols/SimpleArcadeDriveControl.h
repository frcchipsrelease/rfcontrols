/*******************************************************************************
 *
 * File:SimpleArcadeDriveControl.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <string>

#include "gsutilities/tinyxml2.h"

#include "frc/Solenoid.h"
#include "frc/BuiltInAccelerometer.h"

#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "rfutilities/DataLogger.h"

#include "rfhardware/Motor.h"

#include "rfcontrols/PeriodicControl.h"

/*******************************************************************************
 *
 *  Provides a PeriodicControl implementation for driving the base of a robot
 *  using an Arcade-like driver interface, that is a forward and turn command
 *  are used to make the robot move.
 *  
 * This class is designed to be created from an XML element.
 *  
 * 
 ******************************************************************************/
class SimpleArcadeDriveControl : public PeriodicControl, public OIObserver
{
	public:
		enum
		{
			CMD_FORWARD = 0, 	CMD_TURN,
			CMD_LOW_POWER_ON,	CMD_LOW_POWER_OFF, 	CMD_LOW_POWER_TOGGLE, 	CMD_LOW_POWER_STATE
	
		};

		SimpleArcadeDriveControl(std::string name, tinyxml2::XMLElement *xml);
		~SimpleArcadeDriveControl(void);

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void controlInit();

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();

		void doPeriodic();
		void publish();

	private:
        void logFileInit(std::string phase);
        void logFileAppend(void);
		void setLimelightState(bool value);

        DataLogger *m_data_logger;

        Motor* fl_drive_motor;
        Motor* fr_drive_motor;
        Motor* bl_drive_motor;
        Motor* br_drive_motor;
		
		float fl_drive_motor_cmd;
		float fr_drive_motor_cmd;
		float bl_drive_motor_cmd;
		float br_drive_motor_cmd;
		
		float trn_power;
		float fwd_power;

		float m_low_power_scale;
		bool m_low_power_active;
		
};


// =============================================================================
// =============================================================================

/*******************************************************************************
 *
 * This Macro Step sets the forward power, turn power of the
 * Drive Arcade PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn them off unless the
 *          provided values for forward, turn, turn the drive off.
 *
 *  Example XML:
 *
 *	<step name="drive_1" control="drive" type="DrivePower"
 *			forward="0.5" turn="0.1>
 *  	<connect type="next" step="drive_wait"/>>
 *  </step>
 *
 ******************************************************************************/


class MSDriveSimpleArcadeDrivePower : public MacroStepSequence
{
	public:
		MSDriveSimpleArcadeDrivePower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		SimpleArcadeDriveControl *parent_control;

		float forward_power;
		float turn_power;
};
