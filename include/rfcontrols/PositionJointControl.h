/*******************************************************************************
 * 
 * File: PositionJointControl.h
 * 
 * This is an example of how to use a motor with an external encoder,
 * external limit switches, and external PIDs to do position control. 
 * 
 * Please keep in mind that most of the motor controllers we use can be 
 * configured to use external encoders, limit switches, and PID values
 * so this is at least as much a learning aid as it is useful. 
 * 
 * Writing code that uses generic interfaces allows us to switch hardware
 * without making changes to the software. It is harder to write software
 * like this but can save us when we need a quick hardware change. It also
 * helps us help others that don't use the same hardware that we do.
 * 
 * Some day I will try to write a version of this class that uses the motor to
 * do most of the closed loop work.
 * 
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"

#include "rfutilities/MacroStep.h"

#include "rfhardware/BasicPid.h"
#include "rfhardware/Motor.h"
#include "rfhardware/Encoder.h"
#include "rfhardware/LimitSwitch.h"

#include "gsutilities/tinyxml2.h"
#include "gsutilities/DataLogger.h"

#include <map>
#include <string>

/*******************************************************************************	
 * 
 * @class PositionJointControl
 * 
 * A class that controls a position control joint -- that is a joint with
 * a Motor, an Encoder, a trajectory planner, a PID, and optionally two Limit 
 * Switches
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 * 
 * 
 * <control type="position_joint" [name="default"] [period="0.050"] [use_absolute_angles="false"]>
 *     <motor ... min_control="-1.0" max_control="1.0" />
 *     <encoder ... />
 *     <pid ... />
 * 
 *     <setpoints>
 *         <setpoint name="sp_name_0" position="0.0" />
 *         <setpoint name="sp_name_1" position="90.0" />
 *         ...
 *         <setpoint name="sp_name_n" position="127.89" />
 *     </setpoints>
 * 
 *     <oi name="analogPower"      device="hid1" chan="2" scale="1.0" deadband="0.0" />
 *     <oi name="latchPower"       device="hid1" chan="3" value="0.1" />
 *     <oi name="momentaryPower"   device="hid1" chan="3" value="0.1" />
 *     <oi name="closedLoop"       device="hid1" chan="3" closed="false" />
 *     <oi name="nudgePositionPov" device="hid1" chan="3" step="15.0" />
 *     <oi name="selectSetpoint"   device="hid1" chan="3" setpoint="center" />
 * 
 * </control>
 *
 ******************************************************************************/
class PositionJointControl : public PeriodicControl, public OIObserver
{
	public:		
		static const uint8_t NUM_SETPOINTS = 10;

		PositionJointControl(std::string control_name, tinyxml2::XMLElement *xml);
		~PositionJointControl(void);

  		void controlInit(void);
		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setClosedLoop(bool closed, bool pressed=true);
		bool isClosedLoop(void);

		void setPower(float value);
		void setLatchedPower(float value, bool pressed=true);
		void setMomentaryPower(float value, bool pressed=true);
		void adjustPower(float value, bool pressed=true);

		void setPosition(float position);
		float getPosition(void);
		bool isAtTarget(float tolerance);

		void nudgePositionPov(float value, int direction);
		void selectSetpoint(std::string sp_name, bool pressed=true);

		void logHeaders(void);
		void logData(void);
		void publish(void);

	private:
		bool is_closed_loop;

		Motor *motor;             // @TODO: motor should be in rfh namespace
		float motor_min_percent;
		float motor_max_percent;

		float motor_percent_target;  // what the user asked for
		float motor_percent_command; // what this class told the motor
		float motor_percent_actual;  // what the motor (motor_a) is reporting

		rfh::Encoder *encoder;
		float encoder_position_raw;
		float encoder_position_rotation;
		float encoder_position_target;
		float encoder_position_actual;

		BasicPid* pid;  // should probably be part of the rfh namespace

		LimitSwitch* upper_limit;
 		bool upper_limit_pressed;

		LimitSwitch* lower_limit;
		bool lower_limit_pressed;

		bool use_absolute_angles;
		bool hardware_is_ready;          // set to true if all needed components have been created

		gsu::DataLogger data_logger;

		std::map<std::string, float> setpoints;
};

#if 0
/*******************************************************************************	
 *
 * This Macro Step will set the analog power for an instance of the position 
 * joint control class.
 * 
 * <step name="slow_up_power" ctrl_name="lift" type="SetPower" power="0.15" >
 * 
 *   power = desired motor power: min = -1.0, max = +1.0, default = 0.0
 * 
 * NOTE:    Setting the power when the joint is in closed loop mode will have
 *          no impact, this Macro Step only has meaning when the joint is
 *          in open loop mode.
 *  
 * WARNING: The power will ramian at the specified value until something else
 *          sets it to a different value (like another macro step).
 * 
 ******************************************************************************/
class MSPosJointSetPower : public MacroStepSequence
{
	public:
		MSPosJointSetPower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		PositionJointControl *parent_control;
		float power;
};

/*******************************************************************************	
 *
 * This Macro Step will set the target position for an instance of the position 
 * joint control class.
 * 
 * <step name="score_position" ctrl_name="lift" type="SetPosition" 
 *     position="23.0" tolerance="2.0" wait="true">
 * 
 *   position = the desired position of the control, default = 0.0
 * 
 *   tolerance = the allowable tolerance of the position, default 0.1
 * 
 *   wait = true if the macro should wait for the control to reach the target, false
 *          if it does not need to wait
 * 
 * NOTE:    Setting the position when the joint is in open loop mode will have
 *          no impact, this Macro Step only has meaning when the joint is
 *          in closed loop mode.
 * 
 ******************************************************************************/
class MSPosJointSetPosition : public MacroStepSequence
{
	public:
		MSPosJointSetPosition(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		PositionJointControl *parent_control;
		float target_position;
		float target_tolerance;
		bool wait_for_position;
};

class MSPosJointLimelight : public MacroStepSequence
{
	public:
		MSPosJointLimelight(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		PositionJointControl *parent_control;
		// Fix Me. I know, I know. This isn't the right way. In a hurry.
		float RAD_TO_DEG = 57.2958;
		bool limelight_control;
		float distance_offset;  // This will be zero if you calibrate at the spot you want
		float target_position;  
		float target_tolerance;
		bool wait_for_position;
		double heightDifference;
		double calibratedDistance;
		gsu::PiecewiseLinear piecewiseCurves;
};
#endif