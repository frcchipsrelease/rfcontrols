/*******************************************************************************
 *
 * File: SolenoidControl.h
 *
 * Control of a single solenoid or a double solenoid
  *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <units/pressure.h>
#include <frc/Compressor.h>
#include "rfcontrols/PeriodicControl.h"

#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"

#include "frc/Solenoid.h"

/*******************************************************************************
 *
 * Instances of this class can be created to control either a single solenoid
 * or a double solenoid.  If specified, the second solenoid control will be
 * set to the opposite state of the first solenoid.
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="solenoid" [name="unnamed"] [period="0.1"] >
 * 		<solenoid [name="a"] module="1" port="1" />
 * 		[<solenoid [name="b"] module="1" port="2" />]
 *      [<oi name="on"     device="pilot" chan="1" [invert="false"]/>]
 *      [<oi name="off"    device="pilot" chan="2" [invert="false"]/>]
 *      [<oi name="toggle" device="pilot" chan="3" [invert="false"]/>]
 *      [<oi name="state"  device="oi"    chan="1" [invert="false"]/>]
 *  </control>
 * 
 ******************************************************************************/
class SolenoidControl : public PeriodicControl, public OIObserver
{
	public:
		enum {CMD_ON, CMD_OFF, CMD_TOGGLE, CMD_STATE};

		SolenoidControl(std::string control_name, tinyxml2::XMLElement *xml);
		~SolenoidControl(void);

  		void controlInit(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void publish(void);
		void setState(bool state);

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

	private:
		frc::Solenoid *solenoid_a;
		frc::Solenoid *solenoid_b;
		frc::Compressor *compressor;
		bool solenoid_state;
};

/*******************************************************************************
 *
 * This Macro Step sets the Solenoid State.
 *
 *
 ******************************************************************************/
class SolenoidControlMSSetState : public MacroStepSequence
{
	public:
		SolenoidControlMSSetState(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		SolenoidControl *parent_control;

		bool state;
};
