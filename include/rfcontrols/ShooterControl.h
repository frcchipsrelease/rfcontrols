/*******************************************************************************
 *
 * File: ShooterControl.h
 *  
 * Motor with Closed Loop control, feedback from a Pot and limit switches
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"

#include "rfhardware/Motor.h"
#include "rfhardware/ScaledAnalogInput.h"
#include "rfhardware/AbsPosSensor.h"
#include "rfhardware/BasicPid.h"

#include "rfutilities/RobotUtil.h"
#include "rfutilities/DataLogger.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "gsutilities/PiecewiseLinear.h"

/*******************************************************************************	
 * 
 * Create an instance of a shooter control with velocity joint srx utilities and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 * 
 *  <control type="ShooterControl" [name="unnamed"] [closed_loop="false"] [period="0.1"]
 *  		[setpoint0="0.0"] [setpoint1="0.0"] [setpoint2="0.0"] [setpoint3="0.0"] [setpoint4="0.0"]
 *  		[setpoint5="0.0"] [setpoint6="0.0"]  >
 *
 *  	<motor name="front_right" [type="CanTalon"] [port="2"] [control_period=”10”] [invert="false"]>
 *   		[<encoder [invert=”false”] [scale=”1.0”] />]
 *  		[<pid [kf=”0.001”] [kp=”0.0”] [ki=”0.0”] [kd=”0.0”] />]
 *		</motor>
 *
 *
 *      [<oi name="closed_loop_state"  device="switches" chan="1" [invert="false"]/>]
 *
 *      [<oi name="analog"    device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" step="0.1" device="pilot" chan="5" [invert="false"]/>]
 *      [<oi name="decrement" step="0.1" device="pilot" chan="7" [invert="false"]/>]
 *      [<oi name="stop"      device="pilot" chan="1" [invert="false"]/>]
 *
 *      [<oi name="setpoint_idx" device="pilot" chan="0" [scale="0.2222222"]/>
 *
 *      [<oi name="setpoint0" device="pilot" chan="2" [invert="false"]/>]
 *      [<oi name="setpoint1" device="pilot" chan="3" [invert="false"]/>]
 *      [<oi name="setpoint2" device="pilot" chan="3" [invert="false"]/>]   
 *
 *      [<oi name="momentary0" device="pilot" chan="5" ol_power="0.6" cl_step="0.1" [invert="false"]/>]
 *      [<oi name="momentary1" device="pilot" chan="7" ol_power="-0.6" cl_step="-0.1" [invert="false"]/>]
 *      [<oi name="momentary2" device="pilot" chan="6" ol_power="1.0" cl_step="0.2" [invert="false"]/>]
 *      [<oi name="momentary3" device="pilot" chan="8" ol_power="-1.0" cl_step="-0.2" [invert="false"]/>]
 *
 *  </control>
 *  
 *  Note: either pot or aps can be used, if both are specified the pot will be
 *        used.
 *
 ******************************************************************************/
class ShooterControl : public PeriodicControl, public OIObserver
{
	public:
		enum Command
		{
			CMD_CLOSED_LOOP_STATE = 0,
			CMD_MOMENTARY_0, CMD_MOMENTARY_1, CMD_MOMENTARY_2, CMD_MOMENTARY_3,
			CMD_SETPOINT_0, CMD_SETPOINT_1, CMD_SETPOINT_2, CMD_SETPOINT_3,
			CMD_SETPOINT_4, CMD_SETPOINT_5, CMD_SETPOINT_6, CMD_SETPOINT_7,
			CMD_STOP_FLYWHEEL, CMD_INCREMENT_FLYWHEEL, CMD_DECREMENT_FLYWHEEL,
			CMD_KICKER_FWD_STATE, CMD_KICKER_FORWARD, CMD_KICKER_BWD_STATE, CMD_KICKER_BACKWARD, CMD_KICKER_STOP, 
			CMD_FLASHLIGHT_ON, CMD_FLASHLIGHT_OFF, CMD_FLASHLIGHT_TOGGLE, CMD_FLASHLIGHT_STATE, CMD_LIMELIGHT_STATE,
		};


		static const uint8_t NUM_SETPOINTS = 8;
		static const uint8_t NUM_MOMENTARIES = 4;

		ShooterControl(std::string control_name, tinyxml2::XMLElement *xml);
		~ShooterControl(void);

  		void controlInit(void);
		void updateConfig(void);
		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void publish(void);

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void setPosition(float val);
		float getPostion(void);

		void setVelocity(float val);
		float getVelocity(void);

		void initLogFile(void);

		void setClosedLoop(bool closed);
		bool isClosedLoop(void);
		int8_t getSetpointIndex(std::string setpoint_name); // sdr does this go with setpoint_idx
		void applySetpoint(bool on, int idx);
		bool isAtTarget(float tolerance);

		
		void setLimelightState(bool value);

		void setFlywheelPower(float percent);
		void setKickerPower(float percent);

	private:		
		void applyMomentary(bool on, int idx);

		Motor	*m_flywheel_motor1;
		Motor	*m_flywheel_motor2;
		Motor	*m_kicker_motor;
	 	Limelight* m_limelight;

		frc::DigitalOutput *m_flashlight;

		DataLogger *m_teleop_log; 
		DataLogger *m_auton_log;
		DataLogger *m_active_log;

		bool m_flashlight_state;

		bool m_is_ready;
		bool m_closed_loop;
		bool is_limelight_control; 
				
		float m_max_velocity;
		float m_desired_acceleration; 
		float m_desired_deceleration;
		float m_initial_velocity;

		float flywheel_percent_step;
		float flywheel_velocity_step;

		float flywheel_raw_position;
		int32_t flywheel_raw_velocity;
		float flywheel_actual_velocity;

		float kicker_raw_position;
		int32_t kicker_raw_velocity;
		float kicker_actual_velocity;

		float flywheel_target_percent;
		float flywheel_target_velocity;
		float flywheel_command_percent;

		float kicker_target_percent;
		float kicker_command_percent;

		// sdr do i need num_setpoints and setpoint_index ? 
		uint8_t setpoint_index;
		uint8_t num_setpoints;
		
		float momentary_power[NUM_MOMENTARIES];
		float momentary_step[NUM_MOMENTARIES];

		float m_setpoint_velocity[NUM_SETPOINTS];
		float m_setpoint_percent[NUM_SETPOINTS];
		std::string m_setpoint_name[NUM_SETPOINTS];

	
		gsu::PiecewiseLinear pwl01;
		std::vector<double> m_distanceFromTarget;
		std::vector<double> m_mappedMotorVelocity;

		float m_targetHeight;
		float m_limelightHeight;
		float m_heightDifference;
		float m_calibratedDistance;
		float m_distanceOffset;
		double m_target_visible;
		double m_targetOffsetAngle_Horizontal;
		double m_targetOffsetAngle_Vertical;
		double m_targetArea;
		double m_targetSkew;

		
		float min_position;   // read from config, limits target positions
		float max_position;   // read from config, limits target positions

		float raw_position;      // logged and published for debugging and tuning
		float actual_position;   // logged and published for debugging and tuning
		float raw_velocity;      // logged and published for debugging and tuning
		float actual_velocity;   // logged and published for debugging and tuning

		float target_velocity;  // updated based on user inputs, only used in closed loop

};

class MSShooter : public MacroStepSequence
{
	public:
		MSShooter(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ShooterControl *m_parent_control;
		float flywheel_target_percent;
		float kicker_target_percent;
		int setpoint_index;
		bool m_closed_loop;


		static const uint8_t NUM_SETPOINTS = 7;
		std::string m_setpoint_name[NUM_SETPOINTS];

		uint8_t num_setpoints;
		float m_setpoint_velocity[NUM_SETPOINTS];
		bool m_wait;
};

class MSShooterFlywheelPower : public MacroStepSequence
{
	public:
		MSShooterFlywheelPower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ShooterControl *m_parent_control;
		float m_flywheel_percent;
};

class MSShooterKickerPower : public MacroStepSequence
{
	public:
		MSShooterKickerPower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		ShooterControl *m_parent_control;
		float m_kicker_percent;
};