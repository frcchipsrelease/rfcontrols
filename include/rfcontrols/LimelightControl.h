/*******************************************************************************
 * 
 * File: Limelight.h
 * 
 * Motor with Closed Loop control, feedback from a Pot and limit switches
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"

#include "rfhardware/Motor.h"
#include "rfhardware/ScaledAnalogInput.h"
#include "rfhardware/AbsPosSensor.h"
#include "rfhardware/BasicPid.h"

#include "rfutilities/RobotUtil.h"
#include "rfutilities/DataLogger.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "rfhardware/Limelight.h"

/*******************************************************************************	
 * 
 * Create an instance of a limelight and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *
 ******************************************************************************/
class LimelightControl : public PeriodicControl, public OIObserver
{
	public:
		enum Command
		{
			CMD_CLOSED_LOOP_STATE = 0,
			CMD_TARGET_DRIVE_STATE,
			CMD_MOVE_ANALOG,
			CMD_NUDGE_UP,
			CMD_NUDGE_DOWN,
			CMD_SETPOINT_0,
			CMD_SETPOINT_1,
			CMD_SETPOINT_2,
			CMD_SETPOINT_3,
			CMD_SETPOINT_4,
			CMD_SETPOINT_5,
			CMD_SETPOINT_6,
			CMD_SETPOINT_7,
			CMD_SETPOINT_8,
			CMD_SETPOINT_9,
			CMD_SETPOINT_IDX, 
			CMD_SETPOINT_ANALOG_IDX
		};

		static const uint8_t NUM_SETPOINTS = 10;

		LimelightControl(std::string control_name, tinyxml2::XMLElement *xml);
		~LimelightControl(void);

  		void controlInit(void);
		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void publish(void);

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void setPosition(float val);
		float getPostion(void);

		void initLogFile(void);

		void setClosedLoop(bool closed);
		void setTargetDrive(bool target);//:))
		bool isClosedLoop(void);
		bool isTargetDrive(void); //:))
		int8_t getSetpointIndex(std::string setpoint_name);
		void applySetpoint(bool on, int idx);
		bool isAtTarget(float tolerance);

	private:
	 	Limelight* m_limelight;
		Motor	*motor;
		DataLogger      *pjs_log;

		bool is_ready;          // set to true if all needed components have been created
		bool is_closed_loop;    // initially read from config, can be changed by user input 
		bool is_target_drive;   // Drive to target using limelight
		TrapezoidProfile3 trajectory_profile;
		float trajectory_max_velocity;     // read from config, max target velocity in closed loop
		float trajectory_acceleration;     // read from config, target acceleration in closed loop
		float trajectory_deceleration;     // read from config, target deceleration in closed loop
        float trajectory_target_position;  // == commanded_position
		float trajectory_target_velocity;  // == calculated velocity based on accel, decel, and max vel

		float raw_position;      // logged and published for debugging and tuning
		float actual_position;   // logged and published for debugging and tuning
		float raw_velocity;      // logged and published for debugging and tuning
		float actual_velocity;   // logged and published for debugging and tuning

		float target_power;   // updated based on user inputs, only used in open loop
		float command_power;  // always retrieved from motor, logged and published for debugging and tuning

		float target_position;  // updated based on user inputs, only used in closed loop

		float min_position;   // read from config, limits target positions
		float max_position;   // read from config, limits target positions

		std::string setpoint_name[NUM_SETPOINTS];
		float setpoint_position[NUM_SETPOINTS];
        bool setpoint_defined[NUM_SETPOINTS];

		float analog_idx_step_size;  // read from config, used to convert analog input into a setpoint index
		float analog_idx_step_zero;

		float analog_delta_scale;  // read from config, used to set analog_delta_step for closed loop
		float analog_delta_step;   // set based on analog_delta_scal and analog input value, adjusts position each update

		float kpDistance; // Proportional control constant for distance. For use in target drive mode. Set value in the constructor //:))

		float nudge_up_power;    // read from config, used to set target power in open loop
		float nudge_down_power;  // read from config, used to set target power in open loop

		float nudge_up_step;     // read from config, used to adjust target position in closed loop
		float nudge_down_step;   // read from config, used to adjust target position in closed loop

		double m_target_visible;
		double m_targetOffsetAngle_Horizontal;
		double m_targetOffsetAngle_Vertical; //distance_error, as the lime light docs call it.
		double m_targetArea;
		double m_targetSkew;
		double m_target_drive_power; // the product of m_targetOffsetAngle_Vertical and kpDistance //:))
		};

/*******************************************************************************	
 *
 * This Macro Step will set the analog power for an instance of the position 
 * joint control class.
 * 
 * <step name="slow_up_power" ctrl_name="lift" type="SetPower" power="0.15" >
 * 
 *   power = desired motor power: min = -1.0, max = +1.0, default = 0.0
 * 
 * NOTE:    Setting the power when the joint is in closed loop mode will have
 *          no impact, this Macro Step only has meaning when the joint is
 *          in open loop mode.
 *  
 * WARNING: The power will ramian at the specified value until something else
 *          sets it to a different value (like another macro step).
 * 
 ******************************************************************************/
class MSSomething : public MacroStepSequence
{
	public:
		MSSomething(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		LimelightControl *parent_control;
		float power;
};

/*******************************************************************************	
 *
 * This Macro Step will set the target position for an instance of the position 
 * joint control class.
 * 
 * <step name="score_position" ctrl_name="lift" type="SetPosition" 
 *     position="23.0" tolerance="2.0" wait="true">
 * 
 *   position = the desired position of the control, default = 0.0
 * 
 *   tolerance = the allowable tolerance of the position, default 0.1
 * 
 *   wait = true if the macro should wait for the control to reach the target, false
 *          if it does not need to wait
 * 
 * NOTE:    Setting the position when the joint is in open loop mode will have
 *          no impact, this Macro Step only has meaning when the joint is
 *          in closed loop mode.
 * 
 ******************************************************************************/


