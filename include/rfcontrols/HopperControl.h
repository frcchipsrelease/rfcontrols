/*******************************************************************************
 *
 * File: HopperControl.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "rfutilities/DataLogger.h"

#include "rfhardware/Motor.h"
#include "rfhardware/LimitSwitch.h"

/*******************************************************************************	
 * 
 * Create an instance of a two open loop motors to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="motor" [name="unnamed"] [max_cmd_delta="0.25"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *
 *      [<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<motor [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 * 		[<digital_input name="upper_limit" port="1" [normally_open="false"] />]
 * 		[<digital_input name="lower_limit" port="2" [normally_open="false"] />]
 *      [<oi name="analog"    	device="pilot" chan="1" [scale="1.0"|invert="false"]/>]
 *      [<oi name="increment" 	device="pilot" chan="2" step="0.1" [invert="false"]/>]
 *      [<oi name="decrement" 	device="pilot" chan="3" step="0.1" [invert="false"]/>]
 *      [<oi name="stop"      	device="pilot" chan="4" [invert="false"]/>]
 *      [<oi name="momentary_a"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_b"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_c"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *      [<oi name="momentary_d"	device="pilot" chan="4" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class HopperControl : public PeriodicControl, public OIObserver
{
	public:
		enum {CMD_FORWARD=0, CMD_BACKWARD, CMD_STOP };
		
		HopperControl(std::string control_name, tinyxml2::XMLElement *xml);
		~HopperControl(void);

  		void controlInit(void);
		void updateConfig(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void initLogFile(void);
		void publish(void);

		void setPower(float vertical_percent, float horizontal_percent);

	private:		
        Motor* m_vertical_motor;
        Motor* m_horizontal_motor;

		float motor_min_control = -1.0;
		float motor_max_control = 1.0;

        uint32_t m_vertical_max_current;
        uint32_t m_horizontal_max_current;

		float m_motor_increment_step;
		float m_motor_decrement_step;

		float m_momentary_a_value;
		float m_momentary_b_value;

		float m_max_cmd_delta;

		float motor_forward_power;
		float motor_backward_power;

		float m_vertical_target_power;
		float m_horizontal_target_power;

		float m_vertical_command_power;
		float m_horizontal_command_power;

		DataLogger      *hopper_log;
};

/*******************************************************************************
 *
 * This Macro Step sets the power Motor PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn it off unless the
 *          provided value for power is 0.0.
 *
 *  Example XML:
 *
 *	<step name="lift_up" control="motor_1" type="SetPower"	power="0.5" >
 *  	<connect type="next" step="lift_up_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSHopper : public MacroStepSequence
{
	public:
	MSHopper(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		HopperControl *m_parent_control;

		float m_power;
};
