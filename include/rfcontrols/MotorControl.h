/*******************************************************************************
 *
 * File: MotorControl.h
 *
 * Written by:
 *  FRC Team 324, Chips
 * 	Clear Creek Independent School District
 *  NASA, Johnson Spce Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include "rfcontrols/PeriodicControl.h"
#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"

#include "rfhardware/Motor.h"
#include "rfhardware/LimitSwitch.h"

#include "gsutilities/DataLogger.h"

/*******************************************************************************	
 * 
 * Create an instance of an open loop motor control and connect it to the specified
 * motor and inputs
 * 
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional.
 *  
 *  <control type="motor" [name="unnamed"] [max_cmd_delta="0.25"] [period="0.1"]
 *      [min_control="-1.0"] [max_control="1.0"] >
 *
 *      [<motor type="Victor" [module="1"] [port="1"] [invert="false"] />]
 * 		[<motor type="Victor" [module="1"] [port="1"] [invert="false"] />]
 *      [<oi name="analogPower"     device="pilot" chan="1" [scale="1.0"]/>]
 *      [<oi name="latchPower" 	    device="pilot" chan="2" step="0.1" [invert="false"]/>]
 *      [<oi name="momentaryPower" 	device="pilot" chan="3" step="0.1" [invert="false"]/>]
 *      [<oi name="adjustPower"    	device="pilot" chan="4" [invert="false"]/>]
 *      [<oi name="povPower"	    device="pilot" chan="5" value="0.1" [invert="false"]/>]
 *  </control>
 *
 ******************************************************************************/
class MotorControl : public PeriodicControl
{
	public:
		MotorControl(std::string control_name, tinyxml2::XMLElement *xml);
		~MotorControl(void);

  		void controlInit(void);

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();
		void doPeriodic();

		void setPower(float value);
		void setLatchedPower(float value, bool on=true);
		void setMomentaryPower(float value, bool on=true);
		void setPovPower(float value, int direction);
		void adjustPower(float value, bool on=true);

		void publish(void);
        void log(void);
        void logHeaders(void);

	private:		
        Motor* motor_a;
        Motor* motor_b;

		gsu::DataLogger data_logger;

		bool hardware_is_ready;
		
 		bool upper_limit_pressed;
		bool lower_limit_pressed;

		float motor_min_percent;
		float motor_max_percent;

        uint32_t motor_a_max_current;
        uint32_t motor_b_max_current;

		float motor_percent_target;  // what the user asked for
		float motor_percent_command; // what this class told the motor
		float motor_percent_actual;  // what the motor (motor_a) is reporting
};

/*******************************************************************************
 *
 * This Macro Step sets the Motor Target Power.
 *
 * WARNING: This just sets the power, it does not turn it off unless the
 *          provided value for power is 0.0.
 *
 *  Example XML:
 *
 *	<step name="lift_up" control="motor_1" type="SetPower"	power="0.5" >
 *  	<connect type="next" step="lift_up_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MotorControlMSSetPower : public MacroStepSequence
{
	public:
	    MotorControlMSSetPower(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep* update(void);

	private:
		MotorControl* parent_control;

		float target_power;
};
