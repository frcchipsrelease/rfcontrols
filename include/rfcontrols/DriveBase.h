/*******************************************************************************
 *
 * File: DriveBase.h
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#pragma once

#include <string>

#include "gsutilities/tinyxml2.h"

#include "frc/Solenoid.h"
#include "frc/BuiltInAccelerometer.h"

#include "rfutilities/OIObserver.h"
#include "rfutilities/OIController.h"
#include "rfutilities/MacroStep.h"
#include "rfutilities/DataLogger.h"

#include "rfhardware/Motor.h"
#include "rfhardware/ADIS16448_IMU.h"

#include "rfhardware/Limelight.h"
#include "rfcontrols/PeriodicControl.h"

/*******************************************************************************
 *
 *  Provides a PeriodicControl implementation for driving the base of a robot
 *  using an Arcade-like driver interface, that is a forward and turn command
 *  are used to make the robot move.
 *  
 * This class is designed to be created from an XML element with the following
 * format, portions contained in [ ] are optional. 
 *  
 *  <control type="arcade_drive" name="drive">
 *      [<motor name="front_left"   [type="Victor"] [module="1"] [port="1"] [invert="false"] />]
 *      [<motor name="front_right"  [type="Victor"] [module="1"] [port="2"] [invert="false"] />]
 *      [<motor name="back_left"  	[type="Victor"] [module="1"] [port="3"] [invert="false"] />]
 *      [<motor name="back_right" 	[type="Victor"] [module="1"] [port="4"] [invert="false"] />]
 *      [<accelerometer />]
 *      [<imu />]
 *      [<oi name="forward"      	device="pilot" chan="2" 	[scale="-0.8"|invert="false"]/>]
 *      [<oi name="turn"         	device="pilot" chan="3" 	[scale="0.8"|invert="false"]/>]
 *      [<oi name="strafe_on"		device="pilot" chan="9"		[invert="false"]/>]
 *      [<oi name="strafe_off"		device="pilot" chan="10" 	[invert="false"]/>]
 *      [<oi name="strafe_toggle"	device="pilot" chan="11" 	[invert="false"]/>]
 *      [<oi name="strafe_state"	device="pilot" chan="12" 	[invert="false"]/>]
 *  </control>
 * 
 ******************************************************************************/
class DriveBase : public PeriodicControl, public OIObserver
{
	public:
		enum
		{
			CMD_FORWARD = 0, 	CMD_TURN,
			CMD_ARC,            CMD_POWER_ARC, 			CMD_LIMELIGHT_STATE,
		};

		DriveBase(std::string name, tinyxml2::XMLElement *xml); 
		~DriveBase(void);

		void setAnalog(int id, float val);
		void setDigital(int id, bool val);
		void setInt(int id, int val);

		void setPipeline(int id);

		void updateConfig();
		void controlInit();

		void disabledInit();
		void autonomousInit();
		void teleopInit();
		void testInit();

		void doPeriodic();
		void publish();

	private:
        void logFileInit(std::string phase);
        void logFileAppend(void);
		void setLimelightState(bool value);
		const float RAD_TO_DEG = 57.2958;

		//TrapezoidProfile3 m_traj_profile;

        DataLogger *m_data_logger;
	 	Limelight* m_limelight;

		bool is_limelight_control; 
		double m_targetHeight;
		double m_limelightHeight;
		double m_heightDifference;
		double m_calibratedDistance;
		double m_distanceOffset;
		double m_target_visible;
		double m_targetOffsetAngle_Horizontal;
		double m_targetOffsetAngle_Vertical;
		double m_targetArea;
		double m_targetSkew;
		double m_limelight_min_command;
		double m_limelight_scale_factor;


        Motor* fl_drive_motor;
        Motor* fr_drive_motor;
        Motor* bl_drive_motor;
        Motor* br_drive_motor;
		
		float fl_drive_motor_cmd;
		float fr_drive_motor_cmd;
		float bl_drive_motor_cmd;
		float br_drive_motor_cmd;
	
		float trn_power;
		float fwd_power;
		float arc_turn_power;
		float arc_fwd_power;
		float arc_turn_scale;

		int arc_input;
		bool arc_power;

        frc::BuiltInAccelerometer *m_accelerometer;
        double m_accelerometer_x;
        double m_accelerometer_y;
        double m_accelerometer_z;

        ADIS16448_IMU *m_imu;
        double m_imu_accel_x;
        double m_imu_accel_y;
        double m_imu_accel_z;

        double m_imu_mag_x;
        double m_imu_mag_y;
        double m_imu_mag_z;

        double m_imu_rate_x;
        double m_imu_rate_y;
        double m_imu_rate_z;

        double m_imu_angle_x;
        double m_imu_angle_y;
        double m_imu_angle_z;

        double m_imu_roll;
        double m_imu_pitch;
        double m_imu_yaw;

        double m_imu_quat_w;
        double m_imu_quat_x;
        double m_imu_quat_y;
        double m_imu_quat_z;

        double m_imu_bar_press;
        double m_imu_temperature;

		float target_position;
		bool m_closed_loop;
};

/*******************************************************************************
 *
 * This Macro Step sets the forward power, turn power, and strafe state of the
 * Drive Arcade PacBotControl.
 *
 * WARNING: This just sets the power, it does not turn them off unless the
 *          provided values for forward, turn, and strafe turn the drive off.
 *
 *  Example XML:
 *
 *	<step name="drive_1" control="drive" type="DrivePower"
 *			forward="0.5" turn="0.1" strafe="false">
 *  	<connect type="next" step="drive_wait"/>>
 *  </step>
 *
 ******************************************************************************/
class MSDrive : public MacroStepSequence
{
	public:
		MSDrive(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		DriveBase *parent_control;

		float m_forward;
		float m_turn;
};
/*******************************************************************************
 *
 * This Macro Step sets the pipeline number for the limelight.
 *
 *
 *  Example XML:
 *
 *
 ******************************************************************************/
class MSPipeline : public MacroStepSequence
{
	public:
		MSPipeline(std::string type, tinyxml2::XMLElement *xml, void *control);

		void init(void);
		MacroStep * update(void);

	private:
		DriveBase *parent_control;
		int m_pipeline;

};


