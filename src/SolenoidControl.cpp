/*******************************************************************************
 *
 * File: SolenoidControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"

#include "gsutilities/Advisory.h"

#include "frc/smartDashboard/SmartDashboard.h" //WPI

#include "rfcontrols//SolenoidControl.h"

#include "rfutilities/MacroStepFactory.h"

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of this object and configure it based on the provided
 * XML, period, and priority
 * 
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 						
 * @param	period		the time (in seconds) between calls to update()
 * @param	priority	the priority at which this thread/task should run
 * 
 ******************************************************************************/
SolenoidControl::SolenoidControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Solenoid Control [%s] =========================",
	            control_name.c_str());
	solenoid_a = nullptr;
	solenoid_b = nullptr;
	solenoid_state = false;
	
	//
	// Register Macro Steps
	//
	new MacroStepProxy<SolenoidControlMSSetState>(control_name, "SetState", this);

	//
	// Parse the XML
	//
	comp = xml->FirstChildElement("solenoid");
	if (comp != nullptr)
	{
		solenoid_a = HardwareFactory::createSolenoid(comp);
	}
	
	comp = comp->NextSiblingElement("solenoid");
	if (comp != nullptr)
	{
		solenoid_b = HardwareFactory::createSolenoid(comp);
	}
	
	const char *name;
	comp = xml->FirstChildElement("oi");
	while (comp != NULL)
	{
		name = comp->Attribute("name");
		if (name != NULL)
		{
			if (strcmp(name, "on") == 0)
			{
				Advisory::pinfo("  connecting to on channel");
				OIController::subscribeDigital(comp, this, CMD_ON);
			}
			else if (strcmp(name, "off") == 0)
			{
				Advisory::pinfo("  connecting to off channel");
				OIController::subscribeDigital(comp, this, CMD_OFF);
			}
			else if (strcmp(name, "toggle") == 0)
			{
				Advisory::pinfo("  connecting to toggle channel");
				OIController::subscribeDigital(comp, this, CMD_TOGGLE);
			}
			else if (strcmp(name, "state") == 0)
			{
				Advisory::pinfo("  connecting to state channel");
				OIController::subscribeDigital(comp, this, CMD_STATE);
			}
		}
		comp = comp->NextSiblingElement("oi");
	}
	// TODO hooks
	compressor = new frc::Compressor{1,frc::PneumaticsModuleType::REVPH};
	compressor->EnableDigital();
}

/*******************************************************************************	
 *
 * Release any resources used by this object
 * 
 ******************************************************************************/
SolenoidControl::~SolenoidControl(void)
{
	if (solenoid_a != nullptr)
	{
		delete solenoid_a;
		solenoid_a = nullptr;
	}

	if (solenoid_b != nullptr)
	{
		delete solenoid_b;
		solenoid_b = nullptr;
	}
}

/*******************************************************************************	
 *
 *
 *
 ******************************************************************************/
void SolenoidControl::controlInit(void)
{

}

/*******************************************************************************
 *
 * Sets the state of the solenoid based on the command id and value
 * 
 * @param	id	the command id that indicates what the val argument means
 * @param	button_state	true if the button is pressed, false if it's released
 * 
 ******************************************************************************/
void SolenoidControl::setDigital(int id, bool button_state)
{
	switch (id)
	{
		case CMD_ON:
		{
			if (button_state) setState(true);
		} break;

		case CMD_OFF:
		{
			if (button_state) setState(false);
		} break;

		case CMD_TOGGLE:
		{
			if (button_state) setState(!solenoid_state);
		} break;

		case CMD_STATE:
		{
			setState(button_state);
		} break;

		default:
			break;
	}
}

/*******************************************************************************	
 *
 * 
 * 
 ******************************************************************************/
void SolenoidControl::setAnalog(int id, float val)
{
}

/*******************************************************************************	
 *
 * 
 *
 ******************************************************************************/
void SolenoidControl::setInt(int id, int val)
{
}

/*******************************************************************************
 *
 * Set the state of the solenoid to the specified value.
 * 
 * If DISABLED, this will always set the state to false, if not DISABLED,
 * the solenoid_state will be set to the specified state. This is to make
 * sure things don't jump when the Robot is enabled.
 * 
 * @param	state	the new desired state.
 *
 ******************************************************************************/
void SolenoidControl::setState(bool state)
{
    if (this->getPhase() == ControlThread::ControlPhase::DISABLED)
	{
    	solenoid_state = false;
	}
	else
	{
    	solenoid_state = state;
	}
}

/*******************************************************************************	
 *
 * 
 *
 ******************************************************************************/
void SolenoidControl::disabledInit(void)
{
	setState(false);
}

/*******************************************************************************	
 *
 * State to false for start of auton
 *
 ******************************************************************************/
void SolenoidControl::autonomousInit(void)
{
	setState(false);
}

/*******************************************************************************
 *
 * Set state to false for start of teleop
 * 
 ******************************************************************************/
void SolenoidControl::teleopInit(void)
{
	setState(false);
}

/*******************************************************************************	
 *
 * Set state to false for start of test
 *
 ******************************************************************************/
void SolenoidControl::testInit(void)
{
	setState(false);
}

/*******************************************************************************
 *
 * Sends data to dashboard
 *
 ******************************************************************************/
void SolenoidControl::publish()
{
//	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
//	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());
	SmartDashboard::PutBoolean(getName() +" state: ", solenoid_state);
}

/*******************************************************************************
 *
 * Sets solenoid to a boolean value each period
 *
 ******************************************************************************/
void SolenoidControl::doPeriodic()
{
	if (solenoid_a != nullptr)
	{
		solenoid_a->Set(solenoid_state);
	}

	if (solenoid_b != nullptr)
	{
		solenoid_b->Set( ! solenoid_state);
	}
}

// =============================================================================
// =============================================================================
// ===
// === SolenoidControlMSSetState
// ===
// =============================================================================
// =============================================================================

/*******************************************************************************
 *
 * Create an instance of the SetState Macros step for the specified
 * control and desired solenoid state.
 *
 ******************************************************************************/
SolenoidControlMSSetState::SolenoidControlMSSetState(std::string type, tinyxml2::XMLElement *xml, void *control) 
    : MacroStepSequence(type, xml, control)
{
	parent_control = (SolenoidControl *) control;
	state = true;
	xml->QueryBoolAttribute("state", &state);
}

/*******************************************************************************
 *
 * When called this MacroStep will always set the state of the 
 * SolenoidControl to state indicated in the xml passed to the constructor
 *
 ******************************************************************************/
void SolenoidControlMSSetState::init(void)
{
	parent_control->setState(state);
}

/*******************************************************************************
 *
 * This MacroStep will always return the next step in the sequence.
 *
 ******************************************************************************/
MacroStep * SolenoidControlMSSetState::update(void)
{
	return next_step;
}
