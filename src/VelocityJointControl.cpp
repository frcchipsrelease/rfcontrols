/*******************************************************************************
 * 
 * File: VelocityJointControl.cpp
 *
 * Written by:
 *  FRC Team 324, Chips
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfcontrols//VelocityJointControl.h"

#include "rfhardware/HardwareFactory.h"

#include "rfutilities/RobotUtil.h"
#include "rfutilities/MacroStepFactory.h"
#include "rfutilities/OIController.h"

#include "gsutilities/Advisory.h"
#include "gsutilities/Filter.h"

#include "gsinterfaces/Time.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

#include <math.h>

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a Velocity Joint control with a single motor
 * 
 * @param	coontrol_name	the name of this instance so it can be 
 *                          used to link other XML to this and for debug
 * 
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 						
 ******************************************************************************/
VelocityJointControl::VelocityJointControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
    , data_logger("/robot/logs/vjc/", control_name.c_str(), "csv", 10, 5, 512)
{
	Advisory::pinfo("========================= Creating Velocity Joint [%s] =========================", control_name.c_str());
	
	is_closed_loop = false;
	hardware_is_ready = false;

	motor = nullptr;

	motor_percent_min = -1.0;
	motor_percent_max = 1.0;
	motor_percent_target = 0.0;  
	motor_percent_command = 0.0; 
	motor_percent_actual = 0.0;  

	motor_velocity_min = -10.0;
	motor_velocity_max = 10.0;
	motor_velocity_target = 0.0;
	motor_velocity_command = 0.0;
	motor_velocity_actual = 0.0;
	motor_velocity_raw = 0.0;

    //
	// Register Macro Steps
	//
	new MacroStepProxy<MSVelJointSetPower>(control_name, "SetPower", this);
	new MacroStepProxy<MSVelJointSetVelocity>(control_name, "SetVelocity", this);

	//
	// Parse the Controls XML
	//
	XMLElement* comp;
	const char* name;

	xml->QueryBoolAttribute("closed_loop", &is_closed_loop);
	xml->QueryFloatAttribute("percent_min", &motor_percent_min);
	xml->QueryFloatAttribute("percent_max", &motor_percent_max);
	xml->QueryFloatAttribute("velocity_min", &motor_velocity_min);
	xml->QueryFloatAttribute("velocity_max", &motor_velocity_max);

	comp = xml->FirstChildElement("motor");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name == nullptr)
		{
			Advisory::pinfo("found motor element with no name");
		}
		else if (strcmp(name, "motor") == 0)
		{
			Advisory::pinfo("creating motor controller");
			motor = HardwareFactory::createMotor(comp);
		}
		else
		{
			Advisory::pcaution("found motor element with unexpected name - name=\"%s\"", name);
		}

		comp = comp->NextSiblingElement("motor");
	}

	comp = xml->FirstChildElement("oi");
	while (comp != NULL)
	{
		name = comp->Attribute("name");
		if (name == nullptr)
		{
			Advisory::postCaution("OI with no name was ignored");
		}
		else if (strcmp(name, "closedLoop") == 0)
		{
			Advisory::pinfo("  connecting closedLoop channel");
			bool closed = false;
			comp->QueryBoolAttribute("closed", &closed);
			OIController::subscribeDigital(comp, 
				std::bind(&VelocityJointControl::setClosedLoop, this, closed, std::placeholders::_1));
		}
		else if (strcmp(name, "analogPower") == 0)
		{
			Advisory::pinfo("  connecting setPower channel");
			OIController::subscribeAnalog(comp, 
				std::bind(&VelocityJointControl::setPower, this, std::placeholders::_1));
		}
		else if (strcmp(name, "latchedPower") == 0)
		{
			Advisory::pinfo("  connecting latchedPower channel");
			float value = 0.1;
			comp->QueryFloatAttribute("value", &value);
			OIController::subscribeDigital(comp, 
				std::bind(&VelocityJointControl::setLatchedPower, this, value, std::placeholders::_1));
		}
		else if (strcmp(name, "momentaryPower") == 0)
		{
			Advisory::pinfo("  connecting momentaryPower channel");
			float value = 0.1;
			comp->QueryFloatAttribute("value", &value);
			OIController::subscribeDigital(comp, 
				std::bind(&VelocityJointControl::setMomentaryPower, this, value, std::placeholders::_1));
		}
		else if (strcmp(name, "adjustPower") == 0)
		{
			Advisory::pinfo("  connecting adjustPower channel");
			float value = 0.1;
			comp->QueryFloatAttribute("value", &value);
			OIController::subscribeDigital(comp, 
				std::bind(&VelocityJointControl::adjustPower, this, value, std::placeholders::_1));
		}
		else if (strcmp(name, "analogVelocity") == 0)
		{
			Advisory::pinfo("  connecting analogVelocity channel");
			OIController::subscribeAnalog(comp, 
				std::bind(&VelocityJointControl::setVelocity, this, std::placeholders::_1));
		}
		else if (strcmp(name, "latchedVelocity") == 0)
		{
			Advisory::pinfo("  connecting latchedVelocity channel");
			float value = 0.1;
			comp->QueryFloatAttribute("value", &value);
			OIController::subscribeDigital(comp, 
				std::bind(&VelocityJointControl::setLatchedVelocity, this, value, std::placeholders::_1));
		}
		else if (strcmp(name, "momentaryVelocity") == 0)
		{
			Advisory::pinfo("  connecting momentaryVelocity channel");
			float value = 0.1;
			comp->QueryFloatAttribute("value", &value);
			OIController::subscribeDigital(comp, 
				std::bind(&VelocityJointControl::setMomentaryVelocity, this, value, std::placeholders::_1));
		}
		else if (strcmp(name, "adjustVelocity") == 0)
		{
			Advisory::pinfo("  connecting adjustVelocity channel");
			float value = 0.1;
			comp->QueryFloatAttribute("value", &value);
			OIController::subscribeDigital(comp, 
				std::bind(&VelocityJointControl::adjustVelocity, this, value, std::placeholders::_1));
		}
		else
		{
			Advisory::pinfo("OI with unexpected name (%s) was ignored", name);
		}

		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
VelocityJointControl::~VelocityJointControl(void)
{
	if (motor != nullptr)
	{
		delete motor;
		motor = nullptr;
	}

	data_logger.close();
}

/*******************************************************************************	
 *
 * This is called after the constructor has completed. It is used to check 
 * to make sure all required hardware was created. It can also be used to do
 * that is dependant on the hardware creation.
 *
 ******************************************************************************/
void VelocityJointControl::controlInit(void)
{
    bool check_ready = true;
	
	if(motor == nullptr)
	{
		Advisory::pwarning("%s VelocityJointControl missing required motor element -- name=\"motor\"", getName().c_str());
		check_ready = false;
	}

	hardware_is_ready = check_ready;
}

/*******************************************************************************
 *
 * This method is called each time the robot enters the DISABLED game phase,
 * it sets variables that will help make sure the robot does not move
 * unexpectedly when enabled. It also closes the log file if it was open.
 * 
 ******************************************************************************/
void VelocityJointControl::disabledInit()
{
	motor_percent_target = 0.0;  
	motor_percent_command = 0.0; 
	motor_velocity_target = 0.0;
	motor_velocity_command = 0.0;

	data_logger.close();
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::autonomousInit()
{
    data_logger.open("_auton");
    logHeaders();

	motor_percent_target = 0.0;  
	motor_percent_command = 0.0; 
	motor_velocity_target = 0.0;
	motor_velocity_command = 0.0;
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::teleopInit()
{
    data_logger.open("_teleop");
    logHeaders();

	motor_percent_target = 0.0;  
	motor_percent_command = 0.0; 
	motor_velocity_target = 0.0;
	motor_velocity_command = 0.0;
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::testInit()
{
    data_logger.open("_test");
    logHeaders();

	motor_percent_target = 0.0;  
	motor_percent_command = 0.0; 
	motor_velocity_target = 0.0;
	motor_velocity_command = 0.0;
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::setClosedLoop(bool closed, bool pressed)
{
	if (pressed)
	{
		if (is_closed_loop != closed)
		{
			is_closed_loop = closed;

			if (is_closed_loop)
			{
				Advisory::pinfo("setting %s closed loop mode", getName().c_str());
				motor->setControlMode(Motor::VELOCITY); 
			}
			else
			{
				Advisory::pinfo("setting %s open loop mode", getName().c_str());
				motor->setControlMode(Motor::PERCENT); 
			}
		}
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
bool VelocityJointControl::isClosedLoop(void)
{
	return is_closed_loop;
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::setPower(float value)
{
	motor_percent_target = gsu::Filter::limit(value, motor_percent_min, motor_percent_max);
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::setLatchedPower(float value, bool pressed)
{
	if (pressed)
	{
		setPower(value);
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::setMomentaryPower(float value, bool pressed)
{
	if (pressed)
	{
		setPower(value);
	}
	else
	{
		setPower(0.0);
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::adjustPower(float value, bool pressed)
{
	if (pressed)
	{
		setPower(motor_percent_target + value);
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::setVelocity(float val)
{
	motor_velocity_target = gsu::Filter::limit(val, motor_velocity_min, motor_velocity_max);
}

/*******************************************************************************
 *
 ******************************************************************************/
float VelocityJointControl::getVelocity(void)
{
	return motor_velocity_actual;
}

/*******************************************************************************
 *
 ******************************************************************************/
bool VelocityJointControl::isAtTarget(float tolerance)
{
	return (fabs(motor_velocity_target - motor_velocity_actual) < tolerance);
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::setLatchedVelocity(float value, bool pressed)
{
	if(pressed)
	{
		setVelocity(value);
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::setMomentaryVelocity(float value, bool pressed)
{
	if(pressed)
	{
		setVelocity(value);
	}
	else
	{
		setVelocity(0.0);
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::adjustVelocity(float value, bool pressed)
{
	if(pressed)
	{
		setVelocity(motor_velocity_target + value);
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void VelocityJointControl::publish()
{
	SmartDashboard::PutBoolean(getName() + " closed loop: ", is_closed_loop);

	SmartDashboard::PutNumber(getName() + " target velocity: ", motor_velocity_target);
	SmartDashboard::PutNumber(getName() + " command velocity: ", motor_velocity_command);
	SmartDashboard::PutNumber(getName() + " actual velocity: ", motor_velocity_actual);
	SmartDashboard::PutNumber(getName() + " raw velocity: ", motor_velocity_raw);

	SmartDashboard::PutNumber(getName() + " target percent: ", motor_percent_target);
	SmartDashboard::PutNumber(getName() + " command percent: ", motor_percent_command);
	SmartDashboard::PutNumber(getName() + " actual percent: ", motor_percent_actual);
}


/**********************************************************************
 *
 * This method is used to initialize the log files, it is called from
 * teleop init and auton init methods, it should log the row of column
 * headers to the file.
 *
 **********************************************************************/
void VelocityJointControl::logHeaders(void)
{
	gsu::DataBuffer* line_buffer = data_logger.getEmptyBuffer();
    if (line_buffer == nullptr)
    {
       return;
    }

    char* line_ptr = (char*)(line_buffer->data);
    uint16_t buffer_size = line_buffer->getBufferSize();
    uint16_t remaining_bytes = buffer_size;

     line_ptr += snprintf(line_ptr, remaining_bytes, "%s", "time,");
     remaining_bytes = buffer_size - (line_ptr - (char*)(line_buffer->data));

    line_ptr += snprintf(line_ptr, remaining_bytes, "%s,%s,%s,%s,%s,%s,%s,%s\n",
		"closed_loop",
		"motor_percent_target",  
		"motor_percent_command", 
		"motor_percent_actual",
		"motor_velocity_target",
		"motor_velocity_command",
		"motor_velocity_actual",
		"motor_velocity_raw"
    );

//    remaining_bytes = buffer_size - (line_ptr - (char*)(line_buffer->data));
    line_ptr[buffer_size-1] = '\0'; // if too much is put into the buffer, it may not be null terminated

    data_logger.putFullBuffer(line_buffer);
}

/*******************************************************************************
 *
 * This method is used to write data to the log files, it is called from
 * the end of doPeriodic(), it should log a row of data that represents the
 * current pass through doPeriodic().
 *
 ******************************************************************************/
void VelocityJointControl::logData(void)
{
    gsu::DataBuffer* line_buffer = data_logger.getEmptyBuffer();
    if (line_buffer == nullptr)
    {
        return;
    }

    char* line_ptr = (char*)(line_buffer->data);
    uint16_t buffer_size = line_buffer->getBufferSize();
    uint16_t remaining_bytes = buffer_size;

    line_ptr += snprintf(line_ptr, remaining_bytes, "%s,", gsi::Time::getTimeString(gsi::Time::FORMAT_YMDHMSu).c_str());
    remaining_bytes = buffer_size - (line_ptr - (char*)(line_buffer->data));

    line_ptr += snprintf(line_ptr, remaining_bytes, "%d,%f,%f,%f,%f,%f,%f,%f\n",
		is_closed_loop,
		motor_percent_target,  
		motor_percent_command, 
		motor_percent_actual,
		motor_velocity_target,  
		motor_velocity_command,  
		motor_velocity_actual,  
		motor_velocity_raw
    );

//    remaining_bytes = buffer_size - (line_ptr - (char*)(line_buffer->data));
   line_ptr[buffer_size-1] = '\0';

    data_logger.putFullBuffer(line_buffer);
}


/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void VelocityJointControl::doPeriodic()
{
	if(hardware_is_ready == false)
	{
		Advisory::postCaution("VelocityJointControl %s -- hardware is not ready", getName());
		return;
	}

	//
	// Get the Inputs
	motor->doUpdate();

    motor_percent_actual = motor->getPercent();
	motor_velocity_raw = motor->getRawVelocity();
	motor_velocity_actual = motor->getVelocity();
	
	//
	// Make sure the motor doesn't jump when enabled
	//
	if (getPhase() == DISABLED)
	{
		motor_percent_target = 0.0;  
		motor_percent_command = 0.0; 
		motor_velocity_target = 0.0;
		motor_velocity_command = 0.0;
	}

	//
	// Do any Processing
	//
	motor_percent_command = gsu::Filter::limit(motor_percent_target, motor_percent_min, motor_percent_max);
	motor_velocity_command = gsu::Filter::limit(motor_velocity_target, motor_velocity_min, motor_velocity_max);
	
	//
	// Set the Outputs
	//
	if (is_closed_loop)
	{
		motor_percent_target = 0.0;
		motor->setVelocity(motor_velocity_command);
	}
	else
	{
		motor_velocity_target = 0.0;
        motor->setPercent(motor_percent_command);
	}

	//
	// Log data
	//
	logData();
}

// =============================================================================
// =============================================================================
// =============================================================================

/*******************************************************************************
 *
 ******************************************************************************/
MSVelJointSetPower::MSVelJointSetPower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (VelocityJointControl *)control;
	power = 0.0;
	
	xml->QueryFloatAttribute("power", &power);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSVelJointSetPower::init(void)
{
	parent_control->setPower(power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSVelJointSetPower::update(void)
{
 	return next_step;
}

// =============================================================================
// =============================================================================
// =============================================================================

/*******************************************************************************
 *
 ******************************************************************************/
MSVelJointSetVelocity::MSVelJointSetVelocity(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (VelocityJointControl *)control;
	target_velocity = 0.0;
	target_tolerance = 0.1;
	wait_for_position = false;

	xml->QueryFloatAttribute("position", &target_velocity);
	xml->QueryFloatAttribute("tolerance", &target_tolerance);
	xml->QueryBoolAttribute("wait", &wait_for_position);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSVelJointSetVelocity::init(void)
{
	parent_control->setVelocity(target_velocity);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSVelJointSetVelocity::update(void)
{
	if (wait_for_position)
	{
		if (parent_control->isAtTarget(target_tolerance))
		{
			return next_step;
		}
		else
		{
			return this;
		}
	}
	else
	{
 		return next_step;
	}
}
