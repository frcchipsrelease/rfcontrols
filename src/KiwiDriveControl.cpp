/*******************************************************************************
 *
 * File: KiwiDriveControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/

#include "frc/smartDashboard/SmartDashboard.h"
#include "frc/Solenoid.h"

#include "gsutilities/Advisory.h"

#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "rfutilities/MacroStepFactory.h"

#include "rfcontrols//KiwiDriveControl.h"

using namespace tinyxml2;
using namespace frc;

#define SIN_30 (0.500000)
#define COS_30 (0.866025)

/*******************************************************************************	
 * 
 * Create an instance of this object and configure it based on the provided
 * XML, period, and priority
 * 
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 						
 * @param	period		the time (in seconds) between calls to update()
 * @param	priority	the priority at which this thread/task should run
 * 
 * *****************************************************************************/
KiwiDriveControl::KiwiDriveControl(std::string control_name, XMLElement* xml)
	:	PeriodicControl(control_name)
{
    Advisory::pinfo("========================= Creating Kiwi Drive Control [%s] =========================",
                control_name.c_str());
	XMLElement *comp;

	const char *name;

    for (int m = 0; m < NUM_MOTORS; m++)
    {
        drive_motor[m] = nullptr;
        motor_cmd[m] = 0.0;
        motor_trg[m] = 0.0;
    }

	for(int i = 0; i < NUM_INPUTS; i++)
	{
	    input_cmd[i] = 0.0;
	}
	
	name = xml->Attribute("name");
	if (name == nullptr)
	{
		name="drive";
		Advisory::pcaution(  "WARNING: KiwiArchade created without name, assuming \"%s\"", name);
	}

	//
	// Register Macro Steps
	//
	new MacroStepProxy<KiwiDriveMSDrivePower>(control_name, "DrivePower", this);
	
	//
	// Parse the XML
	//
	comp = xml-> FirstChildElement("motor");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "front_left") == 0)
			{				
				Advisory::pinfo("  creating speed controller for %s", name);
				drive_motor[MOTOR_FRONT_LEFT] = HardwareFactory::createMotor(comp);
			}
			else if (strcmp(name, "front_right") == 0)
			{
			    Advisory::pinfo("  creating speed controller for %s", name);
                drive_motor[MOTOR_FRONT_RIGHT] = HardwareFactory::createMotor(comp);
			}
			else if (strcmp(name, "back") == 0)
			{
			    Advisory::pinfo("  creating speed controller for %s", name);
                drive_motor[MOTOR_BACK] = HardwareFactory::createMotor(comp);
			}
			else
			{
				Advisory::pinfo("  could not create unknown motor for %s", name);
			}
		}
		comp = comp->NextSiblingElement("motor");
	}

	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
            Advisory::pinfo("  connecting oi: \"%s\"", name);
			if (strcmp(name, "forward") == 0)
			{
				OIController::subscribeAnalog(comp, this, CMD_FORWARD);
			}
			else if (strcmp(name, "lateral") == 0)
			{
				OIController::subscribeAnalog(comp, this, CMD_LATERAL);
			}
			else if (strcmp(name, "rotate") == 0)
			{
                OIController::subscribeAnalog(comp, this, CMD_ROTATE);
			}
			else
			{
				Advisory::pwarning("  could not connect unknown oi: name=\"%s\"", name);
			}
		}
		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 *
 * Release any resources used by this object
 * 
 ******************************************************************************/
KiwiDriveControl::~KiwiDriveControl(void)
{
    for (int m = 0; m < NUM_MOTORS; m++)
    {
        if (drive_motor[m] != nullptr)
        {
            delete drive_motor[m];
            drive_motor[m] = nullptr;
        }

        motor_cmd[m] = 0.0;
        motor_trg[m] = 0.0;
    }
}

/*******************************************************************************	
 *
 ******************************************************************************/
void KiwiDriveControl::controlInit()
{
    for(int i = 0; i < NUM_INPUTS; i++)
    {
        input_cmd[i] = 0.0;
    }
}

/*******************************************************************************	
 *
 ******************************************************************************/
void KiwiDriveControl::disabledInit()
{
    for(int i = 0; i < NUM_INPUTS; i++)
    {
        input_cmd[i] = 0.0;
    }
}

/*******************************************************************************	
 *
 ******************************************************************************/
void KiwiDriveControl::autonomousInit()
{
    for(int i = 0; i < NUM_INPUTS; i++)
    {
        input_cmd[i] = 0.0;
    }
}

/*******************************************************************************	
 *
 ******************************************************************************/
void KiwiDriveControl::teleopInit()
{
    for(int i = 0; i < NUM_INPUTS; i++)
    {
        input_cmd[i] = 0.0;
    }
}

/*******************************************************************************	
 *
 ******************************************************************************/
void KiwiDriveControl::testInit()
{
    for(int i = 0; i < NUM_INPUTS; i++)
    {
        input_cmd[i] = 0.0;
    }
}

/*******************************************************************************
 *
 ******************************************************************************/
void KiwiDriveControl::setInt(int id, int val)
{
}

/*******************************************************************************
 *
 * Sets the state of the control based on the command id and value
 * 
 * Handled command ids are CMD_TURN and CMD_FORWARD, all others are ignored
 * 
 * @param	id	the command id that indicates what the val argument means
 * @param	val	the new value
 * 
 ******************************************************************************/
void KiwiDriveControl::setAnalog(int id, float val)
{
	switch (id)
	{
        case CMD_FORWARD:
        {
            input_cmd[INPUT_FORWARD] = val;
        } break;

        case CMD_LATERAL:
        {
            input_cmd[INPUT_LATERAL] = val;
        } break;

		case CMD_ROTATE:
		{
		    input_cmd[INPUT_ROTATE] = val;
		} break;

		default:
			break;
	}
}

/*******************************************************************************	
 *
 * Sets the state of the control based on the command id and value
 *  
 * Handled command ids are CMD_BRAKE_[ON|OFF|TOGGLE|STATE] and 
 * CMD_GEAR_[HIGH|LOW|TOGGLE|STATE], all others are ignored
 *
 * @param	id	the command id that indicates what the val argument means
 * @param	val	the new value
 * 
 ******************************************************************************/
void KiwiDriveControl::setDigital(int id, bool val)
{
}

/*******************************************************************************
 *
 ******************************************************************************/
void KiwiDriveControl::publish()
{
//	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
//	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());

    SmartDashboard::PutNumber(getName() +" forward: ", input_cmd[INPUT_FORWARD]);
    SmartDashboard::PutNumber(getName() +" lateral: ", input_cmd[INPUT_LATERAL]);
    SmartDashboard::PutNumber(getName() +" rotate: ",  input_cmd[INPUT_ROTATE]);

    SmartDashboard::PutNumber(getName() +" mtr_cmd_fl: ",  motor_cmd[0]);
    SmartDashboard::PutNumber(getName() +" mtr_trg_fl: ",  motor_trg[0]);
    SmartDashboard::PutNumber(getName() +" mtr_cmd_fr: ",  motor_cmd[1]);
    SmartDashboard::PutNumber(getName() +" mtr_trg_fr: ",  motor_trg[1]);
    SmartDashboard::PutNumber(getName() +" mtr_cmd_b: ",  motor_cmd[2]);
    SmartDashboard::PutNumber(getName() +" mtr_trg_b: ",  motor_trg[2]);
}

/*******************************************************************************	
 * 
 * Sets the actuator values every period
 * 
 ******************************************************************************/
void KiwiDriveControl::doPeriodic()
{
	//
	// Read Sensors
	//
    for(int m = 0; m < NUM_MOTORS; m++)
    {
        drive_motor[m]->doUpdate();
    }

	//
	// Calculate Values
	//
    motor_trg[MOTOR_FRONT_LEFT]  = (-SIN_30 * input_cmd[INPUT_LATERAL]) - (COS_30 * input_cmd[INPUT_FORWARD]) + input_cmd[INPUT_ROTATE];
    motor_trg[MOTOR_FRONT_RIGHT] = (-SIN_30 * input_cmd[INPUT_LATERAL]) + (COS_30 * input_cmd[INPUT_FORWARD]) + input_cmd[INPUT_ROTATE];
    motor_trg[MOTOR_BACK]        = input_cmd[INPUT_LATERAL] + input_cmd[INPUT_ROTATE];

    motor_cmd[MOTOR_FRONT_LEFT] = motor_trg[MOTOR_FRONT_LEFT];
    motor_cmd[MOTOR_FRONT_RIGHT] = motor_trg[MOTOR_FRONT_RIGHT];
    motor_cmd[MOTOR_BACK] = motor_trg[MOTOR_BACK];

    //
    // Normalize the Motor Commands
    //
    float max_cmd_magnitude = 0.0f;
    for(int m = 0; m < NUM_MOTORS; m++)
    {
        max_cmd_magnitude = std::max(max_cmd_magnitude, (float)fabs(motor_cmd[m]));
    }

    if (max_cmd_magnitude > 1.0)
    {
        for(int m = 0; m < NUM_MOTORS; m++)
        {
            motor_cmd[m] = motor_cmd[m]/max_cmd_magnitude;
        }
    }

	//
	// Set Outputs
	//
    for(int m = 0; m < NUM_MOTORS; m++)
    {
        if (drive_motor[m] != nullptr)
        {
            drive_motor[m]->setPercent(motor_cmd[m]);
        }
    }
}

// =============================================================================
// =============================================================================
// ===
// === KiwiDriveMSDrivePower
// ===
// =============================================================================
// =============================================================================

/*******************************************************************************
 *
 * Create an instance of the DrivePower Macros step for the specified
 * control and desired solenoid state.
 *
 ******************************************************************************/
KiwiDriveMSDrivePower::KiwiDriveMSDrivePower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (KiwiDriveControl *)control;

    forward_power = 0.0;
    lateral_power = 0.0;
    rotate_power =  0.0;

    xml->QueryFloatAttribute("forward", &forward_power);
    xml->QueryFloatAttribute("lateral", &lateral_power);
    xml->QueryFloatAttribute("rotate", &rotate_power);
}

/*******************************************************************************
 *
 * Sets the desired forward, lateral, and rotate power values in the 
 * specified control
 * 
 ******************************************************************************/
void KiwiDriveMSDrivePower::init(void)
{
	parent_control->setAnalog(KiwiDriveControl::CMD_FORWARD, forward_power);
	parent_control->setAnalog(KiwiDriveControl::CMD_LATERAL, lateral_power);
	parent_control->setAnalog(KiwiDriveControl::CMD_ROTATE,  rotate_power);
}

/*******************************************************************************
 *
 * Always return the next_step
 * 
 ******************************************************************************/
MacroStep * KiwiDriveMSDrivePower::update(void)
{
	return next_step;
}
