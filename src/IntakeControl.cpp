/*******************************************************************************
 *
 * File: IntakeControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "rfcontrols//IntakeControl.h"
#include "rfutilities/MacroStepFactory.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
IntakeControl::IntakeControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Intake Control [%s] =========================",
	            control_name.c_str());
	m_intake_motor = nullptr;
	m_deploy_motor = nullptr;

	m_stow_limit_sw = nullptr;
	m_deploy_limit_sw = nullptr;

	m_stow_limit_pressed = false;
	m_deploy_limit_pressed = false;

	motor_min_control = -1.0;
	motor_max_control = 1.0;

    m_intake_max_current = 100.0;
    m_deploy_max_current = 100.0;

	motor_forward_power = 0.0;
	motor_backward_power = 0.0;

	m_stow = 0.0;
	m_deploy = 0.0;

	motor_max_cmd_delta = 0.25;

	intake_target_power = 0.0;
	deploy_target_power = 0.0;
	intake_command_power = 0.0;
	deploy_command_power = 0.0;

	const char *name = nullptr;

	//
	// Register Macro Steps
	//
	new MacroStepProxy<MSIntakeRollerPower>(control_name, "IntakeRollerPower", this);
	new MacroStepProxy<MSIntakeDeploy>(control_name, "IntakeDeploy", this);

	//
	// Parse XML
	//
    xml->QueryFloatAttribute("min_control", &motor_min_control);
    xml->QueryFloatAttribute("max_control", &motor_max_control);
    xml->QueryFloatAttribute("max_cmd_delta", &motor_max_cmd_delta);
    Advisory::pinfo("  max cmd delta = %f", motor_max_cmd_delta);

	comp = xml-> FirstChildElement("motor");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "intake_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for intake motor");
				m_intake_motor = HardwareFactory::createMotor(comp);
				comp->QueryUnsignedAttribute("max_current", &m_intake_max_current);
				Advisory::pinfo("	max_current=%u", m_intake_max_current); 
			}
			else if(strcmp(name, "deploy_motor") == 0)
			{
    			Advisory::pinfo("  creating speed controller for Deploy Motor");
				m_deploy_motor = HardwareFactory::createMotor(comp);
				comp -> QueryUnsignedAttribute("max_current", &m_deploy_max_current);
				Advisory::pinfo("	max_current=%u", m_deploy_max_current);
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp -> NextSiblingElement("motor");
	}
	
	comp = xml->FirstChildElement("digital_input");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "stow_limit") == 0)
			{
				Advisory::pinfo("  creating stow limit for %s", name);
				m_stow_limit_sw = HardwareFactory::createLimitSwitch(comp);
			}
			else if (strcmp(name, "deploy_limit") == 0)
			{
				Advisory::pinfo("  creating deploy limit for %s", name);
				m_deploy_limit_sw = HardwareFactory::createLimitSwitch(comp);
			}
			else
			{
				Advisory::pwarning("  found digital_input tag with name %s", name);
			}
		}
		else
		{
			Advisory::pwarning("  found digital_input tag with no name attribute");
		}
		comp = comp->NextSiblingElement("digital_input");
	}

	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "forward") == 0)
			{
				Advisory::pinfo("  connecting forward channel");
				comp->QueryFloatAttribute("value", &motor_forward_power);
				OIController::subscribeDigital(comp, this, CMD_FORWARD);
			}
			else if (strcmp(name, "backward") == 0)
			{
				Advisory::pinfo("  connecting backward channel");
				comp->QueryFloatAttribute("value", &motor_backward_power);
				OIController::subscribeDigital(comp, this, CMD_BACKWARD);
			}
			else if (strcmp(name, "stop") == 0)
			{
				Advisory::pinfo("  connecting stop channel");
				OIController::subscribeDigital(comp, this, CMD_STOP);
			}
			else if (strcmp(name, "stow") == 0)
			{
				Advisory::pinfo("  connecting stow channel");
				comp->QueryFloatAttribute("value", &m_stow);
				OIController::subscribeDigital(comp, this, CMD_STOW);
			}
			else if(strcmp(name, "deploy") == 0)
			{
				Advisory::pinfo("  connecting deploy channel");
				comp->QueryFloatAttribute("value", &m_deploy);
				OIController::subscribeDigital(comp, this, CMD_DEPLOY);
			}
		}
		
		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
IntakeControl::~IntakeControl(void)
{
	if (m_intake_motor != nullptr)
	{
		delete m_intake_motor;
		m_intake_motor = nullptr;
	}
	if (m_deploy_motor != nullptr)
	{
		delete m_deploy_motor;
		m_deploy_motor = nullptr;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void IntakeControl::controlInit(void)
{ 
	/*
    if ((m_intake_motor != nullptr) && (m_intake_max_current < 100.0))
    {
      	m_intake_motor->setCurrentLimit(m_intake_max_current);
        m_intake_motor->setCurrentLimitEnabled(true);
    }
	if ((m_deploy_motor != nullptr) && (m_deploy_max_current < 100.0))
    {
      //  m_Intake_Motor->setCurrentLimit(m_Intake_motor_max_current);
        //m_Intake_Motor->setCurrentLimitEnabled(true);
    }
	*/
}

/*******************************************************************************
 *
 ******************************************************************************/
void IntakeControl::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void IntakeControl::disabledInit(void)
{
	intake_target_power = 0.0;
	intake_command_power = 0.0;
	deploy_target_power = 0.0;
	deploy_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void IntakeControl::autonomousInit(void)
{
	intake_target_power = 0.0;
	intake_command_power = 0.0;
	deploy_target_power = 0.0;
	deploy_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void IntakeControl::teleopInit(void)
{
	intake_target_power = 0.0;
	intake_command_power = 0.0;
	deploy_target_power = 0.0;
	deploy_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void IntakeControl::testInit(void)
{
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void IntakeControl::setAnalog(int id, float val)
{
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void IntakeControl::setDigital(int id, bool val)
{
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	switch (id)
	{
		case CMD_FORWARD:
		{
			if (val)
			{
				intake_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_forward_power);
			}
		} break;
			
		case CMD_BACKWARD:
		{
			if (val)
			{
				intake_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_backward_power);
			}
		} break;
			
		case CMD_STOP:
		{
			if (val)
			{
				intake_target_power = 0.0;
			}
		} break;

		case CMD_STOW:
		{
			if(val)
			{
				deploy_target_power = RobotUtil::limit(motor_min_control, motor_max_control, m_stow);
			}
			else
			{
				deploy_target_power = 0.0;
			}
		} break;

		case CMD_DEPLOY:
		{
			if(val)
			{
				deploy_target_power = RobotUtil::limit(motor_min_control, motor_max_control, m_deploy);
			}
			else
			{
				deploy_target_power = 0.0;
			}
		} break;
			
		default:
			break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void IntakeControl::setInt(int id, int val)
{
}

void IntakeControl::setDeployPower(float power)
{
	deploy_target_power = power;
}
void IntakeControl::setRollerPower(float power)
{
	intake_target_power = power;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void IntakeControl::publish()
{
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() + " intake target_power: ", intake_target_power);
	SmartDashboard::PutNumber(getName() + " intake command_power: ", intake_command_power);
	SmartDashboard::PutNumber(getName() + " deploy target_power: ", deploy_target_power);
	SmartDashboard::PutNumber(getName() + " deploy command_power: ", deploy_command_power);
	
    SmartDashboard::PutBoolean(getName() + " stow_limit: ", m_stow_limit_pressed);
    SmartDashboard::PutBoolean(getName() + " deploy_limit: ", m_deploy_limit_pressed);
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void IntakeControl::doPeriodic()
{
	m_intake_motor->doUpdate();
	m_deploy_motor->doUpdate();

	//
	// Get inputs -- this is just for reporting
	//

	m_stow_limit_pressed = m_intake_motor->isUpperLimitPressed();
	m_deploy_limit_pressed = m_intake_motor->isLowerLimitPressed();

	intake_command_power = m_intake_motor->getPercent();

	//
	// All processing happens in the motor class
	//

	//
	// Set Outputs
	//

	m_intake_motor->setPercent(intake_target_power);
	m_deploy_motor->setPercent(deploy_target_power);

}

/*******************************************************************************
 *
 ******************************************************************************/
MSIntakeDeploy::MSIntakeDeploy(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	deploy_power = 0.3;
	deploy_duration = 0.25;

	m_parent_control = (IntakeControl *)control;

	xml->QueryFloatAttribute("power", &deploy_power);
	xml->QueryDoubleAttribute("duration", &deploy_duration);

	if (deploy_duration > 2.0)
	{
		deploy_duration = 0.3;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSIntakeDeploy::init(void)
{
	deploy_end_time = gsi::Time::getTime() + deploy_duration;
	m_parent_control->setDeployPower(deploy_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSIntakeDeploy::update(void)
{
	if (gsi::Time::getTime() < deploy_end_time)
	{
		return this;
	}

	m_parent_control->setDeployPower(0.0);
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSIntakeRollerPower::MSIntakeRollerPower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	m_intake_roller_power = 0.0;
	m_parent_control = (IntakeControl *)control;

	xml->QueryFloatAttribute("power", &m_intake_roller_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSIntakeRollerPower::init(void)
{
	m_parent_control->setRollerPower(m_intake_roller_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSIntakeRollerPower::update(void)
{
	return next_step;
}
