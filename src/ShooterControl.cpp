/* 
RJP
If i read the code, this is what I think the XML would look like right now

<shooter   closed_loop="false" max_velocity="1 desired_acceleration="1"
             desired_deceleration="1">

    <setpoints>
	    <setpoint index="" name="" velocity="" percent="" />  !!!!! these should hav ol and cl
	    <setpoint index="" name="" velocity="" percent="" /> 
		...
	</setpoints>

    <motor  name="flywheel_motor1" />
    <motor  name="flywheel_motor2" />
    <motor  name="kicker_motor"    />
 
   <oi name="setpoint[0-9]" />  !!!! this disagrees with NUM_SETPOINTS 
    <oi name="momentary[0-3]" /> 
	<oi name="closed_loop_state" /> 
	<oi name="on" /> 
	<oi name="off" /> 
	<oi name="forward" /> 
	<oi name="reverse" /> 
</shooter>
*/

// sdr todo 
// sdr set flywheel2 as follower
// sdr ol_power and cl_step on setpoints as well as momentaries
// sdr auton logs

/*******************************************************************************
 *
 * File: ShooterControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 * 
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include <math.h>

#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "rfutilities/DataLogger.h"
#include "rfutilities/MacroStepFactory.h"

#include "rfcontrols/ShooterControl.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control, feedback, and PID 
 * and connect them to the specified inputs
 * clo
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 						
 * @param	period		the time (in seconds) between calls to update()
 * @param	priority	the priority at which this thread/task should run
 * 
 ******************************************************************************/
ShooterControl::ShooterControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	Advisory::pinfo("========================= Creating Shooter Control [%s] =========================", 
			control_name.c_str());
	
	m_flywheel_motor1 = nullptr;
	m_flywheel_motor2 = nullptr; 
	m_kicker_motor = nullptr;

	m_flashlight = nullptr;
	m_flashlight_state = false;

	m_is_ready = false;
	m_closed_loop = false;
	is_limelight_control = false;

	min_position = -1.0;
	max_position = 1.0;
	
	m_limelight = nullptr;

    m_max_velocity = 0.0;
    m_desired_acceleration = 0.0;
    m_desired_deceleration = 0.0;

	flywheel_percent_step = 0.0125;
	flywheel_velocity_step = 0.0125;

	flywheel_raw_position = 0.0; 
	flywheel_raw_velocity = 0.0;
	flywheel_actual_velocity = 0.0;
	
	kicker_raw_position = 0.0; 
	kicker_raw_velocity = 0.0;
    kicker_actual_velocity = 0.0;

	flywheel_target_percent = 0.0;
	flywheel_target_velocity = 0.0;
	flywheel_command_percent = 0.0;

	kicker_target_percent = 0.0;
	kicker_command_percent = 0.25;

	setpoint_index = 0;
	num_setpoints = 0;

	m_targetHeight = 0.0;
	m_limelightHeight = 0.0;
	m_heightDifference = 0.0;
	m_calibratedDistance = 0.0;
	m_distanceOffset = 0.0;	

	target_velocity = 0.0;
	raw_position = -999.99;
	actual_position = -999.99;

	raw_velocity = -999;
    actual_velocity = -999.99;

	for (uint8_t idx = 0; idx < NUM_MOMENTARIES; idx++)
	{
		momentary_power[idx] = 0.0;
		momentary_step[idx] = 0.0;
	}

	for (uint8_t i = 0; i < NUM_SETPOINTS; i++)
	{
		m_setpoint_velocity[i] = 0.0;
		m_setpoint_percent[i] = 0.0;
		m_setpoint_name[i] = "";
	}

	//
	// Data Logger
	//
	m_teleop_log = new DataLogger("/robot/logs/shooter", "teleshooter", "csv", 10, true);
    m_auton_log = new DataLogger("/robot/logs/shooter", "autoshooter", "csv", 10, true);
	m_active_log = m_auton_log;

    //
	// Register Macro Steps
	//
	new MacroStepProxy<MSShooter>(control_name, "SetPower", this);
	new MacroStepProxy<MSShooterFlywheelPower>(control_name, "SetFlywheelPower", this);
	new MacroStepProxy<MSShooterKickerPower>(control_name, "SetKickerPower", this);

	//
	// Parse the Controls XML
	//
	XMLElement *comp;
	const char *name;

	xml->QueryBoolAttribute("closed_loop", &m_closed_loop);
	xml->QueryBoolAttribute("limelight_control", &is_limelight_control);

	xml->QueryFloatAttribute("max_velocity", &m_max_velocity);
	xml->QueryFloatAttribute("desired_acceleration", &m_desired_acceleration);
	xml->QueryFloatAttribute("desired_deceleration", &m_desired_deceleration);
	xml->QueryFloatAttribute("initial_velocity", &m_initial_velocity);

	xml->QueryFloatAttribute("min_position", &min_position);
	xml->QueryFloatAttribute("max_position", &max_position);

	comp = xml->FirstChildElement("setpoints");
	if (comp != nullptr)
	{
    	XMLElement *setpoint_comp;
		setpoint_comp = comp->FirstChildElement("setpoint");
 		while (setpoint_comp != nullptr)
		{
			int setpoint_index = -1;
			setpoint_comp->QueryIntAttribute("index", &setpoint_index);
			if (setpoint_index >= 0 && setpoint_index < ShooterControl::NUM_SETPOINTS)
			{
				name = setpoint_comp->Attribute("name");
				if (name != nullptr)
				{
					m_setpoint_name[setpoint_index] = std::string(name);
				}
				else
				{
					m_setpoint_name[setpoint_index] = std::string("setpoint_") + std::to_string(setpoint_index);
					Advisory::pwarning("%s found unnamed setpoint, using default", getName().c_str());
				}

				setpoint_comp->QueryFloatAttribute("velocity", &m_setpoint_velocity[setpoint_index]);
				setpoint_comp->QueryFloatAttribute("percent", &m_setpoint_percent[setpoint_index]);

				Advisory::pinfo("%s  -- setpoint %2d: %20s   velocity=%7.2f, percent=%7.2f",getName().c_str(),
					setpoint_index, m_setpoint_name[setpoint_index].c_str(),
					m_setpoint_velocity[setpoint_index], m_setpoint_percent[setpoint_index]);

				if (num_setpoints < setpoint_index + 1)
				{
					num_setpoints = setpoint_index + 1;
				}
			}
			else
			{
				Advisory::pinfo("%s setpoint with index out of range -- %d", getName().c_str(), setpoint_index);
			}
    		setpoint_comp = setpoint_comp->NextSiblingElement("setpoint");
		}
	}

	comp = xml->FirstChildElement("limelight_curve");
			Advisory::pinfo(" limelight comp: %x ", comp);
	if (comp != nullptr)
	{
    	XMLElement *limelight_comp;
		limelight_comp = comp->FirstChildElement("line");
 		while (limelight_comp!=nullptr)
		{
			float distance = 0.0;
			float velocity = 0.0;
			limelight_comp->QueryFloatAttribute("distance", &distance);
			m_distanceFromTarget.push_back(distance);
			limelight_comp->QueryFloatAttribute("velocity", &velocity);
			m_mappedMotorVelocity.push_back(velocity);
			Advisory::pinfo(" -- limelight segment: distance = %7.2f motor velocity = %7.2",
					distance, velocity);
    		limelight_comp = limelight_comp->NextSiblingElement("line");
		}
		pwl01.setCurve(m_distanceFromTarget, m_mappedMotorVelocity);
 		pwl01.setLimitEnds(true);
	}

	comp = xml-> FirstChildElement("limelight");
	if (comp != NULL)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			m_limelight = HardwareFactory::createLimelight(comp);
			Advisory::pinfo("  Created limelight %s", name);
		}
		comp->QueryFloatAttribute("target_height", &m_targetHeight);
		Advisory::pinfo("  Target Height: %f", m_targetHeight);
		comp->QueryFloatAttribute("limelight_height", &m_limelightHeight);
		Advisory::pinfo("  Limelight Height: %f", m_limelightHeight);
		comp->QueryFloatAttribute("calibrated_distance", &m_calibratedDistance);
		Advisory::pinfo("  Calibrated Distance: %f", m_calibratedDistance);
	}


	comp = xml-> FirstChildElement("motor");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "flywheel_motor1") == 0)
			{
				Advisory::pinfo("%s  creating flywheel1 controller", getName().c_str());
				m_flywheel_motor1 = HardwareFactory::createMotor(comp);

				if(m_flywheel_motor1 != nullptr)
				{
					m_flywheel_motor1->setVelocity(0.0); 
				}
			}
			else if (strcmp(name, "flywheel_motor2") == 0)
			{
				Advisory::pinfo("%s  creating flywheel2 controller", getName().c_str());
				m_flywheel_motor2 = HardwareFactory::createMotor(comp);
				if(m_flywheel_motor2 != nullptr)
				{
					m_flywheel_motor2->setVelocity(0.0); 
				}
			}
			else if (strcmp(name, "kicker_motor") == 0)
			{
				Advisory::pinfo("%s  creating kicker controller", getName().c_str());
				m_kicker_motor = HardwareFactory::createMotor(comp);
				comp->QueryFloatAttribute("kicker_percent", &kicker_command_percent);
			}
			else
			{
				Advisory::pinfo("   unrecognized motor for %s", getName().c_str());
			}
		}
		else
		{
			Advisory::pwarning("%s  WARNING: required element \"motor\" not found", getName().c_str());
		}
		comp = comp->NextSiblingElement("motor");
		
	}

	comp = xml-> FirstChildElement("digital_output");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "flashlight") == 0)
			{
				Advisory::pinfo("%s  creating digital output flashlight", getName().c_str());
				m_flashlight = HardwareFactory::createDigitalOutput(comp);
			}
		}
		comp = comp->NextSiblingElement("digital_output");
	}

	comp = xml-> FirstChildElement("oi");
	while (comp != NULL)
	{
		name = comp->Attribute("name");
		if (name != NULL)
		{
		
			if ((strncmp(name, "setpoint", 8) == 0)
				&& (name[8] >= '0') && (name[8] <= '9'))
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_SETPOINT_0 + (name[8] - '0'));
				//comp->QueryFloatAttribute("ol_power", &(momentary_power[idx])); // sdr not sure if this is how to include, shouldnt be momentary
				//comp->QueryFloatAttribute("cl_step", &(momentary_step[idx]));	// sdr not sure if this is how to include, shouldnt be momentary

				// Advisory::pinfo("%s  olp = %f, cls = %f", getName().c_str(), momentary_power[idx], momentary_step[idx] );		

			}
			else if ((strncmp(name, "momentary", 9) == 0)
					&& (name[9] >= '0') && (name[9] <= '3'))
			{
				int idx = name[9] - '0'; // sdr so this is where it should be 0-7 instead of 0-9?

				Advisory::pinfo("%s  connecting %s channel [idx=%d]", getName().c_str(), name, idx);
				comp->QueryFloatAttribute("ol_power", &(momentary_power[idx]));
				comp->QueryFloatAttribute("cl_step", &(momentary_step[idx]));

				Advisory::pinfo("%s  olp = %f, cls = %f", getName().c_str(), momentary_power[idx], momentary_step[idx] );

				OIController::subscribeDigital(comp, this, CMD_MOMENTARY_0 + idx);
			}
			else if (strcmp(name, "closed_loop_state") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_STATE);
			}
			else if (strcmp(name, "stop_flywheel") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_STOP_FLYWHEEL);
			}
			else if (strcmp(name, "inc_flywheel") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_INCREMENT_FLYWHEEL);
			}
			else if (strcmp(name, "dec_flywheel") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_DECREMENT_FLYWHEEL);
			}
			else if (strcmp(name, "kicker_forward") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_KICKER_FORWARD);
			}
			else if (strcmp(name, "kicker_fwd_state") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_KICKER_FWD_STATE);
			}
			else if (strcmp(name, "kicker_backward") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_KICKER_BACKWARD);
			}
			else if (strcmp(name, "kicker_bwd_state") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_KICKER_BWD_STATE);
			}
			else if (strcmp(name, "stop_kicker") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_KICKER_STOP);
			}
			else if (strcmp(name, "flashlight_on") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_FLASHLIGHT_ON);
			}
			else if (strcmp(name, "flashlight_off") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_FLASHLIGHT_OFF);
			}
			else if (strcmp(name, "flashlight_toggle") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_FLASHLIGHT_TOGGLE);
			}
			else if (strcmp(name, "flashlight_state") == 0)
			{
				Advisory::pinfo("%s  connecting %s channel", getName().c_str(), name);
				OIController::subscribeDigital(comp, this, CMD_FLASHLIGHT_STATE);
			}
			else if (strcmp(name, "limelight_control_state") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_LIMELIGHT_STATE);
			}
			else
			{
				Advisory::pinfo("%s unknown oi %s", getName().c_str(), name);
			}
		}
		else
		{
			Advisory::pinfo("%s unnamed oi", getName().c_str());
		}
		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
ShooterControl::~ShooterControl(void)
{
	if (m_flywheel_motor1 != nullptr)
	{
		delete m_flywheel_motor1;
		m_flywheel_motor1 = nullptr;
	}

	if (m_flywheel_motor2 != nullptr)
	{
		delete m_flywheel_motor2;
		m_flywheel_motor2 = nullptr;
	}

	if (m_kicker_motor != nullptr)
	{
		delete m_kicker_motor;
		m_kicker_motor = nullptr;
	}

	if (m_flashlight != nullptr)
	{
		delete m_flashlight;
		m_flashlight = nullptr;
	}

	if (m_limelight != nullptr)
	{
		delete m_limelight;
		m_limelight = nullptr;
	}
	
	if (m_teleop_log != nullptr)
	{
	    delete m_teleop_log;
	    m_teleop_log = nullptr;
	}

	if (m_auton_log != nullptr)
	{
	    delete m_auton_log;
	    m_auton_log = nullptr;
	}

	m_active_log = nullptr;
}

/*******************************************************************************	
 *
 *
 *
 ******************************************************************************/
void ShooterControl::controlInit(void)
{
    bool is_ready = true;
	
	if(m_flywheel_motor1 == nullptr)
	{
		Advisory::pwarning("%s Shooter missing required component -- flywheel motor1", getName().c_str());
		is_ready = false;
	}
	else if(m_flywheel_motor2 == nullptr) 
	{
		Advisory::pwarning("%s Shooter missing required component -- flywheel motor2", getName().c_str());
		is_ready = false;
	}
	else if(m_kicker_motor == nullptr)
	{
		Advisory::pwarning("%s Shooter missing required component -- kicker motor", getName().c_str());
		is_ready = false;
	}
	/*else if(m_flashlight == nullptr)
	{
		Advisory::pwarning("%s Shooter missing required component -- flashlight", getName().c_str());
		is_ready = false;
	}*/
	else if(m_limelight == nullptr)
	{
		Advisory::pwarning("%s Shooter missing required component -- limelight", getName().c_str());
		is_ready = false;
	}

	m_is_ready = is_ready;
	m_heightDifference = m_targetHeight - m_limelightHeight;
}

/*******************************************************************************
 *
 *
 *
 ******************************************************************************/
void ShooterControl::updateConfig(void)
{
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void ShooterControl::setAnalog(int id, float val)
{
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void ShooterControl::setDigital(int id, bool val)
{ 
		Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);

	switch (id)
	{
		case CMD_CLOSED_LOOP_STATE:
		{
			setClosedLoop(val);

		} break;

		case CMD_LIMELIGHT_STATE:
		{
			setLimelightState(val);
		} break;

		case CMD_MOMENTARY_0:	applyMomentary(val, 0);  break;
		case CMD_MOMENTARY_1:	applyMomentary(val, 1);  break;
		case CMD_MOMENTARY_2:	applyMomentary(val, 2);  break;
		case CMD_MOMENTARY_3:	applyMomentary(val, 3);  break;

		case CMD_SETPOINT_0: 	applySetpoint(val, 0);  break;
		case CMD_SETPOINT_1: 	applySetpoint(val, 1);  break;
		case CMD_SETPOINT_2: 	applySetpoint(val, 2);  break;
		case CMD_SETPOINT_3: 	applySetpoint(val, 3);  break;
		case CMD_SETPOINT_4: 	applySetpoint(val, 4);  break;
		case CMD_SETPOINT_5: 	applySetpoint(val, 5);  break;
		case CMD_SETPOINT_6: 	applySetpoint(val, 6);  break;
        case CMD_SETPOINT_7:    applySetpoint(val, 7);  break;

		case CMD_STOP_FLYWHEEL:
		{
			if (val == true)
			{
				flywheel_target_percent = 0; 
				flywheel_target_velocity = 0;			
			}
		} break;

		case CMD_INCREMENT_FLYWHEEL:
		{
			if (val == true)
			{
				flywheel_target_percent += flywheel_percent_step;
				flywheel_target_velocity += flywheel_velocity_step;			
			}
		} break;

		case CMD_DECREMENT_FLYWHEEL:
		{
			if (val == true)
			{
				flywheel_target_percent -= flywheel_percent_step;
				flywheel_target_velocity -= flywheel_velocity_step;			
			}
		} break;

		case CMD_KICKER_STOP:
		{
			if (val == true)
			{
				kicker_target_percent = 0.0; 
			}
		} break;

		case CMD_KICKER_FORWARD:
		{
			if (val == true)
			{
				kicker_target_percent = kicker_command_percent;
			}
		} break;

		case CMD_KICKER_FWD_STATE:
		{
			if (val == true)
			{
				kicker_target_percent = kicker_command_percent;
			}
			else
			{
				kicker_target_percent = 0.0;
			}
			
		} break;

		case CMD_KICKER_BACKWARD:
		{
			if (val == true)
			{
				kicker_target_percent = -kicker_command_percent;
			}
		} break;

		case CMD_KICKER_BWD_STATE:
		{
			if (val == true)
			{
				kicker_target_percent = -kicker_command_percent;
			}
			else
			{
				kicker_target_percent = 0.0;
			}
			
		} break;

		case CMD_FLASHLIGHT_ON:
		{
			if (val == true)
			{
				m_flashlight_state = true;
			}
		} break;

		case CMD_FLASHLIGHT_OFF:
		{
			if (val != true)
			{
				m_flashlight_state = false;
			}
		} break;

		case CMD_FLASHLIGHT_TOGGLE:
		{
			if (val == true)
			{
				m_flashlight_state = !m_flashlight_state;
			}
		} break;

		case CMD_FLASHLIGHT_STATE:
		{
			m_flashlight_state = val;
		} break;

		default:
		{
			// Do Nothing
		} break;
	}
}

/*******************************************************************************	
 *
 ******************************************************************************/
void ShooterControl::setFlywheelPower(float percent)
{
	flywheel_target_percent = percent;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void ShooterControl::setKickerPower(float percent)
{
	kicker_target_percent = percent;
}

/*******************************************************************************	
 *
 * This is the callback for OIController::subscribeInt, if the XML config
 * specifies an int, the constructor of this object will connect that
 * input to this method.
 *
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the int channel that was subscribed to
 *
 ******************************************************************************/
void ShooterControl::setInt(int id, int val)

{/*
// Copied from VelocityJointControl, idk if this is needed
	Advisory::pinfo("%s %s(%d, %d)", getName().c_str(), __FUNCTION__, id, val);
	if (id == CMD_SETPOINT_IDX)
	{
		if (val >= 0)
		{
			applySetpoint( true, val/45);
		}
		else
		{
			applySetpoint( false, val);
		}
	}*/
}


/*******************************************************************************
 *
 ******************************************************************************/
void ShooterControl::applyMomentary(bool on, int idx)
{ /*
	if (on)
	{
		Advisory::pinfo("flywheel raw pos = %f flywheel actual velocity = %f", flywheel_raw_position, flywheel_actual_velocity * 4096 / 600); // sdr changed pos -> raw pos, dunno if number is right
		flywheel_target_percent = momentary_power[idx];

		Advisory::pinfo("kicker raw pos = %f kicker actual velocity = %f", kicker_raw_position, kicker_actual_velocity * 4096 / 600); // sdr changed pos -> raw pos, dunno if number is right
		kicker_target_percent = momentary_power[idx];
		// sdr i dont know what im doin 
	}
	else
	{
		flywheel_target_percent = 0.0;
	}

	Advisory::pinfo("%s ShooterControl::applyMomentary(on=%d, idx=%d) fly_p=%f kick_p=%f", getName().c_str(), on, idx, flywheel_target_percent, kicker_target_percent);
*/
}

/*******************************************************************************
 *
 ******************************************************************************/
void ShooterControl::applySetpoint(bool on, int idx)
{
	if (on && (idx >= 0) && (idx < num_setpoints))
	{
		setpoint_index = idx;
		flywheel_target_velocity = m_setpoint_velocity[idx]; 
		flywheel_target_percent = m_setpoint_percent[idx];  

		// RJP should also have ol -- target_power
		Advisory::pinfo("flywheel rev/s = %f", flywheel_target_velocity);
	    Advisory::pinfo("%s applySetpoint: on=%d, idx=%d, setpoint_vel=%f, setpoint_percent=%f, fly_targ=%f, kick_targ=%f", getName().c_str(), on, idx, m_setpoint_velocity[idx], m_setpoint_percent[idx], flywheel_target_velocity, kicker_target_percent);
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void ShooterControl::setVelocity(float val)
{
	flywheel_target_velocity = val;
}

/*******************************************************************************
 *
 ******************************************************************************/
float ShooterControl::getVelocity(void)
{
	return flywheel_actual_velocity; // TODO: Which one do we want here?
	return kicker_actual_velocity;
}

/*******************************************************************************
 *
 ******************************************************************************/
void ShooterControl::disabledInit()
{
    m_teleop_log->close();
    m_auton_log->close();
	flywheel_target_velocity = 0.0;
	kicker_target_percent = 0.0;
	m_flashlight_state = false;
}

/*******************************************************************************
 *
 ******************************************************************************/
void ShooterControl::autonomousInit()
{
	Advisory::pinfo("%s autonomousInit", getName().c_str());
    ShooterControl::initLogFile();
	m_active_log = m_auton_log;
}

/*******************************************************************************
 *
 ******************************************************************************/
void ShooterControl::teleopInit()
{
    ShooterControl::initLogFile();
	m_active_log = m_teleop_log;
}

/*******************************************************************************
 *
 ******************************************************************************/
void ShooterControl::testInit()
{
}

/**********************************************************************
 *
 * This method is used to initialize the log files, called from
 * teleop init and auton init methods
 *
 **********************************************************************/
void ShooterControl::initLogFile(void)
{
	m_active_log->openSegment();

	m_active_log->log("%s, %s, %s, %s, %s, %s, ",
                    "current_time",
                    "flywheel_raw_position",
                    "flywheel_raw_velocity", "flywheel_actual_velocity",
					"flywheel_target_percent");
	m_active_log->log("%s, %s, %s, %s, %s, %s, ",
					"kicker_raw_position",
                    "kicker_raw_velocity", "kicker_actual_velocity",
					"kicker_target_percent", "kicker_command_percent");

    // shooter_log->log("\n");
    m_active_log->flush();
} 

/*******************************************************************************
 *
 ******************************************************************************/
void ShooterControl::publish()
{
	SmartDashboard::PutBoolean(std::string("  ") + getName().c_str() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());

	SmartDashboard::PutBoolean(getName() +" closed loop: ", m_closed_loop);
	
	SmartDashboard::PutBoolean(getName() + " limelight control: ", is_limelight_control);

	SmartDashboard::PutNumber(getName() +" flywheel raw_position: ", flywheel_raw_position);
	SmartDashboard::PutNumber(getName() +" flywheel actual velocity: ", flywheel_actual_velocity);
	SmartDashboard::PutNumber(getName() +" flywheel target velocity: ", flywheel_target_velocity);
	SmartDashboard::PutNumber(getName() +" flywheel target power: ", flywheel_target_percent);
	SmartDashboard::PutNumber(getName() +" flywheel command power: ", flywheel_command_percent);

	SmartDashboard::PutNumber(getName() +" kicker raw_position: ", kicker_raw_position);
	SmartDashboard::PutNumber(getName() +" kicker actual velocity: ", kicker_actual_velocity);
	SmartDashboard::PutNumber(getName() +" kicker target percent: ", kicker_target_percent);
	SmartDashboard::PutNumber(getName() +" kicker command percent: ", kicker_command_percent);
	SmartDashboard::PutNumber(getName() +" setpoint idx: ", setpoint_index);
	
	SmartDashboard::PutNumber(getName() + " Limelight Horizontal: ", m_targetOffsetAngle_Horizontal);
	SmartDashboard::PutNumber(getName() + " Limelight Vertical: ", m_targetOffsetAngle_Vertical);
	SmartDashboard::PutNumber(getName() + " Limelight Area: ", m_targetArea);
	SmartDashboard::PutNumber(getName() + " Limelight Skew: ", m_targetSkew);
	SmartDashboard::PutNumber(getName() + " Limelight Distance Offset: ", m_distanceOffset);



}

/*******************************************************************************
 *
 ******************************************************************************/
void ShooterControl::setClosedLoop(bool closed)
{
	if (m_closed_loop != closed)
	{
		m_closed_loop = closed;

		if (m_closed_loop)
		{
			if(m_flywheel_motor1 != nullptr)
			{
				m_flywheel_motor1->setControlMode(Motor::VELOCITY);
			}
			if(m_flywheel_motor2 != nullptr)
			{
				m_flywheel_motor2->setControlMode(Motor::VELOCITY);
			}
		    Advisory::pinfo("setting Shooter closed loop mode");
		}
		else
		{
			if(m_flywheel_motor1 != nullptr)
			{
				m_flywheel_motor1->setControlMode(Motor::PERCENT);
			}
			if(m_flywheel_motor2 != nullptr)
			{
				m_flywheel_motor2->setControlMode(Motor::PERCENT);
			}

            Advisory::pinfo("setting Shooter open loop mode");
		}
	}
}
/*******************************************************************************
 *
 ******************************************************************************/
void ShooterControl::setLimelightState(bool value)
{
	if (is_limelight_control != value)
	{
		is_limelight_control = value;

		if (is_limelight_control)
		{
		    Advisory::pinfo("setting %s limelight control mode", getName().c_str());
		    m_flywheel_motor1->setControlMode(Motor::VELOCITY); 
			m_flywheel_motor2->setControlMode(Motor::VELOCITY); 
		}
		else
		{
            Advisory::pinfo("setting %s open loop mode", getName().c_str());
        	m_flywheel_motor1->setControlMode(Motor::PERCENT); 
			m_flywheel_motor2->setControlMode(Motor::PERCENT); 
		}
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
bool ShooterControl::isClosedLoop(void)
{
	return m_closed_loop;
}
/*******************************************************************************
 *
 ******************************************************************************/
bool ShooterControl::isAtTarget(float tolerance)
{
	return (fabs(flywheel_actual_velocity - flywheel_target_velocity) < tolerance);
	// return (fabs(kicker_actual_velocity - kicker_target_velocity) < tolerance);
}
/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void ShooterControl::doPeriodic()
{
	if(m_is_ready == false)
	{
		Advisory::pinfo("DoPeriodic is not ready");
		return;
	}

	double current_time = gsi::Time::getTime();
	m_flywheel_motor1->doUpdate();
	m_flywheel_motor2->doUpdate();
	m_kicker_motor->doUpdate();

	// for debugging
	flywheel_raw_position = m_flywheel_motor1->getRawPosition();
	flywheel_raw_velocity = m_flywheel_motor1->getRawVelocity();
	flywheel_actual_velocity = m_flywheel_motor1->getVelocity();

	flywheel_raw_position = m_flywheel_motor2->getRawPosition();
	flywheel_raw_velocity = m_flywheel_motor2->getRawVelocity();
	flywheel_actual_velocity = m_flywheel_motor2->getVelocity();

	//
	// Make sure the motor doesn't jump when enabled
	//
	if (getPhase() == DISABLED)
	{
		flywheel_target_velocity = 0.0; 
		flywheel_target_percent = 0.0;
		flywheel_command_percent = 0.0;
		kicker_target_percent = 0.0;
		m_flashlight_state = false;
	}

	if(flywheel_target_percent < 0.2)
	{
		kicker_target_percent = RobotUtil::limit(-1.0, 0.0,
			kicker_target_percent);
	}
	
	//
	// We want to be able to see this data in SmartDashboard
	// without triggering the limelight control
	//
	if(m_limelight != nullptr)
	{
		m_limelight->getData(m_target_visible,
							m_targetOffsetAngle_Horizontal,
							m_targetOffsetAngle_Vertical,
							m_targetArea,
							m_targetSkew);
	}

	if (is_limelight_control)
	{
		if(m_limelight != nullptr)
		{
			// d = (h2-h1)/tan(a1+a2)
			if (fabs(m_targetOffsetAngle_Vertical) > 0.25)
			{
				m_distanceOffset = m_calibratedDistance - m_heightDifference/tan(m_targetOffsetAngle_Vertical);
				flywheel_target_velocity = pwl01.evaluate(m_distanceOffset);
				m_flywheel_motor1->setVelocity(flywheel_target_velocity);
				m_flywheel_motor2->setVelocity(flywheel_target_velocity);
			}
			else
			{
				m_distanceOffset = 0;
				flywheel_target_velocity = pwl01.evaluate(m_distanceOffset);
				m_flywheel_motor1->setVelocity(flywheel_target_velocity);
				m_flywheel_motor2->setVelocity(flywheel_target_velocity);			
			}

			Advisory::pinfo("Distance Offset: %.2f", m_distanceOffset);

		}
	}

	else if (m_closed_loop)
	{
		m_flywheel_motor1->setVelocity(flywheel_target_velocity);
		m_flywheel_motor2->setVelocity(flywheel_target_velocity);
		m_kicker_motor->setPercent(kicker_target_percent);
	}
	else
	{
        m_flywheel_motor1->setPercent(flywheel_target_percent);
        m_flywheel_motor2->setPercent(flywheel_target_percent);
		m_kicker_motor->setPercent(kicker_target_percent);
	}


	m_flashlight->Set(m_flashlight_state);

	m_active_log->log("%6.8f, %6.8f, %6.8f, %6.8f, %6.8f,",
	                current_time,
	                flywheel_raw_position, 
					flywheel_raw_velocity, flywheel_actual_velocity, 
					flywheel_target_percent);
	m_active_log->log("%6.8f, %6.8f, %6.8f,",
					kicker_raw_position, 
					kicker_raw_velocity, kicker_actual_velocity, 
					kicker_target_percent, kicker_command_percent);
}

/*******************************************************************************
 *
 ******************************************************************************/
MSShooter::MSShooter(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	flywheel_target_percent = 0.0;
	kicker_target_percent = 0.0;
	m_wait = false;
	setpoint_index = 7;
	m_closed_loop = false;
	
	m_parent_control = (ShooterControl *)control;
	xml->QueryFloatAttribute("flywheel_per", &flywheel_target_percent);
	xml->QueryFloatAttribute("kicker_per", &kicker_target_percent);
	xml->QueryIntAttribute("setpoint", &setpoint_index);
	xml->QueryBoolAttribute("closed_loop", &m_closed_loop);
	xml->QueryBoolAttribute("wait", &m_wait);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSShooter::init(void)
{
	m_parent_control->setClosedLoop(m_closed_loop);
	if (setpoint_index < NUM_SETPOINTS - 1)
	{
		m_parent_control->applySetpoint(setpoint_index, setpoint_index + 1);
	}
	else
	{
		m_parent_control->applySetpoint(setpoint_index, NUM_SETPOINTS - 1);
	}	
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooter::update(void)
{
    if(m_wait == true)
	{
	    if(!m_parent_control->isAtTarget(1.0))
	 	{
			return this;
	 	}
	}

	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSShooterFlywheelPower::MSShooterFlywheelPower(std::string type,
    tinyxml2::XMLElement *xml, void *control) 
	: MacroStepSequence(type, xml, control)
{
	m_flywheel_percent = 0.0;
	m_parent_control = (ShooterControl *)control;
	xml->QueryFloatAttribute("flywheel_percent", &m_flywheel_percent);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterFlywheelPower::init(void)
{
	m_parent_control->setFlywheelPower(m_flywheel_percent);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterFlywheelPower::update(void)
{
	return next_step;
}

/*******************************************************************************
 *
 ******************************************************************************/
MSShooterKickerPower::MSShooterKickerPower(std::string type,
    tinyxml2::XMLElement *xml, void *control) 
	: MacroStepSequence(type, xml, control)
{
	m_kicker_percent = 0.0;
	m_parent_control = (ShooterControl *)control;
	xml->QueryFloatAttribute("kicker_percent", &m_kicker_percent);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSShooterKickerPower::init(void)
{
	m_parent_control->setKickerPower(m_kicker_percent);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSShooterKickerPower::update(void)
{
	return next_step;
}
