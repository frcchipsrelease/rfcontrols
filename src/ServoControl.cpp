/*******************************************************************************
 *
 * File: ServoControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfcontrols//ServoControl.h"

#include "rfhardware/HardwareFactory.h"

#include "rfutilities/RobotUtil.h"
#include "rfutilities/MacroStepFactory.h"

#include "gsutilities/Advisory.h"

#include "frc/smartDashboard/SmartDashboard.h"

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of this object and configure it based on the provided
 * XML, period, and priority
 * 
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 						
 * @param	period		the time (in seconds) between calls to update()
 * @param	priority	the priority at which this thread/task should run
 * 
 ******************************************************************************/
ServoControl::ServoControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Servo Control [%s] =========================", control_name.c_str());

	m_servo = nullptr;

	m_servo_step_size = 5.0;
	m_servo_max_up_position = 180.0;
	m_servo_max_down_position = -180.0;
	m_servo_setpoint_up_position = 60.0;
	m_servo_setpoint_mid_position = 0.0;
	m_servo_setpoint_down_position = -60.0;

	m_servo_target_velocity = 0.0;

	//
	// Register Macro Steps
	//
	//new MacroStepProxy<MSServoPBCSetState>(control_name, "SetState", this);

	//
	// Parse the XML
	//
	xml->QueryFloatAttribute("step_size", &m_servo_step_size);
	xml->QueryFloatAttribute("max_up", &m_servo_max_up_position);
	xml->QueryFloatAttribute("max_down", &m_servo_max_down_position);
	xml->QueryFloatAttribute("setpoint_up", &m_servo_setpoint_up_position);
	xml->QueryFloatAttribute("setpoint_mid", &m_servo_setpoint_mid_position);
	xml->QueryFloatAttribute("setpoint_down", &m_servo_setpoint_down_position);
	m_servo_target_position = m_servo_setpoint_mid_position;

	m_servo_setpoint_up_position = RobotUtil::limit(m_servo_max_down_position, m_servo_setpoint_up_position, m_servo_setpoint_up_position);
	m_servo_setpoint_mid_position = RobotUtil::limit(m_servo_max_down_position, m_servo_setpoint_up_position, m_servo_setpoint_mid_position);
	m_servo_setpoint_down_position = RobotUtil::limit(m_servo_max_down_position, m_servo_setpoint_up_position, m_servo_setpoint_down_position);

	comp = xml->FirstChildElement("servo");
	if (comp != nullptr)
	{
			Advisory::pinfo("  creating servo");
			m_servo = HardwareFactory::createServo(comp);
	}
	
	Advisory::pinfo("    servo created with step_size=%5.1f, max_up=%5.1f, max_down=%5.1f, setpoint_up=%5.1f, setpoint_mid=%5.1f, setpoint_down=%5.1f",
	    m_servo_step_size, m_servo_max_up_position, m_servo_max_down_position, 
		m_servo_setpoint_up_position, m_servo_setpoint_mid_position, m_servo_setpoint_down_position);

	const char *name;
	comp = xml->FirstChildElement("oi");
	while (comp != NULL)
	{
		name = comp->Attribute("name");
		if (name != NULL)
		{
			if (strcmp(name, "step_up") == 0)
			{
				Advisory::pinfo("  connecting step_up channel");
				OIController::subscribeDigital(comp, this, CMD_STEP_UP);
			}
			else if (strcmp(name, "step_down") == 0)
            {
			     Advisory::pinfo("  connecting step_down channel");
			     OIController::subscribeDigital(comp, this, CMD_STEP_DOWN);
			}
			else if (strcmp(name, "setpoint_up") == 0)
            {
			     Advisory::pinfo("  connecting setpoint_up channel");
			     OIController::subscribeDigital(comp, this, CMD_SETPOINT_UP);
			}
			else if (strcmp(name, "setpoint_mid") == 0)
            {
			     Advisory::pinfo("  connecting setpoint_mid channel");
			     OIController::subscribeDigital(comp, this, CMD_SETPOINT_MID);
			}
			else if (strcmp(name, "setpoint_down") == 0)
            {
			     Advisory::pinfo("  connecting setpoint_down channel");
			     OIController::subscribeDigital(comp, this, CMD_SETPOINT_DOWN);
			}
			else if (strcmp(name, "servo_state") == 0)
			{
			     Advisory::pinfo("  connecting servo_state channel");
			     OIController::subscribeDigital(comp, this, CMD_STATE);
			}
			else if (strcmp(name, "servo_toggle") == 0)
			{
			     Advisory::pinfo("  connecting servo_toggle channel");
			     OIController::subscribeDigital(comp, this, CMD_TOGGLE);
			}
            else if (strcmp(name, "servo_velocity") == 0)
			{
			     Advisory::pinfo("  connecting servo velocity channel");
			     OIController::subscribeAnalog(comp, this, CMD_VELOCITY);
			}
		}
		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 *
 * Release any resources used by this object
 * 
 ******************************************************************************/
ServoControl::~ServoControl(void)
{
	if (m_servo != nullptr)
	{
		delete m_servo;
		m_servo = nullptr;
	}
}

/*******************************************************************************	
 *
 *
 *
 ******************************************************************************/
void ServoControl::controlInit(void)
{

	if (m_servo == nullptr)
	{
		Advisory::pwarning("  WARNING: servo element \"servo\" not found");
	}
}

/*******************************************************************************
 *
 * Sets the state of the Servo based on the command id and value
 * 
 * @param	id	the command id that indicates what the val argument means
 * @param	val	the new value
 * 
 ******************************************************************************/
void ServoControl::setDigital(int id, bool button_pressed)
{
	Advisory::pinfo("ServoControl::setDigital(id=%d, val=%d)", id, button_pressed);

	switch (id)
	{
		case CMD_STEP_UP:
		{
			if(button_pressed)
			{
				m_servo_target_position = RobotUtil::limit(m_servo_max_down_position, m_servo_max_up_position, m_servo_target_position + m_servo_step_size);
			}
		} break;

		case CMD_STEP_DOWN:
		{
			if(button_pressed)
			{
				m_servo_target_position = RobotUtil::limit(m_servo_max_down_position, m_servo_max_up_position, m_servo_target_position - m_servo_step_size);
			}
		} break;

        case CMD_SETPOINT_UP:
		{
			if(button_pressed)
			{
				m_servo_target_position = m_servo_setpoint_up_position;
			}
		} break;

		case CMD_SETPOINT_MID:
		{
			if(button_pressed)
			{
				m_servo_target_position = m_servo_setpoint_mid_position;
			}
		} break;

		case CMD_SETPOINT_DOWN:
		{
			if(button_pressed)
			{
				m_servo_target_position = m_servo_setpoint_down_position;
			}
		} break;

		case CMD_STATE:
		{
			if(button_pressed)
			{
				m_servo_target_position = m_servo_setpoint_up_position;
			}
			else
			{
				m_servo_target_position = m_servo_setpoint_down_position;
			}
		} break;

		case CMD_TOGGLE:
		{
			if(button_pressed)
			{
				if(m_servo_target_position == m_servo_setpoint_down_position)
				{
					m_servo_target_position = m_servo_setpoint_up_position;
				}
				else
				{
					m_servo_target_position = m_servo_setpoint_down_position;
				}
			}
		} break;
	}
}

/*******************************************************************************	
 *
 * Set an analog value
 * 
 * @param	id	the id of the value to set
 * @param	val	the new value
 * 
 ******************************************************************************/
void ServoControl::setAnalog(int id, float val)
{
	switch(id)
	{
		case CMD_VELOCITY:
		{
			if (fabs(val) < 0.1)
			{
				m_servo_target_velocity=0.0;
			}
			else
			{
				m_servo_target_velocity=val;
			}
			
		} break;
	}
}

/*******************************************************************************	
 *
 * 
 *
 ******************************************************************************/
void ServoControl::setInt(int id, int val)
{
}

/*******************************************************************************	
 *
 * 
 *
 ******************************************************************************/
void ServoControl::disabledInit(void)
{

}

/*******************************************************************************	
 *
 * State to false for start of auton
 *
 ******************************************************************************/
void ServoControl::autonomousInit(void)
{

}

/*******************************************************************************
 *
 * Set state to false for start of teleop
 * 
 ******************************************************************************/
void ServoControl::teleopInit(void)
{

}

/*******************************************************************************	
 *
 * Set state to false for start of test
 *
 ******************************************************************************/
void ServoControl::testInit(void)
{

}

/*******************************************************************************
 *
 * Sends data to dashboard
 *
 ******************************************************************************/
void ServoControl::publish()
{
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() + " target position: ", m_servo_target_position);
}

/*******************************************************************************
 *
 * Sets Servo to a boolean value each period
 *
 ******************************************************************************/
void ServoControl::doPeriodic()
{
	m_servo_target_position = RobotUtil::limit(m_servo_max_up_position, m_servo_max_down_position,
			m_servo_target_position + m_servo_target_velocity);

	if (m_servo != nullptr)
	{
		m_servo->SetAngle(m_servo_target_position);
	}
}
