/*******************************************************************************
 *
 * File: SimpleArcadeDriveControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfcontrols/SimpleArcadeDriveControl.h"

#include "rfhardware/HardwareFactory.h"

#include "rfutilities/MacroStepFactory.h"
#include "rfutilities/RobotUtil.h"

#include "gsutilities/Advisory.h"

#include "gsinterfaces/Time.h"

#include "frc/smartDashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of this object and configure it based on the provided
 * XML, period, and priority    
 * 
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 						
 * @param	period		the time (in seconds) between calls to update()
 * @param	priority	the priority at which this thread/task should run
 * 
 * *****************************************************************************/
SimpleArcadeDriveControl::SimpleArcadeDriveControl(std::string control_name, XMLElement* xml)
	:	PeriodicControl(control_name)
{
    Advisory::pinfo("========================= Creating Simple Arcade Drive Control [%s] =========================",
                control_name.c_str());
	XMLElement *comp;

	const char *name;

    m_data_logger = new DataLogger("/robot/logs", "drive", "csv", 50, true);

	fl_drive_motor = nullptr;
	fr_drive_motor = nullptr;
	bl_drive_motor = nullptr;
	br_drive_motor = nullptr;

	fl_drive_motor_cmd = 0.0;
	fr_drive_motor_cmd = 0.0;
	bl_drive_motor_cmd = 0.0;
	br_drive_motor_cmd = 0.0;

	trn_power = 0.0;
	fwd_power = 0.0;

	m_low_power_scale = 0.5;
	m_low_power_active = false;
	
	name = xml->Attribute("name");
	if (name == nullptr)
	{
		name="drive";
		Advisory::pcaution(  "WARNING: SimpleDriveArcade created without name, assuming \"%s\"", name);
	}

	//
	// Register Macro Steps
	//
	new MacroStepProxy<MSDriveSimpleArcadeDrivePower>(control_name, "DrivePower", this);

	xml->QueryFloatAttribute("low_power_scale", &m_low_power_scale);

	//
	// Parse the XML
	//
	comp = xml-> FirstChildElement("motor");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "front_left") == 0)
			{				
				Advisory::pinfo("  creating speed controller for %s", name);
                fl_drive_motor = HardwareFactory::createMotor(comp);
				//fl_drive_motor_invert = comp->BoolAttribute("invert") ? -1.0 : 1.0;
			}
			else if (strcmp(name, "front_right") == 0)
			{
			    Advisory::pinfo("  creating speed controller for %s", name);
                fr_drive_motor = HardwareFactory::createMotor(comp);
				//fr_drive_motor_invert = comp->BoolAttribute("invert") ? -1.0 : 1.0;
			}
			else if (strcmp(name, "back_left") == 0)
			{
			    Advisory::pinfo("  creating speed controller for %s", name);
                bl_drive_motor = HardwareFactory::createMotor(comp);
				//bl_drive_motor_invert = comp->BoolAttribute("invert") ? -1.0 : 1.0;
			}
			else if (strcmp(name, "back_right") == 0)
			{
				Advisory::pinfo("  creating speed controller for %s", name);
                br_drive_motor = HardwareFactory::createMotor(comp);
				//br_drive_motor_invert = comp->BoolAttribute("invert") ? -1.0 : 1.0;
			}
		}
		comp = comp->NextSiblingElement("motor");
	}


    comp = xml-> FirstChildElement("oi");
	while (comp != NULL)
	{
		name = comp->Attribute("name");
		if (name != NULL)
		{
            
			if (strcmp(name, "forward") == 0)
			{
				Advisory::pinfo("  connecting to forward channel   ");
                OIController::subscribeAnalog(comp, this, CMD_FORWARD);
			}
			else if (strcmp(name, "turn") == 0)
			{
				Advisory::pinfo("  connecting to turn channel");
				OIController::subscribeAnalog(comp, this, CMD_TURN);
			}
            
			else if (strcmp(name, "low_power_on") == 0)
			{
				Advisory::pinfo("  connecting to low_power on channel");
				OIController::subscribeDigital(comp, this, CMD_LOW_POWER_ON);
			}
			else if (strcmp(name, "low_power_off") == 0)
			{
				Advisory::pinfo("  connecting to low_power off channel");
				OIController::subscribeDigital(comp, this, CMD_LOW_POWER_OFF);
			}
			else if (strcmp(name, "low_power_toggle") == 0)
			{
				Advisory::pinfo("  connecting to low_power toggle channel");
				OIController::subscribeDigital(comp, this, CMD_LOW_POWER_TOGGLE);
			}
			else if (strcmp(name, "low_power_state") == 0)
			{
				Advisory::pinfo("  connecting to low_power state channel");
				OIController::subscribeDigital(comp, this, CMD_LOW_POWER_STATE);
			}
			else 
			{
				Advisory::pwarning("  unrecognized oi detected: \"%s\"", name);
			}
            
		}
		comp = comp->NextSiblingElement("oi");
	}

}

/*******************************************************************************	
 *
 * Release any resources used by this object
 * 
 ******************************************************************************/
SimpleArcadeDriveControl::~SimpleArcadeDriveControl(void)
{
	if (fl_drive_motor != nullptr)
	{
		delete fl_drive_motor;
		fl_drive_motor = nullptr;
	}
	
	if (fr_drive_motor != nullptr)
	{
		delete fr_drive_motor;
		fr_drive_motor = nullptr;
	}
	
	if (bl_drive_motor != nullptr)
	{
		delete bl_drive_motor;
		bl_drive_motor = nullptr;
	}
	
	if (br_drive_motor != nullptr)
	{
		delete br_drive_motor;
		br_drive_motor = nullptr;
	}
   
}

/*******************************************************************************	
 *
 ******************************************************************************/
void SimpleArcadeDriveControl::controlInit()
{
	fwd_power = 0.0;
	trn_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void SimpleArcadeDriveControl::disabledInit()
{
    m_data_logger->close();

	fwd_power = 0.0;
	trn_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void SimpleArcadeDriveControl::autonomousInit()
{
    logFileInit("Auton");

    fwd_power = 0.0;
	trn_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void SimpleArcadeDriveControl::teleopInit()
{
    logFileInit("Teleop");

    fwd_power = 0.0;
	trn_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void SimpleArcadeDriveControl::testInit()
{
    logFileInit("Test");

    fwd_power = 0.0;
	trn_power = 0.0;
}

/*******************************************************************************
 *
 ******************************************************************************/
void SimpleArcadeDriveControl::setInt(int id, int val)
{
}

/*******************************************************************************
 *
 * Sets the state of the control based on the command id and value
 * 
 * Handled command ids are CMD_TURN and CMD_FORWARD, all others are ignored
 * 
 * @param	id	the command id that indicates what the val argument means
 * @param	val	the new value
 * 
 ******************************************************************************/
void SimpleArcadeDriveControl::setAnalog(int id, float val)
{
	switch (id)
	{
		case CMD_TURN:
     	{
			trn_power = val;
		} break;

		case CMD_FORWARD:
		{
			fwd_power = val;
		} break;

	}
}

/*******************************************************************************	
 *
 * Sets the state of the control based on the command id and value
 *  
 * Handled command ids are CMD_BRAKE_[ON|OFF|TOGGLE|STATE] and 
 * CMD_GEAR_[HIGH|LOW|TOGGLE|STATE], all others are ignored
 *
 * @param	id	the command id that indicates what the val argument means
 * @param	val	the new value
 * 
 ******************************************************************************/
void SimpleArcadeDriveControl::setDigital(int id, bool button_pressed)
{
	switch(id)
	{	
		case CMD_LOW_POWER_ON:
		{
			if (button_pressed) m_low_power_active = true;
		} break;

		case CMD_LOW_POWER_OFF:
		{
			if (button_pressed) m_low_power_active = false;
		} break;

		case CMD_LOW_POWER_TOGGLE:
		{
			if (button_pressed) m_low_power_active = !m_low_power_active;
		} break;

		case CMD_LOW_POWER_STATE:
		{
			m_low_power_active = button_pressed;
		} break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void SimpleArcadeDriveControl::publish()
{
//	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
//	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() +" forward: ", fwd_power);
	SmartDashboard::PutNumber(getName() +" turn: ", trn_power);

	if (fl_drive_motor != nullptr)
	{
		SmartDashboard::PutNumber(getName() +" fl cmd: ", fl_drive_motor_cmd);
	}

	if (fr_drive_motor != nullptr)
	{
		SmartDashboard::PutNumber(getName() +" fr cmd: ", fr_drive_motor_cmd);
	}

	if (bl_drive_motor != nullptr)
	{
		SmartDashboard::PutNumber(getName() +" bl cmd: ", bl_drive_motor_cmd);
	}

	if (br_drive_motor != nullptr)
	{
		SmartDashboard::PutNumber(getName() +" br cmd: ", br_drive_motor_cmd);
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void SimpleArcadeDriveControl::logFileInit(std::string phase)
{
    Advisory::pinfo("initializing log file for %s", phase.c_str());

    m_data_logger->openSegment();

    m_data_logger->log("%s, %s, %s, ",
        "time", "phase", "pet"
    );

    m_data_logger->log("%s, %s, %s, %s, %s, %s ",
        "fwd_power", "trn_power", 
        "fl_drive_motor_cmd", "fr_drive_motor_cmd", "bl_drive_motor_cmd", "br_drive_motor_cmd");

    m_data_logger->log("\n");

    m_data_logger->flush();
}

/*******************************************************************************
 *
 ******************************************************************************/
void SimpleArcadeDriveControl::logFileAppend(void)
{
    m_data_logger->log("%f, %d, %f, ",
        gsi::Time::getTime(), this->getPhase(), this->getPhaseElapsedTime());

    m_data_logger->log("%f, %f,  %f, %f, %f, %f,  ",
        fwd_power, trn_power, 
        fl_drive_motor_cmd, fr_drive_motor_cmd, bl_drive_motor_cmd,br_drive_motor_cmd);

    m_data_logger->log("\n");
}

/*******************************************************************************	
 * 
 * Sets the actuator values every period
 * 
 ******************************************************************************/


void SimpleArcadeDriveControl::doPeriodic()
{
	//
	// Calculate Values
	//
	fl_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power - trn_power);
	fr_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power + trn_power);
	bl_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power - trn_power);
	br_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power + trn_power);

    // Normalize the drive command values
	float normalize_max = fabs(fl_drive_motor_cmd);
	if (fabs(fr_drive_motor_cmd) > normalize_max) { normalize_max = fabs(fr_drive_motor_cmd); }
	if (fabs(bl_drive_motor_cmd) > normalize_max) { normalize_max = fabs(bl_drive_motor_cmd); }
	if (fabs(br_drive_motor_cmd) > normalize_max) { normalize_max = fabs(br_drive_motor_cmd); }

    if (normalize_max > 1.0)
	{
		fl_drive_motor_cmd /= normalize_max;
		fr_drive_motor_cmd /= normalize_max;
	    bl_drive_motor_cmd /= normalize_max;
        br_drive_motor_cmd /= normalize_max;
	}

    // apply low power factor
	if (m_low_power_active)
	{
		fl_drive_motor_cmd *= m_low_power_scale;
		fr_drive_motor_cmd *= m_low_power_scale;
	    bl_drive_motor_cmd *= m_low_power_scale;
        br_drive_motor_cmd *= m_low_power_scale;
	}

	//
	// Set Outputs
	//
	
	if (fl_drive_motor != nullptr)
	{
		fl_drive_motor->setPercent(fl_drive_motor_cmd);
		fl_drive_motor->doUpdate();
	}
	
	if (fr_drive_motor != nullptr)
	{
		fr_drive_motor->setPercent(fr_drive_motor_cmd);
		fr_drive_motor->doUpdate();
	}
	
	if (bl_drive_motor != nullptr)
	{
		bl_drive_motor->setPercent(bl_drive_motor_cmd);
		bl_drive_motor->doUpdate();
	}
	
	if (br_drive_motor != nullptr)
	{
		br_drive_motor->setPercent(br_drive_motor_cmd);
		br_drive_motor->doUpdate();
	}	

	logFileAppend();
}




// =============================================================================
// =============================================================================


/*******************************************************************************
 *
 ******************************************************************************/
MSDriveSimpleArcadeDrivePower::MSDriveSimpleArcadeDrivePower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (SimpleArcadeDriveControl *)control;

	forward_power = xml->FloatAttribute("forward");
	turn_power = xml->FloatAttribute("turn");
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDriveSimpleArcadeDrivePower::init(void)
{
	parent_control->setAnalog(SimpleArcadeDriveControl::CMD_FORWARD, forward_power);
	parent_control->setAnalog(SimpleArcadeDriveControl::CMD_TURN, turn_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSDriveSimpleArcadeDrivePower::update(void)
{
	return next_step;
}
