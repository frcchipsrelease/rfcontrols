/*******************************************************************************
 * 
 * File: LimelightControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include <math.h>

#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "gsinterfaces/Time.h"
#include "rfutilities/DataLogger.h"
#include "rfutilities/MacroStepFactory.h"

#include "rfcontrols//LimelightControl.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control, feedback, and PID 
 * and connect them to the specified inputs
 * clo
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 						
 * @param	period		the time (in seconds) between calls to update()
 * @param	priority	the priority at which this thread/task should run
 * 
 ******************************************************************************/
LimelightControl::LimelightControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{

	Advisory::pinfo("========================= Creating Limelight Control [%s] =========================",
			            control_name.c_str());
	is_closed_loop = false;
	is_target_drive = false; //:))

	kpDistance = -0.1f; //:))

	motor = nullptr;

	is_ready = false;

    trajectory_max_velocity = 200.0;
    trajectory_acceleration = 0.0;
    trajectory_deceleration = 0.0;
    trajectory_target_position = 0.0;
	trajectory_target_velocity = 0.0;

	target_power = 0.0;
	command_power = 0.0;

	target_position = 0.0;
	raw_position = -999.99;
	actual_position = -999.99;

	raw_velocity = -999;
    actual_velocity = -999.99;

	nudge_up_power = 0.2;
	nudge_down_power = -0.2;
	nudge_up_step = 0.01;
	nudge_down_step = -0.01;

	analog_idx_step_size = 0.1;
	analog_idx_step_zero = -1.0;

	min_position = 0.0;
	max_position = 359.0;

	analog_delta_scale = 0.01;
    analog_delta_step = 0.0;

	for (uint8_t i = 0; i < NUM_SETPOINTS; i++)
	{
		setpoint_name[i] = "unknown";
		setpoint_position[i] = 0.0;
		setpoint_defined[i] = false;
	}

	pjs_log = new DataLogger(std::string(("/robot/logs/") + getName()), "log", "csv", 10, true);

    //
	// Register Macro Steps
	//
	//new MacroStepProxy<MSSomething>(control_name, "SetPower", this);
	//new MacroStepProxy<MSSomethingPosition>(control_name, "SetPosition", this);

	//
	// Parse the Controls XML
	//
	XMLElement *comp;
	const char *name;

	m_limelight = HardwareFactory::createLimelight(nullptr);

	xml->QueryBoolAttribute("closed_loop", &is_closed_loop);
	xml->QueryBoolAttribute("target_drive", &is_target_drive); //:)) I dont know what this actually does but it seems important

	xml->QueryFloatAttribute("trajectory_max_velocity", &trajectory_max_velocity);
	xml->QueryFloatAttribute("trajectory_acceleration", &trajectory_acceleration);
	xml->QueryFloatAttribute("trajectory_deceleration", &trajectory_deceleration);

	xml->QueryFloatAttribute("min_position", &min_position);
	xml->QueryFloatAttribute("max_position", &max_position);

	comp = xml->FirstChildElement("setpoints");
	if (comp != nullptr)
	{
    	XMLElement *setpoint_comp;
		setpoint_comp = comp->FirstChildElement("setpoint");
 		while (setpoint_comp!=nullptr)
		{
			int setpoint_index = -1;
			setpoint_comp->QueryIntAttribute("index", &setpoint_index);
			if (setpoint_index >= 0 && setpoint_index < LimelightControl::NUM_SETPOINTS)
			{
				name = setpoint_comp->Attribute("name");
				if (name != nullptr)
				{
					setpoint_name[setpoint_index] = std::string(name);
				}
				else
				{
					setpoint_name[setpoint_index] = std::string("setpoint_") + std::to_string(setpoint_index);
				}

				setpoint_comp->QueryFloatAttribute("position", &setpoint_position[setpoint_index]);

				setpoint_defined[setpoint_index] = true;

				Advisory::pinfo(" -- setpoint %2d: %20s   position=%7.2f",
					setpoint_index, setpoint_name[setpoint_index].c_str(),
					setpoint_position[setpoint_index]);
			}
			else
			{
				Advisory::pinfo("setpoint with index out of range -- %d", setpoint_index);
			}

    		setpoint_comp = setpoint_comp->NextSiblingElement("setpoint");
		}
	}

	comp = xml-> FirstChildElement("motor");
	if (comp != NULL)
	{
		Advisory::pinfo("creating motor controller");
		motor = HardwareFactory::createMotor(comp);
		motor->setPosition(0.0); // set initial position to known value
		// @TODO: read initial position from config
	}
	else
	{
		Advisory::pwarning("WARNING: required element \"motor\" not found");
	}

	comp = xml-> FirstChildElement("oi");
	while (comp != NULL)
	{
		name = comp->Attribute("name");
		if (name != NULL)
		{
			if (strcmp(name, "analog") == 0)
			{
				comp->QueryFloatAttribute("delta_scale", &analog_delta_scale);
				Advisory::pinfo("connecting analog channel, delta_scale=%f", analog_delta_scale);
				OIController::subscribeAnalog(comp, this, CMD_MOVE_ANALOG);
			}
			else if (strcmp(name, "setpoint_idx") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeInt(comp, this, CMD_SETPOINT_IDX);
			}
			else if (strcmp(name, "setpoint_analog_idx") == 0)
			{
				comp->QueryFloatAttribute("step", &analog_idx_step_size);
				comp->QueryFloatAttribute("zero", &analog_idx_step_zero);
				Advisory::pinfo("connecting %s channel, step=%f, zero=%f",  name, analog_idx_step_size, analog_idx_step_zero);
				OIController::subscribeAnalog(comp, this, CMD_SETPOINT_ANALOG_IDX);
			}
			else if ((strncmp(name, "setpoint", 8) == 0)
				&& (name[8] >= '0') && (name[8] <= '9'))
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_SETPOINT_0 + (name[8] - '0'));
			}
			else if (strcmp(name, "nudge_up") == 0)
			{
				comp->QueryFloatAttribute("power", &nudge_up_power);
				comp->QueryFloatAttribute("step", &nudge_up_step);
				Advisory::pinfo("connecting %s channel, power=%f, step=%f", name, nudge_up_power, nudge_up_step);
				OIController::subscribeDigital(comp, this, CMD_NUDGE_UP);
			}
			else if (strcmp(name, "nudge_down") == 0)
			{
				comp->QueryFloatAttribute("power", &nudge_down_power);
				comp->QueryFloatAttribute("step", &nudge_down_step);
				Advisory::pinfo("connecting %s channel, power=%f, step=%f", name, nudge_down_power, nudge_down_step);
				OIController::subscribeDigital(comp, this, CMD_NUDGE_DOWN);
			}			
			else if (strcmp(name, "closed_loop_state") == 0)
			{
				Advisory::pinfo("connecting %s channel   OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO", name);
				OIController::subscribeDigital(comp, this, CMD_CLOSED_LOOP_STATE);
			}
			else if (strcmp(name, "target_drive_state") == 0)//:)) My understanding of this is that it allows a button to be pressed 
			{
				Advisory::pinfo("connecting %s channel", name);//to turn on target drive.
				OIController::subscribeDigital(comp, this, CMD_TARGET_DRIVE_STATE);
			}
		    else
			{
				Advisory::pinfo("unknown oi %s", name);
			}
		}
		else
		{
			Advisory::pinfo("unnamed oi");
		}

		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
LimelightControl::~LimelightControl(void)
{
	if (motor != nullptr)
	{
		delete motor;
		motor = nullptr;
	}
	
	if (pjs_log != nullptr)
	{
	    delete pjs_log;
	    pjs_log = nullptr;
	}
}

/*******************************************************************************	
 *
 *
 *
 ******************************************************************************/
void LimelightControl::controlInit(void)
{
    bool check_ready = true;
	
	if(motor == nullptr)
	{
		Advisory::pwarning("%s Position Joint Control missing required component -- motor", getName().c_str());
		check_ready = false;
	}

    trajectory_profile.setConfiguration(trajectory_max_velocity, trajectory_acceleration, trajectory_deceleration, this->getPeriod());

	is_ready = check_ready;
}

/*******************************************************************************
 *
 * This is the callback for OIController::subscribeAnalog, if the XML config 
 * specifies an analog input, the constructor of this object will connect that 
 * input to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the analog channel that was subscribed to
 * 
 ******************************************************************************/
void LimelightControl::setAnalog(int id, float val)
{
	switch (id)
	{
		case CMD_MOVE_ANALOG:
		{
			target_power = RobotUtil::limit(-1.0, 1.0, val);    // for open loop
			analog_delta_step = val * analog_delta_scale;  // for closed loop
		} break;

		case CMD_SETPOINT_ANALOG_IDX:
		{
			int idx = (int)((val - analog_idx_step_zero  + (0.3 * analog_idx_step_size)) / (analog_idx_step_size));
			// Advisory::pinfo(" TESTING LimelightControl::setAnalog val=%f, step=%f, zero=%f, idx=%d", 
			//     val, analog_idx_step_size, analog_idx_step_zero, idx);
			applySetpoint(true, idx);
		} break;
	}
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void LimelightControl::setDigital(int id, bool val)
{
	switch (id)
	{
		case CMD_CLOSED_LOOP_STATE:
		{
			Advisory::pinfo("############################### Closed loop");
			setClosedLoop(val);
		} break;

		case CMD_TARGET_DRIVE_STATE: //:))
		{
			setTargetDrive(val);
			Advisory::pinfo("###############################  Target Drive Mode");
		} break;

		case CMD_NUDGE_UP:
		{
			
			if (val)
			{
				target_power = nudge_up_power;
				target_position += nudge_up_step;
			}
			else
			{
				target_power = 0.0;
			}
		} break;
		
		case CMD_NUDGE_DOWN:
		{
			if (val)
			{
				target_power = nudge_down_power;
				target_position += nudge_down_step;
			}
			else
			{
				target_power = 0.0;
			}
		} break;

		case CMD_SETPOINT_0: 	applySetpoint(val, 0);  break;
		case CMD_SETPOINT_1: 	applySetpoint(val, 1);  break;
		case CMD_SETPOINT_2: 	applySetpoint(val, 2);  break;
		case CMD_SETPOINT_3: 	applySetpoint(val, 3);  break;
		case CMD_SETPOINT_4: 	applySetpoint(val, 4);  break;
		case CMD_SETPOINT_5: 	applySetpoint(val, 5);  break;
		case CMD_SETPOINT_6: 	applySetpoint(val, 6);  break;
        case CMD_SETPOINT_7:    applySetpoint(val, 7);  break;
	}
}

/*******************************************************************************	
 *
 * This is the callback for OIController::subscribeInt, if the XML config
 * specifies an int, the constructor of this object will connect that
 * input to this method.
 *
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the int channel that was subscribed to
 *
 ******************************************************************************/
void LimelightControl::setInt(int id, int val)
{
	Advisory::pinfo("%s %s(%d, %d)", getName(), __FUNCTION__, id, val);
	if (id == CMD_SETPOINT_IDX)
	{
		if (val >= 0)
		{
			applySetpoint( true, val/45);
		}
		else
		{
			applySetpoint( false, val);
		}
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void LimelightControl::applySetpoint(bool on, int idx)
{
	if (on && (idx >= 0) && (idx < NUM_SETPOINTS))
	{
		if (setpoint_defined[idx] == true)
		{
			target_position = setpoint_position[idx];
			Advisory::pinfo("%s applySetpoint: on=%d, idx=%d, targ=%f", getName().c_str(), on, idx, target_position);
		}
		else
		{
			Advisory::pinfo("%s applySetpoint: rejected, setpoint not defined for index %d", getName().c_str(), idx);
		}
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void LimelightControl::setPosition(float val)
{
	target_position = val;
}

/*******************************************************************************
 *
 ******************************************************************************/
float LimelightControl::getPostion(void)
{
	return actual_position;
}

/*******************************************************************************
 *
 ******************************************************************************/
void LimelightControl::disabledInit()
{
    pjs_log->close();
}

/*******************************************************************************
 *
 ******************************************************************************/
void LimelightControl::autonomousInit()
{
    initLogFile();
}

/*******************************************************************************
 *
 ******************************************************************************/
void LimelightControl::teleopInit()
{
    initLogFile();
}

/*******************************************************************************
 *
 ******************************************************************************/
void LimelightControl::testInit()
{
    initLogFile();
}

/**********************************************************************
 *
 * This method is used to initialize the log files, called from
 * teleop init and auton init methods
 *
 **********************************************************************/
void LimelightControl::initLogFile(void)
{
	Advisory::pinfo("Creating log file");
    pjs_log->openSegment();

    pjs_log->log("%s, %s, %s, %s, %s, %s, ",
                    "current_time",
                    "raw_position", "actual_position", "target_position",
                    "raw_velocity", "actual_velocity");

    pjs_log->log("%s, %s",
        "traj_position",
        "traj_velocity");

    pjs_log->log("\n");
    pjs_log->flush();

}

/*******************************************************************************
 *
 ******************************************************************************/
void LimelightControl::publish()
{
//	SmartDashboard::PutBoolean(std::string("  ") + getName().c_str() + "  ", (getCyclesSincePublish() > 0));
//	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());

	SmartDashboard::PutBoolean(getName() + " closed loop: ", is_closed_loop);
	SmartDashboard::PutBoolean(getName() + " target drive: ", is_target_drive);


	SmartDashboard::PutNumber(getName() + " raw position: ", raw_position);
	SmartDashboard::PutNumber(getName() + " act position: ", actual_position);

	SmartDashboard::PutNumber(getName() + " actual velocity: ", actual_velocity);
	SmartDashboard::PutNumber(getName() + " target position: ", target_position);

    SmartDashboard::PutNumber(getName() + " traj targ position: ", trajectory_target_position);
    SmartDashboard::PutNumber(getName() + " traj targ velocity: ", trajectory_target_velocity);

	SmartDashboard::PutNumber(getName() + " target power: ", target_power);
	SmartDashboard::PutNumber(getName() + " command power: ", command_power);

	SmartDashboard::PutNumber(getName() + " Horizontal: ", m_targetOffsetAngle_Horizontal);
	SmartDashboard::PutNumber(getName() + " Vertical: ", m_targetOffsetAngle_Vertical);
	SmartDashboard::PutNumber(getName() + " Area: ", m_targetArea);
	SmartDashboard::PutNumber(getName() + " Skew: ", m_targetSkew);
}

/*******************************************************************************
 *
 ******************************************************************************/
void LimelightControl::setClosedLoop(bool closed)
{
	if (is_closed_loop != closed)
	{
		is_closed_loop = closed;

		if (is_closed_loop)
		{
			motor->setControlMode(Motor::POSITION);
		    Advisory::pinfo("setting %s closed loop mode", getName().c_str());
		}
		else
		{
    		motor->setControlMode(Motor::PERCENT);
			Advisory::pinfo("setting %s open loop mode", getName().c_str());
		}
	}
}

/*******************************************************************************
 * 
 * //:))
 ******************************************************************************/
void LimelightControl::setTargetDrive(bool target)
{
	if (is_target_drive != target)
	{
		is_target_drive = target;
	}
}

/*******************************************************************************
 * Returns true if the bool variable is_closed_loop is true.
 ******************************************************************************/
bool LimelightControl::isClosedLoop(void)
{
	return is_closed_loop;
}

/*******************************************************************************
 * Returns true if the bool variable is_target_drive is true.
 * //:))
 ******************************************************************************/
bool LimelightControl::isTargetDrive(void)
{
	return is_target_drive;
}
/*******************************************************************************
 *
 ******************************************************************************/
bool LimelightControl::isAtTarget(float tolerance)
{
	return (fabs(actual_position - target_position) < tolerance);
}
/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void LimelightControl::doPeriodic()
{
	if(is_ready == false)
	{
		Advisory::pinfo("DoPeriodic is not ready");
		return;
	}

	double current_time = gsi::Time::getTime();

	m_limelight->getData(m_target_visible,
		m_targetOffsetAngle_Horizontal,
		 m_targetOffsetAngle_Vertical,
		 m_targetArea,
		 m_targetSkew);


	motor->doUpdate();

	// for debugging
    command_power = motor->getPercent();
	raw_position = motor->getRawPosition();
	raw_velocity = motor->getRawVelocity();
	actual_position = motor->getPosition();
	actual_velocity = motor->getVelocity();
	
	//
	// Make sure the motor doesn't jump when enabled
	//
	if (getPhase() == DISABLED)
	{
		target_position = actual_position;
		target_power = 0.0;

		trajectory_profile.setInitialPosition(target_position);
	}

	//
	// Command the position
	//
	if (is_closed_loop)
	{

 	//  target_position += target_position + analog_delta_step;
	//	target_position = RobotUtil::limit(min_position, max_position, target_position); 
		  
	//	trajectory_profile.setTargetPosition(target_position);
    //	trajectory_profile.update();
	//	trajectory_target_position = trajectory_profile.getTrajectoryPosition();
	//	trajectory_target_velocity = trajectory_profile.getTrajectoryVelocity();
	//	trajectory_profile.setTargetPosition(target_position);
	//  Advisory::pinfo("LL setting postiong %f", target_position);

		motor->setPosition((double)target_position);

	}
	else
	{
		//Advisory::pinfo("oooooooopen loop %f", target_power);
		// Check if we are in target drive mode
		// If we are in target mode get the estimated distance 
		// using the limelight data.
		// Thrn figure out how to adjust the motor commands as needed.
		if(is_target_drive) //:))
		{
			// getData for the limelight is called earlier in the lime light so it doesnt have to be called here.
			// OR maybe it should be called here to that there is not another function that gets called when its not needed.
			// makes it run faster!! (maybe. idk im not a profesional)

			m_target_drive_power = m_targetOffsetAngle_Vertical * kpDistance; //This is what the limelight website did. idk

			target_power = m_target_drive_power; // Im not sure what the difference of command power vs target power but target power seems to be correct here!
		
			Advisory::pinfo("I didn't bring a limelight home  %f", target_power);
			
		}

	    motor->setPercent(target_power);
	}

	//
	// Log data
	//
	pjs_log->log("%6.8f, %6.8f, %6.8f, %6.8f, %6.8f, %6.8f, ",
	                current_time,
	                raw_position, actual_position, target_position,
	                raw_velocity, actual_velocity);

    pjs_log->log("%6.8f, %6.8f\n",
        trajectory_target_position,
        trajectory_target_velocity);
}

// =============================================================================
// =============================================================================
// =============================================================================
