/**********************************************************
 *
 * File: HopperControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "rfcontrols//HopperControl.h"
#include "rfutilities/MacroStepFactory.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
HopperControl::HopperControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Hopper Control [%s] =========================",
	            control_name.c_str());
	m_vertical_motor = nullptr;
	m_horizontal_motor = nullptr;

	motor_min_control = -1.0;
	motor_max_control = 1.0;

    m_vertical_max_current = 100.0;
    m_horizontal_max_current = 100.0;

	m_momentary_a_value = 0.0;
	m_momentary_b_value = 0.0;

	m_max_cmd_delta = 0.25;

	motor_forward_power = 0.0;
	motor_backward_power = 0.0;

	m_vertical_target_power = 0.0;
	m_vertical_command_power = 0.0;

	m_horizontal_target_power = 0.0;
	m_horizontal_command_power = 0.0;

	const char *name = nullptr;

	//
	// Register Macro Steps
	//
	new MacroStepProxy<MSHopper>(control_name, "SetPower", this);

	hopper_log = new DataLogger("/robot/logs/hopper", "hopper", "csv", 10, true);

	//
	// Parse XML
	//
    xml->QueryFloatAttribute("min_control", &motor_min_control);
    xml->QueryFloatAttribute("max_control", &motor_max_control);
    xml->QueryFloatAttribute("max_cmd_delta", &m_max_cmd_delta);
	Advisory::pinfo("  max cmd delta = %f", m_max_cmd_delta);

	comp = xml-> FirstChildElement("motor");
		
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if(name != nullptr)
		{
			if(strcmp(name, "vertical_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for Vertical Motor");
				m_vertical_motor = HardwareFactory::createMotor(comp);
				comp -> QueryUnsignedAttribute("max_current", &m_vertical_max_current);
				Advisory::pinfo("max_current=%u", m_vertical_max_current); 
			}
			else if(strcmp(name, "horizontal_motor") == 0)
			{
				Advisory::pinfo("  creating speed controller for Horizontal Motor");
				m_horizontal_motor = HardwareFactory::createMotor(comp);
				comp -> QueryUnsignedAttribute("max_current", &m_horizontal_max_current);
				Advisory::pinfo("max_current=%u", m_horizontal_max_current); 
			}
			else
			{
				Advisory::pwarning("found unexpected motor with name %s");
			}
		}
		else
		{
			Advisory::pwarning("  found unexpected motor with no name attribute");
		}
		comp = comp->NextSiblingElement("motor");
	}
	
	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "forward") == 0)
			{
				Advisory::pinfo("  connecting forward channel");
				comp->QueryFloatAttribute("value", &motor_forward_power);
				OIController::subscribeDigital(comp, this, CMD_FORWARD);
			}
			else if (strcmp(name, "backward") == 0)
			{
				Advisory::pinfo("  connecting backward channel");
				comp->QueryFloatAttribute("value", &motor_backward_power);
				OIController::subscribeDigital(comp, this, CMD_BACKWARD);
			}
			else if (strcmp(name, "stop") == 0)
			{
				Advisory::pinfo("  connecting stop channel");
				OIController::subscribeDigital(comp, this, CMD_STOP);
			}
		}
		
		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
HopperControl::~HopperControl(void)
{
	if (m_vertical_motor != nullptr)
	{
		delete m_vertical_motor;
		m_vertical_motor = nullptr;
	}

	if (m_horizontal_motor != nullptr)
	{
		delete m_horizontal_motor;
		m_horizontal_motor = nullptr;
	}
	
	if (hopper_log != nullptr)
	{
	    delete hopper_log;
	    hopper_log = nullptr;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void HopperControl::controlInit(void)
{
	/*
    if ((m_vertical_motor != nullptr) && (m_vertical_max_current < 100.0))
    {
        m_vertical_motor->setCurrentLimit(m_vertical_max_current);
        m_vertical_motor->setCurrentLimitEnabled(true);
    }

    if ((m_horizontal_motor != nullptr) && (m_horizontal_max_current < 100.0))
    {
        m_horizontal_motor->setCurrentLimit(m_horizontal_max_current);
        m_horizontal_motor->setCurrentLimitEnabled(true);
    }
	*/
}

/*******************************************************************************
 *
 ******************************************************************************/
void HopperControl::updateConfig(void)
{
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void HopperControl::disabledInit(void)
{
	m_vertical_target_power = 0.0;
	m_vertical_command_power = 0.0;

	m_horizontal_target_power = 0.0;
	m_horizontal_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void HopperControl::autonomousInit(void)
{
	m_vertical_target_power = 0.0;
	m_vertical_command_power = 0.0;

	m_horizontal_target_power = 0.0;
	m_horizontal_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 * 
 ******************************************************************************/
void HopperControl::teleopInit(void)
{
	m_vertical_target_power = 0.0;
	m_vertical_command_power = 0.0;

	m_horizontal_target_power = 0.0;
	m_horizontal_command_power = 0.0;
}

/*******************************************************************************	
 *
 * Reset power to 0
 *
 ******************************************************************************/
void HopperControl::testInit(void)
{
}

/*******************************************************************************	
 *
 * This is the callback for OIController::setDigital, if the XML config 
 * specifies an increment, decrement, or stop input, the constructor 
 * of this object will connect that/those input(s) to this method.
 * 
 * @param id	the control id passed to the subscribe
 * @param val	the new value of the digital channel that was subscribed to
 * 
 ******************************************************************************/
void HopperControl::setDigital(int id, bool val)
{
	switch (id)
	{
		case CMD_FORWARD:
		{
			if (val)
			{
				m_vertical_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_forward_power);
				m_horizontal_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_forward_power);
				//Advisory::pinfo("  -------------------------VerticL power %d", m_vertical_target_power);
			}
		} break;
	
		case CMD_BACKWARD:
		{
			if (val)
			{
				m_vertical_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_backward_power);
				m_horizontal_target_power = RobotUtil::limit(motor_min_control, motor_max_control, motor_backward_power);
			}
		} break;
	
		case CMD_STOP:
		{
			if (val)
			{
				m_vertical_target_power = 0.0;
				m_horizontal_target_power = 0.0;
			}
		} break;
			
		default:
			break;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
void HopperControl::setPower(float vertical_percent, float horizontal_percent)
{
	m_vertical_target_power = vertical_percent;
	m_horizontal_target_power = horizontal_percent;
}

/*******************************************************************************
 *
 ******************************************************************************/
void HopperControl::setInt(int id, int val)
{
}

/*******************************************************************************	
 *
 ******************************************************************************/
void HopperControl::publish()
{
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() + " vertical target_power: ", m_vertical_target_power);
	SmartDashboard::PutNumber(getName() + " horizontal target_power: ", m_horizontal_target_power);
	SmartDashboard::PutNumber(getName() + " vertical command_power: ", m_vertical_command_power);
	SmartDashboard::PutNumber(getName() + " horizontal command_power: ", m_horizontal_command_power);
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 ******************************************************************************/
void HopperControl::doPeriodic() // sdr this aint right rearrange
{	
	m_vertical_motor->doUpdate();
	m_horizontal_motor->doUpdate();

	//
	// Get inputs -- this is just for reporting
	//

	m_vertical_command_power = m_vertical_motor->getPercent();
	m_horizontal_command_power = m_horizontal_motor->getPercent();

	//
	// All processing happens in the motor class
	//

	//
	// Set Outputs
	//

	m_vertical_motor->setPercent(m_vertical_target_power);
	m_horizontal_motor->setPercent(m_horizontal_target_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MSHopper::MSHopper(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	m_power = 0.0;
	m_parent_control = (HopperControl *)control;
	xml->QueryFloatAttribute("power", &m_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSHopper::init(void)
{
	m_parent_control->setPower(m_power, m_power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSHopper::update(void)
{
	return next_step;
}