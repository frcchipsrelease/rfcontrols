/*******************************************************************************
 *
 * File: DriveBase.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfcontrols/DriveBase.h"

#include "rfhardware/HardwareFactory.h"

#include "rfutilities/MacroStepFactory.h"
#include "rfutilities/RobotUtil.h"


#include "gsutilities/Advisory.h"

#include "gsinterfaces/Time.h"

#include "frc/smartDashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of this object and configure it based on the provided
 * XML, period, and priority
 * 
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 						
 * @param	period		the time (in seconds) between calls to update()
 * @param	priority	the priority at which this thread/task should run
 * 
 * *****************************************************************************/
DriveBase::DriveBase(std::string control_name, XMLElement* xml)
	:	PeriodicControl(control_name)
{
    Advisory::pinfo("========================= Creating Arcade Drive Control [%s] =========================",
                control_name.c_str());
	XMLElement *comp;

	const char *name;

    m_data_logger = new DataLogger("/robot/logs", "drive", "csv", 50, true);

	is_limelight_control = false;
	m_limelight = nullptr;

	fl_drive_motor = nullptr;
	fr_drive_motor = nullptr;
	bl_drive_motor = nullptr;
	br_drive_motor = nullptr;

    m_accelerometer = nullptr;
    m_accelerometer_x = 0.0;
    m_accelerometer_y = 0.0;
    m_accelerometer_z = 0.0;

#ifndef WIN32
#if 0  // remove IMU support for 2022
    m_imu = nullptr;
    m_imu_accel_x = 0.0;
    m_imu_accel_y = 0.0;
    m_imu_accel_z = 0.0;

    m_imu_mag_x = 0.0;
    m_imu_mag_y = 0.0;
    m_imu_mag_z = 0.0;

    m_imu_rate_x = 0.0;
    m_imu_rate_y = 0.0;
    m_imu_rate_z = 0.0;

    m_imu_angle_x = 0.0;
    m_imu_angle_y = 0.0;
    m_imu_angle_z = 0.0;

    m_imu_roll  = 0.0;
    m_imu_pitch = 0.0;
    m_imu_yaw   = 0.0;

    m_imu_quat_w = 0.0;
    m_imu_quat_x = 0.0;
    m_imu_quat_y = 0.0;
    m_imu_quat_z = 0.0;

    m_imu_bar_press   = 0.0;
    m_imu_temperature = 0.0;
#endif  // #if 0    
#endif  // WIN32

	fl_drive_motor_cmd = 0.0;
	fr_drive_motor_cmd = 0.0;
	bl_drive_motor_cmd = 0.0;
	br_drive_motor_cmd = 0.0;

	trn_power = 0.0;
	fwd_power = 0.0;
	arc_turn_power = -0.5;
	arc_fwd_power = 0.2;
	arc_turn_scale = 0.0;
	
	arc_input = 0;
	arc_power = false;

	m_targetHeight = 0.0;
	m_limelightHeight = 0.0;
	m_heightDifference = 0.0;
	m_calibratedDistance = 0.0;
	m_distanceOffset = 0.0;
	m_targetOffsetAngle_Horizontal = 0.0;
	m_targetOffsetAngle_Vertical = 0.0;
	m_targetArea = 0.0;
	m_targetSkew = 0.0;
	m_limelight_min_command = 0.025;
	m_limelight_scale_factor = -0.1;

	
	name = xml->Attribute("name");
	if (name == nullptr)
	{
		name="drive";
		Advisory::pcaution(  "WARNING: DriveBase created without name, assuming \"%s\"", name);
	}

	//
	// Register Macro Steps
	//
	new MacroStepProxy<MSDrive>(control_name, "DrivePower", this);
	
	new MacroStepProxy<MSPipeline>(control_name, "SetPipeline", this);

	//
	// Parse the XML
	//
	comp = xml-> FirstChildElement("motor");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "front_left") == 0)
			{				
				Advisory::pinfo("  creating speed controller for %s", name);
                fl_drive_motor = HardwareFactory::createMotor(comp);
			}
			else if (strcmp(name, "front_right") == 0)
			{
			    Advisory::pinfo("  creating speed controller for %s", name);
                fr_drive_motor = HardwareFactory::createMotor(comp);
			}
			else if (strcmp(name, "back_left") == 0)
			{
			    Advisory::pinfo("  creating speed controller for %s", name);
                bl_drive_motor = HardwareFactory::createMotor(comp);
			}
			else if (strcmp(name, "back_right") == 0)
			{
				Advisory::pinfo("  creating speed controller for %s", name);
                br_drive_motor = HardwareFactory::createMotor(comp);
			}
		}
		comp = comp->NextSiblingElement("motor");
	}

    comp = xml-> FirstChildElement("accelerometer");
    if (comp != nullptr)
    {
        // if type built_in then
        Advisory::pinfo("  creating accelerometer");
        m_accelerometer = new BuiltInAccelerometer();
    }

#ifndef WIN32
#if 0  // remove IMU support for 2022
    comp = xml-> FirstChildElement("imu");
    if (comp != nullptr)
    {
        // if type ADIS16448 then
        Advisory::pinfo("  creating imu");
        m_imu = new ADIS16448_IMU();
    }
#endif  // #if 0    
#endif  // WIN32

 	comp = xml-> FirstChildElement("limelight");
	if (comp != NULL)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			m_limelight = HardwareFactory::createLimelight(comp);
			Advisory::pinfo("  Created limelight %s", name);
		}
		comp->QueryDoubleAttribute("target_height", &m_targetHeight);
		Advisory::pinfo("  Target Height: %f", m_targetHeight);
		comp->QueryDoubleAttribute("limelight_height", &m_limelightHeight);
		Advisory::pinfo("  Limelight Height: %f", m_limelightHeight);
//		comp->QueryFloatAttribute("calibrated_distance", &m_calibratedDistance);
//		Advisory::pinfo("  Calibrated Distance: %f", m_calibratedDistance);
		comp->QueryDoubleAttribute("scale_factor", &m_limelight_scale_factor);
		Advisory::pinfo("  Limelight Scale Factor: %f", m_limelight_scale_factor);
		comp->QueryDoubleAttribute("min_command", &m_limelight_min_command);
		Advisory::pinfo("  Min Command: %f", m_limelight_min_command);
	}

    comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "forward") == 0)
			{
				Advisory::pinfo("  connecting to forward channel");
				OIController::subscribeAnalog(comp, this, CMD_FORWARD);
			}
			else if (strcmp(name, "turn") == 0)
			{
				Advisory::pinfo("  connecting to turn channel");
				OIController::subscribeAnalog(comp, this, CMD_TURN);
			}
			else if (strcmp(name, "arc") == 0)
			{
				Advisory::pinfo("  connecting to arc channel");
				OIController::subscribeInt(comp, this, CMD_ARC);
			}
			else if (strcmp(name, "power_arc") == 0)
			{
				Advisory::pinfo("  connecting to power_arc channel");
				OIController::subscribeDigital(comp, this, CMD_POWER_ARC);
				comp->QueryFloatAttribute("arc_turn_power", &arc_turn_power);
				comp->QueryFloatAttribute("arc_fwd_power", &arc_fwd_power);
				comp->QueryFloatAttribute("arc_turn_scale", &arc_turn_scale);
			}
			else if (strcmp(name, "limelight_control_state") == 0)
			{
				Advisory::pinfo("connecting %s channel", name);
				OIController::subscribeDigital(comp, this, CMD_LIMELIGHT_STATE);
			}
		}
		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 *
 * Release any resources used by this object
 * 
 ******************************************************************************/
DriveBase::~DriveBase(void)
{
	if (fl_drive_motor != nullptr)
	{
		delete fl_drive_motor;
		fl_drive_motor = nullptr;
	}
	
	if (fr_drive_motor != nullptr)
	{
		delete fr_drive_motor;
		fr_drive_motor = nullptr;
	}
	
	if (bl_drive_motor != nullptr)
	{
		delete bl_drive_motor;
		bl_drive_motor = nullptr;
	}
	
	if (br_drive_motor != nullptr)
	{
		delete br_drive_motor;
		br_drive_motor = nullptr;
	}
}

/*******************************************************************************	
 *
 ******************************************************************************/
void DriveBase::updateConfig()
{
}

/*******************************************************************************	
 *
 ******************************************************************************/
void DriveBase::controlInit()
{
	fwd_power = 0.0;
	trn_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void DriveBase::disabledInit()
{
    m_data_logger->close();

	fwd_power = 0.0;
	trn_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void DriveBase::autonomousInit()
{
    logFileInit("Auton");

    fwd_power = 0.0;
	trn_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void DriveBase::teleopInit()
{
    logFileInit("Teleop");

    fwd_power = 0.0;
	trn_power = 0.0;
}

/*******************************************************************************	
 *
 ******************************************************************************/
void DriveBase::testInit()
{
    logFileInit("Test");

    fwd_power = 0.0;
	trn_power = 0.0;
}

/*******************************************************************************
 *
 ******************************************************************************/

void DriveBase::setInt(int id, int val)
{
     try
     {
         switch (id)
         {
             case CMD_ARC:
             {
                 if (val == 90)
                 {
					trn_power = 0.1;
					fwd_power = 0.2;
                 }
                 else if (val == 270)
                 {
					trn_power = 0.1;
					fwd_power = -0.2;
                 }
                 else
                 {
                	;//Do nothing
                 }

            } break;
        }
    }
    catch (...)
    {
        Advisory::pwarning("EXCEPTION: caught in LightControl::setInt"); // sdr ??
    }
}


/*******************************************************************************
 *
 * Sets the state of the control based on the command id and value
 * 
 * Handled command ids are CMD_TURN and CMD_FORWARD, all others are ignored
 * 
 * @param	id	the command id that indicates what the val argument means
 * @param	val	the new value
 * 
 ******************************************************************************/
void DriveBase::setAnalog(int id, float val)
{
	switch (id)
	{
		case CMD_TURN:
			if (arc_power == false)
			{
				trn_power = val;
			}
			else if (arc_power == true)
			{
				//if (fwd_power == 0.0)
					//trn_power == 0.0;
				//else
				trn_power = arc_turn_power;
				Advisory::pinfo("DriveBase::setAnalog TURN(id: %d, val: %f, tp: %f, atp: %f)",
						id, val, trn_power, arc_turn_power);
			}
			break;

		case CMD_FORWARD:
			if (arc_power == false)
			{
				fwd_power = val;
			}
			else if (arc_power == true)
			{
				// going forward
				if (val>0)
				{
					trn_power = arc_turn_power * arc_turn_scale;
					fwd_power = arc_fwd_power;
					Advisory::pinfo("DriveBase::setAnalog  FWD(ID %d, turn %f, fp %f, atp %f)",
							id, trn_power, arc_fwd_power, arc_turn_power);
				}
				// going backward
				else if (val<0)
				{
					trn_power = -(arc_turn_power);
					fwd_power = -(arc_fwd_power);
					Advisory::pinfo("DriveBase::setAnalog  BACK(ID %d, turn %f, fp %f, atp %f)",
							id, trn_power, arc_fwd_power, arc_turn_power);
				}
				// stopped
				else if (val == 0)
				{
					fwd_power = 0;//brake
					trn_power = 0;
					Advisory::pinfo("STOP");
				}
			}
			break;

		default:
			break;
	}
}

/*******************************************************************************	
 *
 * Sets the state of the control based on the command id and value
 *  
 * Handled command ids are CMD_BRAKE_[ON|OFF|TOGGLE|STATE] and 
 * CMD_GEAR_[HIGH|LOW|TOGGLE|STATE], all others are ignored
 *
 * @param	id	the command id that indicates what the val argument means
 * @param	val	the new value
 * 
 ******************************************************************************/
void DriveBase::setDigital(int id, bool val)
{
	switch(id)
	{
		case CMD_POWER_ARC:
			arc_power = val;
			if (arc_power == 0)
			{
				fwd_power = 0;
				trn_power = 0;
				Advisory::pinfo("arc_power off, so FORCING FWD/TRN TO STOP!");
			}
			break;
		case CMD_LIMELIGHT_STATE:
		{
			setLimelightState(val);
		} break;

	}
}
/*******************************************************************************
 *
 ******************************************************************************/
void DriveBase::setLimelightState(bool value)
{
	if (is_limelight_control != value)
	{
		is_limelight_control = value;

		if (is_limelight_control)
		{
		    Advisory::pinfo("setting %s limelight control mode", getName().c_str());
//		    motor->setControlMode(Motor::VELOCITY); 
		}
		else
		{
            Advisory::pinfo("setting %s open loop mode", getName().c_str());
 //       	motor->setControlMode(Motor::PERCENT); 
		}
	}
}
/*******************************************************************************
 *
 ******************************************************************************/

void DriveBase::setPipeline(int id) {
    if(m_limelight != nullptr)
    {
    	m_limelight->setPipeline(id);
    }
}

/*******************************************************************************
 *
 ******************************************************************************/

void DriveBase::publish()
{
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());

	SmartDashboard::PutNumber(getName() +" forward: ", fwd_power);
	SmartDashboard::PutNumber(getName() +" turn: ", trn_power);

	SmartDashboard::PutNumber(getName() +" fl cmd: ", fl_drive_motor_cmd);
	SmartDashboard::PutNumber(getName() +" fr cmd: ", fr_drive_motor_cmd);
	SmartDashboard::PutNumber(getName() +" bl cmd: ", bl_drive_motor_cmd);
	SmartDashboard::PutNumber(getName() +" br cmd: ", br_drive_motor_cmd);

    if (m_accelerometer != nullptr)
    {
        SmartDashboard::PutNumber(getName() + " accel_x: ", m_accelerometer_x);
        SmartDashboard::PutNumber(getName() + " accel_y: ", m_accelerometer_y);
        SmartDashboard::PutNumber(getName() + " accel_z: ", m_accelerometer_z);
    }

#ifndef WIN32
#if 0  // remove IMU support for 2022
    if(m_imu != nullptr)
    {
        SmartDashboard::PutNumber(getName() + " IMU Accel X: ", m_imu->GetAccelX()); // m_accel_x -- from sensor
        SmartDashboard::PutNumber(getName() + " IMU Accel Y: ", m_imu->GetAccelY());
        SmartDashboard::PutNumber(getName() + " IMU Accel Z: ", m_imu->GetAccelZ());

        SmartDashboard::PutNumber(getName() + " IMU Mag X: ", m_imu->GetMagX());  // m_mag_x  -- from sensor
        SmartDashboard::PutNumber(getName() + " IMU Mag Y: ", m_imu->GetMagY());
        SmartDashboard::PutNumber(getName() + " IMU Mag Z: ", m_imu->GetMagZ());

        SmartDashboard::PutNumber(getName() + " IMU Rate X: ", m_imu->GetRateX()); // m_gyro_x  -- from sensor
        SmartDashboard::PutNumber(getName() + " IMU Rate Y: ", m_imu->GetRateY());
        SmartDashboard::PutNumber(getName() + " IMU Rate Z: ", m_imu->GetRateZ());

        SmartDashboard::PutNumber(getName() + " IMU Angle X: ", m_imu->GetAngleX());  // m_integ_gyro_x, m_integ_gyro_x += (gyro_x - m_gyro_offset_x) * dt;
        SmartDashboard::PutNumber(getName() + " IMU Angle Y: ", m_imu->GetAngleY());
        SmartDashboard::PutNumber(getName() + " IMU Angle Z: ", m_imu->GetAngleZ());

        SmartDashboard::PutNumber(getName() + " IMU Roll: ",  m_imu->GetRoll()); // calculated
        SmartDashboard::PutNumber(getName() + " IMU Pitch: ", m_imu->GetPitch());
        SmartDashboard::PutNumber(getName() + " IMU Yaw: ",   m_imu->GetYaw());

        SmartDashboard::PutNumber(getName() + " IMU Quat W: ", m_imu->GetQuaternionW());  // calculated
        SmartDashboard::PutNumber(getName() + " IMU Quat X: ", m_imu->GetQuaternionX());
        SmartDashboard::PutNumber(getName() + " IMU Quat Y: ", m_imu->GetQuaternionY());
        SmartDashboard::PutNumber(getName() + " IMU Quat Z: ", m_imu->GetQuaternionZ());

        SmartDashboard::PutNumber(getName() + " IMU Bar Press: ",   m_imu->GetBarometricPressure());
        SmartDashboard::PutNumber(getName() + " IMU Temperature: ", m_imu->GetTemperature());
    }
#endif  // #if 0    
#endif  // WIN32

	SmartDashboard::PutBoolean(getName() + " Limelight Control: ", is_limelight_control);
	SmartDashboard::PutNumber(getName() + " Limelight Horizontal: ", m_targetOffsetAngle_Horizontal);
	SmartDashboard::PutNumber(getName() + " Limelight Vertical: ", m_targetOffsetAngle_Vertical);
	SmartDashboard::PutNumber(getName() + " Limelight Area: ", m_targetArea);
	SmartDashboard::PutNumber(getName() + " Limelight Skew: ", m_targetSkew);
	SmartDashboard::PutNumber(getName() + " Limelight Distance Offset: ", m_distanceOffset);

}

/*******************************************************************************
 *
 ******************************************************************************/
void DriveBase::logFileInit(std::string phase)
{
    Advisory::pinfo("initializing log file for %s", phase.c_str());

    m_data_logger->openSegment();

    m_data_logger->log("%s, %s, %s, ",
        "time", "phase", "pet"
    );

    m_data_logger->log("%s, %s,   %s, %s, %s, %s, ",
        "trn_power", "fwd_power",
        "fl_drive_motor_cmd", "fr_drive_motor_cmd", "bl_drive_motor_cmd", "br_drive_motor_cmd");

    if (m_accelerometer != nullptr)
    {
        m_data_logger->log("%s, %s, %s, ",
            "m_accelerometer_x", "m_accelerometer_y", "m_accelerometer_z");
    }

#ifndef WIN32
#if 0  // remove IMU support for 2022
   if (m_imu!= nullptr)
    {
        m_data_logger->log("%s, %s, %s,   %s, %s, %s,   %s, %s, %s,   %s, %s, %s,   %s, %s, %s,   %s, %s, %s, %s,     %s, %s ",
            "m_imu_accel_x", "m_imu_accel_y", "m_imu_accel_z",
            "m_imu_mag_x", "m_imu_mag_y", "m_imu_mag_z",
            "m_imu_rate_x", "m_imu_rate_y", "m_imu_rate_z",
            "m_imu_angle_x", "m_imu_angle_y", "m_imu_angle_z",
            "m_imu_roll", "m_imu_pitch", "m_imu_yaw",
            "m_imu_quat_w", "m_imu_quat_x", "m_imu_quat_y", "m_imu_quat_z",
            "m_imu_bar_press", "m_imu_temperature");
    }
#endif  // #if 0    
#endif  // WIN32

    m_data_logger->log("\n");

    m_data_logger->flush();
}

/*******************************************************************************
 *
 ******************************************************************************/
void DriveBase::logFileAppend(void)
{
    m_data_logger->log("%f, %d, %f, ", 
        gsi::Time::getTime(), this->getPhase(), this->getPhaseElapsedTime());

    m_data_logger->log("%f, %f,   %f, %f, %f, %f, ",
        trn_power, fwd_power,
        fl_drive_motor_cmd, fr_drive_motor_cmd, bl_drive_motor_cmd,br_drive_motor_cmd);

    if (m_accelerometer != nullptr)
    {
        m_data_logger->log("%f, %f, %f, ",
            m_accelerometer_x, m_accelerometer_y, m_accelerometer_z);
    }

    if (m_imu!= nullptr)
    {
        m_data_logger->log("%f, %f, %f,   %f, %f, %f,   %f, %f, %f,   %f, %f, %f,   %f, %f, %f,   %f, %f, %f, %f,     %f, %f ",
            m_imu_accel_x, m_imu_accel_y, m_imu_accel_z,
            m_imu_mag_x, m_imu_mag_y, m_imu_mag_z,
            m_imu_rate_x, m_imu_rate_y, m_imu_rate_z,
            m_imu_angle_x, m_imu_angle_y, m_imu_angle_z,
            m_imu_roll, m_imu_pitch, m_imu_yaw,
            m_imu_quat_w, m_imu_quat_x, m_imu_quat_y, m_imu_quat_z,
            m_imu_bar_press, m_imu_temperature);
    }

    m_data_logger->log("\n");
}

/*******************************************************************************	
 * 
 * Sets the actuator values every period
 * 
 ******************************************************************************/
void DriveBase::doPeriodic()
{
	fl_drive_motor->doUpdate();
	fr_drive_motor->doUpdate();
	bl_drive_motor->doUpdate();
	br_drive_motor->doUpdate();

	//
	// Read Sensors
	//

    if (m_accelerometer != nullptr)
    {
        m_accelerometer_x = m_accelerometer->GetX();
        m_accelerometer_y = m_accelerometer->GetY();
        m_accelerometer_z = m_accelerometer->GetZ();
    }

#ifndef WIN32
#if 0  // remove IMU support for 2022
   if (m_imu != nullptr)
    {
        m_imu_accel_x      = m_imu->GetAccelX();
        m_imu_accel_y      = m_imu->GetAccelY();
        m_imu_accel_z      = m_imu->GetAccelZ();

        m_imu_mag_x        = m_imu->GetMagX();
        m_imu_mag_y        = m_imu->GetMagY();
        m_imu_mag_z        = m_imu->GetMagZ();

        m_imu_rate_x       = m_imu->GetRateX();
        m_imu_rate_y       = m_imu->GetRateY();
        m_imu_rate_z       = m_imu->GetRateZ();

        m_imu_angle_x      = m_imu->GetAngleX();
        m_imu_angle_y      = m_imu->GetAngleY();
        m_imu_angle_z      = m_imu->GetAngleZ();

        m_imu_roll         = m_imu->GetRoll();
        m_imu_pitch        = m_imu->GetPitch();
        m_imu_yaw          = m_imu->GetYaw();

        m_imu_quat_w       = m_imu->GetQuaternionW();
        m_imu_quat_x       = m_imu->GetQuaternionX();
        m_imu_quat_y       = m_imu->GetQuaternionY();
        m_imu_quat_z       = m_imu->GetQuaternionZ();

        m_imu_bar_press    = m_imu->GetBarometricPressure();
        m_imu_temperature  = m_imu->GetTemperature();
    }
#endif  // #if 0    
#endif  // WIN32

    if(m_limelight != nullptr)
    {
        m_limelight->getData(m_target_visible,
                            m_targetOffsetAngle_Horizontal,
                            m_targetOffsetAngle_Vertical,
                            m_targetArea,
                            m_targetSkew);
    }
  	if (is_limelight_control)
	{
		if(m_limelight != nullptr)
		{
        	float steering_adjust = 0.0f;
        	if (m_targetOffsetAngle_Horizontal > 1.0)
        	{
                steering_adjust = m_limelight_scale_factor * -m_targetOffsetAngle_Horizontal/RAD_TO_DEG - m_limelight_min_command;
        	}
        	else if (m_targetOffsetAngle_Horizontal < 1.0)
        	{
                steering_adjust = m_limelight_scale_factor *-m_targetOffsetAngle_Horizontal/RAD_TO_DEG + m_limelight_min_command;
        	}
        	fl_drive_motor_cmd += steering_adjust;
			bl_drive_motor_cmd += steering_adjust;
        	fr_drive_motor_cmd -= steering_adjust;
        	br_drive_motor_cmd -= steering_adjust;
		}
	}
	else
	{
		//
		// Calculate Values
		//
		fl_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power - trn_power);
		fr_drive_motor_cmd = RobotUtil::limit(-1.0, 1.0, fwd_power + trn_power);
		bl_drive_motor_cmd = fl_drive_motor_cmd;
		br_drive_motor_cmd = fr_drive_motor_cmd;	
	}

	//
	// Set Outputs
	//

	fl_drive_motor->setPercent(fl_drive_motor_cmd);
	fr_drive_motor->setPercent(fr_drive_motor_cmd);
	bl_drive_motor->setPercent(bl_drive_motor_cmd);
	br_drive_motor->setPercent(br_drive_motor_cmd);

	logFileAppend();
}

/*******************************************************************************
 *
 ******************************************************************************/
MSDrive::MSDrive(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (DriveBase *)control;

	m_forward = xml->FloatAttribute("forward");
	m_turn = xml->FloatAttribute("turn");
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSDrive::init(void)
{
	parent_control->setAnalog(DriveBase::CMD_FORWARD, m_forward);
	parent_control->setAnalog(DriveBase::CMD_TURN, m_turn);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSDrive::update(void)
{
	return next_step;
}


/*******************************************************************************
 *
 ******************************************************************************/
MSPipeline::MSPipeline(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (DriveBase *)control;

	parent_control->setPipeline(0);
	m_pipeline = 0;
	m_pipeline = xml->IntAttribute("id");

}

/*******************************************************************************
 *
 ******************************************************************************/
void MSPipeline::init(void)
{
	parent_control->setPipeline(m_pipeline);

}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSPipeline::update(void)
{
	return next_step;
}