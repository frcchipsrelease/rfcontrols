/*******************************************************************************
 *
 * @file: MotorControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District
 *  NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfutilities/RobotUtil.h"
#include "rfhardware/HardwareFactory.h"
#include "gsutilities/Advisory.h"
#include "rfcontrols//MotorControl.h"
#include "rfutilities/MacroStepFactory.h"

#include "gsutilities/Filter.h"
#include "gsinterfaces/Time.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * @brief Create an instance of a motor control and connect it to the specified
 * motor and inputs
 * 
 ******************************************************************************/
MotorControl::MotorControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name) 
    , data_logger("/robot/logs/motor_control/", control_name.c_str(), "csv", 10, 5, 512)
{
	XMLElement *comp;
	Advisory::pinfo("========================= Creating Motor Control [%s] =========================",
	            control_name.c_str());

	// 
	// Initialize all class variables to a known, safe, value
	// 			
	motor_a = nullptr;
	motor_b = nullptr;

	hardware_is_ready = false;

	upper_limit_pressed = false;
	lower_limit_pressed = false;

	motor_min_percent = -1.0;
	motor_max_percent = 1.0;

    motor_a_max_current = 100.0;
    motor_b_max_current = 100.0;

	motor_percent_target = 0.0;
	motor_percent_command = 0.0;
	motor_percent_actual = 0.0;

	const char *name = nullptr;

	//
	// Register Macro Steps
	//
	// Macro Steps are small bits of code that can be used to control
	// instances of this class from auton scripts or other Macro scripts 
	// that are used to build complex actions.
	//
	new MacroStepProxy<MotorControlMSSetPower>(control_name, "SetPower", this);

	//
	// Parse XML
	//
	// Query for any attributes specified in the control element 
	// here. If you query for an attribute that was not specified, the
	// variable that is passed in will not be updated.
	//
    xml->QueryFloatAttribute("min_control", &motor_min_percent);
    xml->QueryFloatAttribute("max_control", &motor_max_percent);

	// 
	// Next look for any hardware elements contained inside of the control 
	// element. In this case there could up to two motors. Loops can be
	// used to look for more sub-elements.
	//
	comp = xml-> FirstChildElement("motor");
	if (comp != nullptr)
	{
		Advisory::pinfo("  creating speed controller for motor_a");
		motor_a = HardwareFactory::createMotor(comp);
	    comp->QueryUnsignedAttribute("max_current", &motor_a_max_current);
	}
	
	comp = comp-> NextSiblingElement("motor");
	if (comp != nullptr)
	{
		Advisory::pinfo("  creating speed controller for motor_b");
		motor_b = HardwareFactory::createMotor(comp);
        comp->QueryUnsignedAttribute("max_current", &motor_b_max_current);
	}
	
	//
	// Query for Operator Interface connections
	//
	// These allow us to configure joysticks, game-pads, and other
	// inputs the way the drivers like, they can change there minds
	// later and we just remap to something different.
	//
	// These mappings are connected to methods in this class with 
	// a subscribe and a standard C++ bind. This bind tells the 
	// OI Controller to call a method (like MotorControl::setPower) in
	// "this" instance of the class with the arguments (placeholders)
	// for the values that the OI Controller expect to pass to the
	// method. We can also bind in values that are parsed from the XML
	// or constants we define someplace else. This gives us a lot of 
	// flexibility in how we implement our code.
	//
	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name != nullptr)
		{
			if (strcmp(name, "analogPower") == 0)
			{
				Advisory::pinfo("  connecting analogPower channel");
				// When this analog is changed call the analogPower() method
				// of "this" instance of the MotorControl class with the provided value
				Advisory::pinfo("  connecting setPower channel");
				float value;
				comp->QueryFloatAttribute("value", &value);
				OIController::subscribeAnalog(comp, 
					std::bind(&MotorControl::setPower, this, std::placeholders::_1));
			}
			else if (strcmp(name, "latchPower") == 0)
			{
				// When this button is pressed or released call the setPower() method
				// of "this" instance of the MotorControl class with the provided value
				// and if the button was pressed or released 
				Advisory::pinfo("  connecting setPower channel");
				float value;
				comp->QueryFloatAttribute("value", &value);
				OIController::subscribeDigital(comp, 
					std::bind(&MotorControl::setLatchedPower, this, value, std::placeholders::_1));
			}
			else if (strcmp(name, "momentaryPower") == 0)
			{
				// When this button is pressed or released call the momentaryPower() method
				// of "this" instance of the MotorControl class with the provided value
				// and if the button was pressed or released 
				Advisory::pinfo("  connecting momentaryPower channel");
				float value;
				comp->QueryFloatAttribute("value", &value);
				OIController::subscribeDigital(comp, 
					std::bind(&MotorControl::setMomentaryPower, this, value, std::placeholders::_1));
			}
			else if (strcmp(name, "adjustPower") == 0)
			{
				// When this button is pressed or released call the adjustPower() method
				// of "this" instance of the MotorControl class with the provided value
				// and if the button was pressed or released 
				Advisory::pinfo("  connecting adjustPower channel");
				float value;
				comp->QueryFloatAttribute("value", &value);
				OIController::subscribeDigital(comp, 
					std::bind(&MotorControl::adjustPower, this, value, std::placeholders::_1));
			}
			else if (strcmp(name, "povPower") == 0)
			{
				// When thispov is changed call the povPower() method
				// of "this" instance of the MotorControl class with the provided value
				// and the POV direction 
				Advisory::pinfo("  connecting povPower channel");
				float value;
				comp->QueryFloatAttribute("value", &value);
				OIController::subscribeInt(comp, 
					std::bind(&MotorControl::setPovPower, this, value, std::placeholders::_1));
			}
			else
			{
				Advisory::pcaution("  OI with unexpected name (%s) was ignored", name);
			}
		}
		else
		{
			Advisory::pcaution("  OI with no name was ignored");
		}
		
		comp = comp->NextSiblingElement("oi");
	}
}

/*******************************************************************************	
 * 
 * @brief Release any resources allocated by this object
 * 
 * NOTE: This method will likely never be called because we just turn the
 *       robot power off when finished. It's just good practice to think about
 *       cleaning up your resources when the class is deleted.
 * 
 ******************************************************************************/
MotorControl::~MotorControl(void)
{
	data_logger.close();

	if (motor_a != nullptr)
	{
		delete motor_a;
		motor_a = nullptr;
	}

	if (motor_b != nullptr)
	{
		delete motor_b;
		motor_b = nullptr;
	}
}

/*******************************************************************************
 *
 * @brief Initialize control values
 * 
 * This method is intended to provide checks that make sure all required
 * hardware components were created while parsing the XML in the constructor.
 * 
 * A flag is set to indicate if all of the needed hardware was created, later
 * in the code (in doPeriodic) the flag is checked once so we don't need to
 * add nullptr checks for all of the hardware components every cycle.
 * 
 * Additional hardware initialization can also be done in this method
 * 
 ******************************************************************************/
void MotorControl::controlInit(void)
{
	bool is_ready = true;

	if (motor_a == nullptr)
	{
		is_ready = false;
		Advisory::pcaution("MotorControl requires at least one motor but none were found");
	}
	else
	{
		if (motor_a_max_current < 100.0)
		{
			motor_a->setCurrentLimit(motor_a_max_current);
			motor_a->setCurrentLimitEnabled(true);
		}
	}

	// motor_b is optional hardware so it does not impact
	// the is_ready check, but it must be checked before it 
	// is used in all other places in the code
    if ((motor_b != nullptr) && (motor_b_max_current < 100.0))
    {
        motor_b->setCurrentLimit(motor_b_max_current);
        motor_b->setCurrentLimitEnabled(true);
    }

	hardware_is_ready = is_ready;
}

/*******************************************************************************	
 *
 * @brief Reset values when entering DISABLED
 * 
 * This method gets called by the base Robot Class at the beginning of the
 * DISABLED game phase. The DISABLED game phase happens at least three times
 * during a match, when the robot first powers up, between auton and teleop,
 * and when the match ends.
 * 
 * It is a good idea to set things to prevent unexpected motion or actions
 * when the robot is enabled.
 * 
 * Also, most of our controls should create a data logger. Data loggers help
 * us understand what our robot is doing when running. The data logger
 * can be closed when the robot is disabled.
 * 
 ******************************************************************************/
void MotorControl::disabledInit(void)
{
	data_logger.close();

	motor_percent_target = 0.0;
	motor_percent_command = 0.0;
}

/*******************************************************************************	
 *
 * @brief Reset values when entering AUTON
 * 
 * This method gets called by the base Robot Class at the beginning of the
 * AUTON game phase. 
 * 
 * Any variables that can be changed by the user should be reset to avoid
 * any unexpected motion. 
 * 
 * The data logger should be opened and the logHeaders() method of this class
 * should be called.
 *  
 ******************************************************************************/
void MotorControl::autonomousInit(void)
{
    data_logger.open("_auton");
    logHeaders();
   
	motor_percent_target = 0.0;
	motor_percent_command = 0.0;
}

/*******************************************************************************	
 *
 * @brief Reset values when entering TELEOP
 * 
 * This method gets called by the base Robot Class at the beginning of the
 * TELEOP game phase. 
 * 
 * Any variables that can be changed by the user should be reset to avoid
 * any unexpected motion. 
 * 
 * The data logger should be opened and the logHeaders() method of this class
 * should be called.
 * 
 ******************************************************************************/
void MotorControl::teleopInit(void)
{
    data_logger.open("_teleop");
    logHeaders();
   
	motor_percent_target = 0.0;
	motor_percent_command = 0.0;
}

/*******************************************************************************	
 *
 * @brief Reset values when entering TEST
 * 
 * This method gets called by the base Robot Class at the beginning of the
 * TEST game phase. 
 * 
 * Any variables that can be changed by the user should be reset to avoid
 * any unexpected motion. 
 * 
 * The data logger should be opened and the logHeaders() method of this class
 * should be called.
 *  
 ******************************************************************************/
void MotorControl::testInit(void)
{
    data_logger.open("_test");
    logHeaders();
   
	motor_percent_target = 0.0;
	motor_percent_command = 0.0;
}

/*******************************************************************************
 *
 * @brief	Set the target power to the given value
 * 
 * This method can be mapped to an analog OI input, it is also called by 
 * methods that set power based on button presses and by macro steps.
 *  
 * @param	value	the new value for the target power, this is the percent
 *                  duty cycle that the motor should run at, the value will be
 *                  limited between motor_min_control and motor_max_control
 *                  by this method.
 * 
 ******************************************************************************/
void MotorControl::setPower(float value)
{
	motor_percent_target = gsu::Filter::limit(value, motor_min_percent, motor_max_percent);
}

/*******************************************************************************
 *
 * @brief	Latch the power to a given value when a button is pressed
 * 
 * This method set the target motor power to a given value when a button is 
 * pressed and does nothing when the button is released. It can be mapped to 
 * button presses that are intended to set motor power for a long time.
 * 
 * @param 	value	the motor power value as a percent of the duty cycle from 
 * 					motor_min_control to motor_max_control
 * 
 * @param	pressed if true the motor target power will be set to the specified
 * 					value, if false the motor target power will not be changed,
 * 					default = true
 * 
 ******************************************************************************/
void MotorControl::setLatchedPower(float value, bool pressed)
{
	Advisory::pinfo("MotorControl::setPower(%f, %s)", value, pressed?"on":"off");
	if(pressed)
	{
		setPower(value);
	}
}

/*******************************************************************************
 *
 * @brief	Set the target motor power to a given value while a button is pressed
 * 
 * This method sets the target motor value to the provided value when a button 
 * is pressed and sets it to 0.0 when the button is released. It can be mapped to 
 * button presses that are intended to set motor power for short bursts.
 * 
 * @param 	value	the motor power value as a percent of the duty cycle from 
 * 					motor_min_control to motor_max_control
 * 
 * @param	pressed	if true the motor target power will be set to the specified
 * 					value, if false the motor target power will be set to 0.0,
 * 					default = true
 * 
 ******************************************************************************/
void MotorControl::setMomentaryPower(float value, bool pressed)
{
	Advisory::pinfo("MotorControl::momentaryPower(%f, %s)", value, pressed?"on":"off");
	if(pressed)
	{
		setPower(value);
	}
	else
	{
		setPower(0.0);
	}
}

/*******************************************************************************
 *
 * @brief 	Adjust the target motor power by the specified amount.
 * 
 * This is intended to increase or decrease the motor power by some amount
 * when a button is pressed and do nothing when the button is released.
 * 
 * @param 	value	the amount that the motor power should be adjusted as a
 *                  percentage, this can be positive or negative.
 * 
 * @param	on		if true the motor target power will be adjusted by the specified
 * 					value, if false the motor target power will not be changed,
 * 					default = true
 * 
 ******************************************************************************/
void MotorControl::adjustPower(float value, bool on)
{
	Advisory::pinfo("MotorControl::adjustPower(%f, %s)", value, on?"on":"off");
	if(on)
	{
		setPower(motor_percent_target + value);
	}
}

/*******************************************************************************
 *
 * @brief	Set the target power with POV inputs
 * 
 * This method uses the POV input as several buttons to set the motor power
 * to several different powers based on the provided value and the POV direction. 
 * This is more of an example of how to use the POV input, it is not a very 
 * practical use of a POV.
 * 
 * @param	value	the base input magnitude for the target power.
 * 
 * @param	direction	An angular direction that is used to adjust the
 *                      provided value based on which portion of the POV
 *                      was pressed.
 * 
 ******************************************************************************/
void MotorControl::setPovPower(float value, int direction)
{
		Advisory::pinfo("MotorControl::povPower(%f, %d)", value, direction);

		switch(direction)
		{
			case 0:   setPower(value * 1.0);  break;
			case 45:  setPower(value * 0.5);  break;
			case 90:  setPower(value * 0.0);  break;
			case 135: setPower(value * -0.5); break;
			case 180: setPower(value * -1.0); break;
			case 225: setPower(value * -0.5); break;
			case 270: setPower(value * 0.0);  break;
			case 315: setPower(value * 0.5);  break;
			default:  setPower(0.0);
		}
}


/*******************************************************************************	
 *
 * @brief send data to the dashboard
 * 
 * This method is called by the Robot base class every so often, normally about
 * every half second. This method sends data to the "SmartDashboard" and other
 * user displays that use the SmartDashboard interface.
 * 
 ******************************************************************************/
void MotorControl::publish()
{
	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
	SmartDashboard::PutNumber(getName() + " cycles: ", getCyclesSincePublish());
	
    SmartDashboard::PutBoolean(getName() + " upper_limit: ", upper_limit_pressed);
    SmartDashboard::PutBoolean(getName() + " lower_limit: ", lower_limit_pressed);

	SmartDashboard::PutNumber(getName() + " percent_target: ", motor_percent_target);
	SmartDashboard::PutNumber(getName() + " percent_command: ", motor_percent_command);
	SmartDashboard::PutNumber(getName() + " percent_actual: ", motor_percent_command);
}

/*******************************************************************************
 * 
 * @brief write column headers to the log file
 * 
 * This method should be called by this class every time the data logger open 
 * method is called (each enabled phase change) so every log file has the 
 * data headers at the top of the columns.
 * 
 * This classes log method should write data to the log file such that it
 * matches the headers.
 * 
 ******************************************************************************/
void MotorControl::logHeaders(void)
{
    gsu::DataBuffer* line_buffer = data_logger.getEmptyBuffer();
    if (line_buffer == nullptr)
    {
       return;
    }

    char* line_ptr = (char*)(line_buffer->data);
    uint16_t buffer_size = line_buffer->getBufferSize();
    uint16_t remaining_bytes = buffer_size;

     line_ptr += snprintf(line_ptr, remaining_bytes, "%s", "time,");
     remaining_bytes = buffer_size - (line_ptr - (char*)(line_buffer->data));

    line_ptr += snprintf(line_ptr, remaining_bytes, "%s,%s,%s,%s,%s\n",
		"upper_limit_pressed",
		"lower_limit_pressed",
		"motor_percent_target",  
		"motor_percent_command", 
		"motor_percent_actual"  
    );

//    remaining_bytes = buffer_size - (line_ptr - (char*)(line_buffer->data));
    line_ptr[buffer_size-1] = '\0'; // if too much is put into the buffer, it may not be null terminated

    data_logger.putFullBuffer(line_buffer);
}

/*******************************************************************************
 *
 * @brief write current values to the log file
 * 
 * This method should be called at the end of this classes doPeriodic() method
 * to log data that will help determine what is happening when this control is 
 * running. It should log target, commanded, and actual values for things whenever
 * possible.
 * 
 ******************************************************************************/
void MotorControl::log(void)
{
   gsu::DataBuffer* line_buffer = data_logger.getEmptyBuffer();
    if (line_buffer == nullptr)
    {
        return;
    }

    char* line_ptr = (char*)(line_buffer->data);
    uint16_t buffer_size = line_buffer->getBufferSize();
    uint16_t remaining_bytes = buffer_size;

    line_ptr += snprintf(line_ptr, remaining_bytes, "%s,", gsi::Time::getTimeString(gsi::Time::FORMAT_YMDHMSu).c_str());
    remaining_bytes = buffer_size - (line_ptr - (char*)(line_buffer->data));

    line_ptr += snprintf(line_ptr, remaining_bytes, "%d,%d,%f,%f,%f\n",
 		upper_limit_pressed,
		lower_limit_pressed,
		motor_percent_target,  
		motor_percent_command, 
		motor_percent_actual  
    );

//    remaining_bytes = buffer_size - (line_ptr - (char*)(line_buffer->data));
   line_ptr[buffer_size-1] = '\0';

    data_logger.putFullBuffer(line_buffer);
}

/*******************************************************************************
 *
 * @brief perform periodic checks and update to this control
 * 
 * This method will be called by the robot base once a period to do anything 
 * that is needed, in this case it just sets the motor power to the current value.
 * 
 * The period can be set in the controls XML class, this method is called by 
 * the base class at all times, game phase dependent operations should be
 * wrapped in if statements.
 * 
 ******************************************************************************/
void MotorControl::doPeriodic()
{
	//
	// If the required hardware has not been created, don't do anything
	//
	if ( ! hardware_is_ready )
	{
		return;
	}

	//
	// Get inputs
	//
	motor_a->doUpdate();

	upper_limit_pressed = motor_a->isUpperLimitPressed();
	lower_limit_pressed = motor_a->isLowerLimitPressed();

	motor_percent_actual = motor_a->getPercent();

	// motor_b is optional, so make sure it was created before trying to use it
	if (motor_b != nullptr)
	{
		motor_b->doUpdate();
	}

    // don't let the user change power levels while DISABLED
	if (getPhase() == DISABLED)
	{
		motor_percent_target = 0.0;
		motor_percent_command = 0.0;
	}

	//
	// Processing
	//

	// The step filter will move the previous command toward the target in small steps
	// to prevent rapid acceleration and deceleration to help make hardware last longer
	motor_percent_command = gsu::Filter::step(motor_percent_command, 0.05, motor_percent_target);

	// limiting values between 0.0 and another value prevents the motor from being commanded 
	// in the direction of a pressed limit switch
    if (upper_limit_pressed)
	{
		motor_percent_command = gsu::Filter::limit(motor_percent_command, 0.0, motor_max_percent);
	}

	if (lower_limit_pressed)
	{
		motor_percent_command = gsu::Filter::limit(motor_percent_command, motor_min_percent, 0.0);
	}

	//
	// Set Outputs
	//
	motor_a->setPercent(motor_percent_command);

	// motor_b is optional, so make sure it was created before trying to use it
	if (motor_b != nullptr)
	{
		motor_b->setPercent(motor_percent_command);
	}

	if (getPhase() != DISABLED)
	{
		log();
	}
}

// =============================================================================
// =============================================================================
// ===
// === MotorControlMSSetPower
// ===
// =============================================================================
// =============================================================================

/*******************************************************************************
 *
 * @brief Create an instance of the a macro step that can be used to set the
 *        power of the MotorControl motors.
 * 
 * The MacroStep constructor is called when the Macro File is read, so at
 * robot initialization or when a new auton script is selected. This needs
 * to read the XML and remember anything that will be used when the 
 * macro step is executed.
 *
 * @param type 	the macro step type is passed to the MacroStep base class so
 *             	meaningful debug can be provided
 * 
 * @param xml	The XML element for this MacroStep, the base class pulls some 
 *              information from this, this class can pull additional 
 *              information
 * 
 * @param control	a pointer to the parent control so this class can call
 *                  methods of the parent control
 * 
 ******************************************************************************/
MotorControlMSSetPower::MotorControlMSSetPower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (MotorControl*)control;
	target_power = 0.0;

	xml->QueryFloatAttribute("power", &target_power);
}

/*******************************************************************************
 *
 * @brief Inititalize the macro step by setting the power in the parent control
 * 
 * When a macro is being run the macro step init method is called as the 
 * macro transitions to this step.
 * 
 ******************************************************************************/
void MotorControlMSSetPower::init(void)
{
	parent_control->setPower(target_power);
}

/*******************************************************************************
 *
 * @brief return the nextstep to tell the macro controller that this step
 *        has finishd evrything it needs to do
 * 
 * Some macro steps require time or they wait for a condition, the update method 
 * is called as long as the update method returns "this" once it is finished
 * it should return "next_step" for sequence steps or some other step 
 * variables for different types of macro steps
 * 
 * @return	a pointer to the next step that should be called
 * 
 ******************************************************************************/
MacroStep* MotorControlMSSetPower::update(void)
{
	return next_step;
}

