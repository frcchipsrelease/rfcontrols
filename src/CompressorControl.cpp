/*******************************************************************************
 *
 * File: CompressorControl.cpp
 *
 * Written by:
 * 	Clear Creek Independent School District FIRST Robotics
 * 	FRC Team 118, The Robonauts
 *  FRC Team 324, Chips
 * 	NASA, Johnson Space Center
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include <math.h>

#include "rfutilities/RobotUtil.h"

#include "gsutilities/Advisory.h"

#include "rfcontrols//CompressorControl.h"
#include "rfhardware/HardwareFactory.h"

#include "frc/smartDashboard/SmartDashboard.h"

using namespace tinyxml2;
using namespace gsi;
using namespace frc;

/******************************************************************************
 *
 *
 *
 ******************************************************************************/
CompressorControl::CompressorControl(std::string name, tinyxml2::XMLElement *xml)
	: PeriodicControl(name)
{
	XMLElement *element;

	Advisory::pinfo("========================= Creating Compressor Control [%s] =========================",
			name.c_str());

	compressor_relay = nullptr;
	compressor_switch = nullptr;

	compressor_switch_invert = false;
	compressor_on = false;

	//
	// Parse the XML
	//

	element = xml->FirstChildElement("digital_input");
	if (element != nullptr)
	{
		Advisory::pinfo("  creating switch for compressor");
        compressor_switch = HardwareFactory::createLimitSwitch(element);
		compressor_switch_invert = element->BoolAttribute("invert") ? true : false;
	}

	element = xml->FirstChildElement("relay");
	if (element != NULL)
	{
		Advisory::pinfo("  creating compressor relay");
        compressor_relay = HardwareFactory::createRelay(element);
	}

	if (compressor_switch == nullptr)
	{
		Advisory::pinfo("  WARNING: failed to initialize compressor switch");
	}

	if (compressor_relay == nullptr)
	{
	    Advisory::pinfo("  WARNING: failed to create compressor relay");
	}
}

/******************************************************************************
 *
 * Destructor
 * 
 ******************************************************************************/
CompressorControl::~CompressorControl()
{
    Advisory::pinfo("CompressorSys::~CompressorSys");
}

/******************************************************************************
 *
 * Implements method required by PeriodicControl
 * 
 ******************************************************************************/
void CompressorControl::controlInit()
{

}

/******************************************************************************
 *
 * Implements method required by PeriodicControl
 *
 ******************************************************************************/
void CompressorControl::updateConfig()
{
}

/******************************************************************************
 *
 * Implements method required by PeriodicControl
 *
 ******************************************************************************/
void CompressorControl::disabledInit()
{
}

/******************************************************************************
 *
 * Implements method required by PeriodicControl
 *
 ******************************************************************************/
void CompressorControl::autonomousInit()
{
}

/******************************************************************************
 *
 * Implements method required by PeriodicControl
 *
 ******************************************************************************/
void CompressorControl::teleopInit()
{
}

/**********************************************************************
 *
 *
 **********************************************************************/
void CompressorControl::testInit(void)
{
}

/******************************************************************************
 *
 * Implements method required by PeriodicControl
 * 
 ******************************************************************************/
void CompressorControl::publish()
{
//	SmartDashboard::PutBoolean(std::string("  ") + getName() + "  ", (getCyclesSincePublish() > 0));
//	SmartDashboard::PutNumber(getName() +" cycles: ", getCyclesSincePublish());

	SmartDashboard::PutBoolean(getName() + " on: ", (compressor_switch != nullptr) && compressor_on);
//	SmartDashboard::PutNumber(getName() + " current: ", compressor_current);
}

/******************************************************************************
 *
 * Implements method required by PeriodicControl
 * 
 ******************************************************************************/
void CompressorControl::doPeriodic()
{
	if (compressor_switch != nullptr)
	{
		compressor_on = compressor_switch->Get() != compressor_switch_invert;
	}

	if (compressor_relay != nullptr)
	{
		compressor_relay->Set(compressor_on ? Relay::kForward : Relay::kOff);
	}
}
