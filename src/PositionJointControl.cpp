/*******************************************************************************
 * 
 * File: PositionJointControl.cpp
 *
 * This is an example of how to use a motor with an external encoder,
 * external limit switches, and external PIDs to do position control. 
 * 
 * Please keep in mind that most of the motor controllers we use can be 
 * configured to use external encoders, limit switches, and PID values
 * so this is at least as much a learning aid as it is useful. 
 * 
 * Writing code that uses generic interfaces allows us to switch hardware
 * without making changes to the software. It is harder to write software
 * like this but can save us when we need a quick hardware change. It also
 * helps us help others that don't use the same hardware that we do.
 * 
 * Some day I will try to write a version of this class that uses the motor to
 * do most of the closed loop work.
 * 
 * Written by:
 * 	Clear Creek Independent School District
 * 	NASA, Johnson Space Center
 *  FRC Team 324, Chips
 *
 * Acknowledgements:
 *
 * Copywrite and License:
 *   Copywrite and License information can be found in the LICENSE.md file 
 *   distributed with this software.
 *
 ******************************************************************************/
#include "rfcontrols//PositionJointControl.h"

#include "rfhardware/HardwareFactory.h"

#include "rfutilities/RobotUtil.h"
#include "rfutilities/MacroStepFactory.h"
#include "rfutilities/OIController.h"

#include "gsutilities/Advisory.h"
#include "gsutilities/Filter.h"

#include "gsinterfaces/Time.h"

#include "frc/smartdashboard/SmartDashboard.h" //WPI

#include <math.h>

#include <chrono>
#include <thread>
using namespace std::chrono_literals;

using namespace tinyxml2;
using namespace frc;

/*******************************************************************************	
 * 
 * Create an instance of a position control joint -- that is a joint with
 * a Motor, an Encoder, [a trajectory planner], a PID, and optionally two Limit 
 * Switches
 * 
 * @param	control_name the name of this control
 * 
 * @param	xml			the XML used to configure this object, see the class
 * 						definition for the XML format
 * 
 ******************************************************************************/
PositionJointControl::PositionJointControl(std::string control_name, XMLElement* xml)
	: PeriodicControl(control_name)
	, data_logger("/robot/logs/pjc/", control_name.c_str(), "csv", 10, 5, 512)
{
	Advisory::pinfo("========================= Creating Position Joint [%s] =========================", control_name.c_str());

	is_closed_loop = false;
	use_absolute_angles = false;

	motor = nullptr;
	motor_min_percent = -1.0;
	motor_max_percent = 1.0;

	motor_percent_target = 0.0;  
	motor_percent_command = 0.0; 
	motor_percent_actual = 0.0;  

	encoder = nullptr;
	encoder_position_raw = 0.0;
	encoder_position_rotation = 0.0;
	encoder_position_target = 0.0;
	encoder_position_actual = 0.0;

	upper_limit = nullptr;
 	upper_limit_pressed = false;

	lower_limit = nullptr;
	lower_limit_pressed = false;

	//
	// Parse the Controls XML
	//
	XMLElement *comp;
	const char *name;

	// Query for any attributes specified in the control element 
	// here. If you query for an attribute that was not specified, the
	// variable that is passed in will not be updated.
    xml->QueryBoolAttribute("use_absolute_angles", &use_absolute_angles);
	Advisory::pinfo("use_absolute_angles=%s", use_absolute_angles?"true":"false");

	comp = xml->FirstChildElement("motor");
	if (comp != nullptr)
	{
	    xml->QueryFloatAttribute("min_control", &motor_min_percent);
	    xml->QueryFloatAttribute("max_control", &motor_max_percent);

		Advisory::pinfo("creating motor");
		motor = HardwareFactory::createMotor(comp);
	}

	comp = xml->FirstChildElement("encoder");
	if (comp != nullptr)
	{
		Advisory::pinfo("creating encoder");
		encoder = HardwareFactory::createEncoder(comp);
	}

	comp = xml->FirstChildElement("pid");
	if (comp != nullptr)
	{
		Advisory::pinfo("creating PID");
		pid = HardwareFactory::createPid(comp);
	}

	comp = xml->FirstChildElement("limit_switch");
	while(comp != nullptr)
	{
		name = comp->Attribute("name");
		if (name == nullptr)
		{
			Advisory::postCaution("horse with no name");
		}
		else if (strcmp(name, "upper") == 0)
		{
			Advisory::postInfo("creating %s limit switch", name);
			upper_limit = HardwareFactory::createLimitSwitch(comp);
		}
		else if (strcmp(name, "lower") == 0)
		{
			Advisory::postInfo("creating %s limit switch", name);
			lower_limit = HardwareFactory::createLimitSwitch(comp);
		}
		else
		{
			Advisory::postCaution("found limit switch with unknown name -- %s", name);
		}

		comp = comp->NextSiblingElement("limit_switch");
	}

	//
	// Load any Setpoints
	//
	comp = xml->FirstChildElement("setpoints");
	if (comp != nullptr)
	{
    	XMLElement *setpoint_comp;
		setpoint_comp = comp->FirstChildElement("setpoint");
 		while (setpoint_comp!=nullptr)
		{
			name = setpoint_comp->Attribute("name");

			float sp_position = std::numeric_limits<float>::quiet_NaN();
			setpoint_comp->QueryFloatAttribute("position", &sp_position);

			if ((name == nullptr) || std::isnan(sp_position))
			{
				Advisory::pcaution("could not load setpoint, setpoints require both a \"name\" and a \"position\" attribute");
			}
			else
			{
				Advisory::pinfo(" -- setpoint %20s   position=%7.2f", name, sp_position);
				setpoints[std::string(name)] = sp_position;
			}

    		setpoint_comp = setpoint_comp->NextSiblingElement("setpoint");
		}
	}

	//
	// Query for Operator Interface connections
	//
	// These allow us to configure joysticks, game-pads, and other
	// inputs the way the drivers like, they can change there minds
	// later and we just remap to something different.
	//
	// These mappings are connected to methods in this class with 
	// a subscribe and a standard C++ bind. This bind tells the 
	// OI Controller to call a method (like MotorControl::setPower) in
	// "this" instance of the class with the arguments (placeholders)
	// for the values that the OI Controller expect to pass to the
	// method. We can also bind in values that are parsed from the XML
	// or constants we define someplace else. This gives us a lot of 
	// flexibility in how we implement our code.
	//
	comp = xml-> FirstChildElement("oi");
	while (comp != nullptr)
	{
		name = comp->Attribute("name");

		if (name == nullptr)
		{
			Advisory::pcaution("  OI with no name was ignored");
		}
		else if (strcmp(name, "analogPower") == 0)
		{
			Advisory::pinfo("  connecting setPower channel");
			OIController::subscribeAnalog(comp, 
				std::bind(&PositionJointControl::setPower, this, std::placeholders::_1));
		}
		else if (strcmp(name, "latchPower") == 0)
		{ 
			Advisory::pinfo("  connecting setPower channel");
			float value = 0.1;
			comp->QueryFloatAttribute("value", &value);
			OIController::subscribeDigital(comp, 
				std::bind(&PositionJointControl::setLatchedPower, this, value, std::placeholders::_1));
		}
		else if (strcmp(name, "momentaryPower") == 0)
		{
			Advisory::pinfo("  connecting momentaryPower channel");
			float value = 0.1;
			comp->QueryFloatAttribute("value", &value);
			OIController::subscribeDigital(comp, 
				std::bind(&PositionJointControl::setMomentaryPower, this, value, std::placeholders::_1));
		}
		else if (strcmp(name, "closedLoop") == 0)
		{
			Advisory::pinfo("  connecting closedLoop channel");
			bool closed = false;
			comp->QueryBoolAttribute("closed", &closed);
			OIController::subscribeDigital(comp, 
				std::bind(&PositionJointControl::setClosedLoop, this, closed, std::placeholders::_1));
		}
		else if (strcmp(name, "nudgePositionPov") == 0)
		{
			Advisory::pinfo("  connecting nudgePositionPov channel");
			float step = 15.0;
			comp->QueryFloatAttribute("step", &step);
			OIController::subscribeInt(comp, 
				std::bind(&PositionJointControl::nudgePositionPov, this, step, std::placeholders::_1));
		}
		else if (strcmp(name, "selectSetpoint") == 0)
		{
			Advisory::pinfo("  connecting selectSetpoint channel");
			const char* sp_name = comp->Attribute("setpoint");
			OIController::subscribeDigital(comp, 
				std::bind(&PositionJointControl::selectSetpoint, this, std::string(sp_name), std::placeholders::_1));
		}
		else
		{
			Advisory::pcaution("  OI with unexpected name (%s) was ignored", name);
		}

		comp = comp->NextSiblingElement("oi");
	}

#if 0
    //
	// Register Macro Steps
	//
	new MacroStepProxy<MSPosJointSetPower>(control_name, "SetPower", this);
	new MacroStepProxy<MSPosJointSetPosition>(control_name, "SetPosition", this);
#endif
}

/*******************************************************************************	
 * 
 * Release any resources allocated by this object
 * 
 ******************************************************************************/
PositionJointControl::~PositionJointControl(void)
{
	data_logger.close();

	if (encoder != nullptr)
	{
		delete encoder;
		encoder = nullptr;
	}

	if (motor != nullptr)
	{
		delete motor;
		motor = nullptr;
	}

	if (pid != nullptr)
	{
		delete pid;
		pid = nullptr;
	}

	if (upper_limit != nullptr)
	{
		delete upper_limit;
		upper_limit = nullptr;
	}

	if (lower_limit != nullptr)
	{
		delete lower_limit;
		lower_limit = nullptr;
	}
}

/*******************************************************************************	
 *
 * @brief	check to see if the hardware was created and configured, finish 
 * 			any additional initialization
 *
 ******************************************************************************/
void PositionJointControl::controlInit(void)
{
    bool check_ready = true;
	
	if(motor == nullptr)
	{
		Advisory::pwarning("%s Position Joint Control missing required component -- motor", getName().c_str());
		check_ready = false;
	}

 	if (encoder == nullptr)
	{
		Advisory::pwarning("%s Position Joint Control missing required component -- encoder", getName().c_str());
		check_ready = false;
	}

	if (pid == nullptr)
	{
		Advisory::pwarning("%s Position Joint Control missing required component -- pid", getName().c_str());
		check_ready = false;
	}

	if (upper_limit == nullptr)
	{
		Advisory::postCaution("%s Position Joint Control missing optional limit_switch -- name=\"upper\"", getName().c_str());
		// optional so don't change check_ready
	}
	
	if (lower_limit == nullptr)
	{
		Advisory::postCaution("%s Position Joint Control missing optional limit_switch -- name=\"lower\"", getName().c_str());
		// optional so don't change check_ready
	}
	
	if ((use_absolute_angles) && (encoder != nullptr) && (pid != nullptr))
	{
		pid->setIsAngle(true);

		if (! encoder->isCapableOf(rfh::Encoder::Capability::ABSOLUTE))
		{
			Advisory::pcaution("%s Position Joint Control configuration requested absolute angles but the encoder does not support this");
			use_absolute_angles = false;
			check_ready = false;
		}
	}

	hardware_is_ready = check_ready;
}

/*******************************************************************************
 *
 * When DISABLED mode starts, set targets to something safe
 * 
 ******************************************************************************/
void PositionJointControl::disabledInit()
{
	motor_percent_target = 0.0;
	motor_percent_command = 0.0;
	encoder_position_target = encoder_position_actual;
}

/*******************************************************************************
 *
 * When AUTON mode starts, set targets to something safe
 * 
 ******************************************************************************/
void PositionJointControl::autonomousInit()
{
    data_logger.open("_auton");
    logHeaders();

	motor_percent_target = 0.0;
	motor_percent_command = 0.0;
	encoder_position_target = encoder_position_actual;
}

/*******************************************************************************
 *
 * When TELEOP mode starts, set targets to something safe
 * 
 ******************************************************************************/
void PositionJointControl::teleopInit()
{
    data_logger.open("_auton");
    logHeaders();

	motor_percent_target = 0.0;
	motor_percent_command = 0.0;
	encoder_position_target = encoder_position_actual;
}

/*******************************************************************************
 *
 * When TEST mode starts, set targets to something safe
 * 
 ******************************************************************************/
void PositionJointControl::testInit()
{
    data_logger.open("_auton");
    logHeaders();

	motor_percent_target = 0.0;
	motor_percent_command = 0.0;
	encoder_position_target = encoder_position_actual;
}

/*******************************************************************************
 *
 * @brief	Set closed/open loop
 * 
 * This method sets the closed loop flag to true or false. 
 * 
 * The second argument is provided so this can be bound to a button press, 
 * if the button pressed argument is true (the button is pressed) the closed 
 * loop will be set, this will do nothing if the pressed argument is false
 * (the button is released).
 * 
 * @param 	closed	true to set this control to closed loop false to set it
 *                  to open loop
 * 
 * @param	pressed (optional) if true the closed loop will be set to the 
 *                  closed value, default=true
 * 
 ******************************************************************************/
void PositionJointControl::setClosedLoop(bool closed, bool pressed)
{
	if (pressed)
	{
		is_closed_loop = closed;
	}
}

/*******************************************************************************
 *
 * @return true if this control is in the closed loop mode
 * 
 ******************************************************************************/
bool PositionJointControl::isClosedLoop(void)
{
	return is_closed_loop;
}

/*******************************************************************************
 *
 * @brief	Set the target power to the given value
 * 
 * This method can be mapped to an analog OI input, it is also called by 
 * methods that set power based on button presses and by macro steps.
 *  
 * @param	value	the new value for the target power, this is the percent
 *                  duty cycle that the motor should run at, the value will be
 *                  limited between motor_min_control and motor_max_control
 *                  by this method.
 * 
 ******************************************************************************/
void PositionJointControl::setPower(float value)
{
	motor_percent_target = gsu::Filter::limit(value, motor_min_percent, motor_max_percent);
}

/*******************************************************************************
 *
 * @brief	Latch the power to a given value when a button is pressed
 * 
 * This method sets the target motor power to a given value.
 * 
 * The pressed argument is provided so this can be bound to a button press.
 * If the pressed value is true (the button is pressed) this will set the
 * value, if the pressed value is false (the button is released), this 
 * method will do nothing.
 * 
 * @param 	value	the motor power value as a percent of the duty cycle from 
 * 					motor_min_control to motor_max_control
 * 
 * @param	pressed (optional) if true the motor target power will be set to the specified
 * 					value, if false the motor target power will not be changed,
 * 					default = true
 * 
 ******************************************************************************/
void PositionJointControl::setLatchedPower(float value, bool pressed)
{
	Advisory::pinfo("MotorControl::setPower(%f, %s)", value, pressed?"on":"off");
	if(pressed)
	{
		setPower(value);
	}
}

/*******************************************************************************
 *
 * @brief	Set the target motor power to a given value while a button is pressed
 * 
 * This method sets the target motor power to a given value.
 * 
 * The pressed argument is provided so this can be bound to a button press.
 * If the pressed value is true (the button is pressed) this will set the
 * provided value, if the pressed value is false (the button is released), 
 * the value will be set to 0.0.
 * 
 * @param 	value	the motor power value as a percent of the duty cycle from 
 * 					motor_min_control to motor_max_control
 * 
 * @param	pressed	(optional) if true the motor target power will be set to the 
 * 					specified value, if false the motor target power will be set
 * 					to 0.0, default = true
 * 
 ******************************************************************************/
void PositionJointControl::setMomentaryPower(float value, bool pressed)
{
	Advisory::pinfo("MotorControl::momentaryPower(%f, %s)", value, pressed?"on":"off");
	if(pressed)
	{
		setPower(value);
	}
	else
	{
		setPower(0.0);
	}
}

/*******************************************************************************
 *
 * @brief 	Adjust the target motor power by the specified amount.
 * 
 * This method increase or decrease the motor power by the specified amount.
 * 
 * The pressed argument is provided so this can be bound to a button press.
 * If the pressed value is true (the button is pressed) this will adjust
 * the motor power by the provided value, if the pressed value is false (the 
 * button is released), this method will do nothing.
 * 
 * @param 	value	the amount that the motor power should be adjusted as a
 *                  percentage, this can be positive or negative.
 * 
 * @param	pressed (optional) if true the motor target power will be adjusted by the specified
 * 					value, if false the motor target power will not be changed,
 * 					default = true
 * 
 ******************************************************************************/
void PositionJointControl::adjustPower(float value, bool pressed)
{
	Advisory::pinfo("MotorControl::adjustPower(%f, %s)", value, pressed?"on":"off");
	if(pressed)
	{
		setPower(motor_percent_target + value);
	}
}

/*******************************************************************************
 *
 * @brief adjust the target position by a specified amount
 * 
 * This method is intended to be an example of how the POV input can be used
 * but it may not be the best user interface.
 * 
 * The direction argument is passed from the POV input to indicate which
 * part of the POV input was pressed, these represent compase directions in
 * 45 degree increments with nothing being pressed represetned by a 
 * negative value.
 * 
 * This will adjust the input based on the value and the direction parameters
 * 
 * With this implementation the target position will be adjusted by the 
 * specified value in the positive direction if the NORTH (0) direction
 * is specified, by the specified value in the negative direction if
 * the SOUTH (180) direction is pressed, by 1/3 of the value in the EAST
 * and WEST directions, and will not be adjusted for any other values.
 * 
 * @param	value	the base value to adjust the target position
 * 
 * @param	direction	the POV dirction input
 * 
 ******************************************************************************/
void PositionJointControl::nudgePositionPov(float value, int direction)
{
	if (is_closed_loop)
	{
		switch (direction)
		{
			case 0: 	setPosition(encoder_position_target + value); break;
			case 90: 	setPosition(encoder_position_target + value/3.0); break;
			case 180:   setPosition(encoder_position_target - value); break;
			case 270:   setPosition(encoder_position_target - value/3.0); break;

			default: /* don't adjust */ break;
		}
	}
}

/*******************************************************************************
 *
 * @brief set the target position to the specified value
 * 
 * If this instance was configured to use absolute positions and the position
 * specified is outside of -180 to 180, the specified value will be adjusted
 * by unwrapping it until it is in the specified range.
 * 
 * @param	position	the desired target position in user unitis (degrees)
 * 
 ******************************************************************************/
void PositionJointControl::setPosition(float position)
{
	encoder_position_target = position;
	if (use_absolute_angles)
	{
		while (encoder_position_target > 180.0)
		{
			encoder_position_target -= 360.0;
		}

		while (encoder_position_target < -180.0)
		{
			encoder_position_target += 360.0;
		}
	}
}

/*******************************************************************************
 *
 * @return	the actaul position in user units (degrees)
 * 
 ******************************************************************************/
float PositionJointControl::getPosition(void)
{
	return encoder_position_actual;
}

/*******************************************************************************
 *
 * @brief  get an indication of whether or not the joint is at the target 
 *         position.
 * 
 * @param	tolerance	the allowable error in user units (degrees)
 * 
 * @return true if the difference between the target position and the 
 * 			actaul position is less than the specified tolerance
 * 
 ******************************************************************************/
bool PositionJointControl::isAtTarget(float tolerance)
{
	return (fabs(encoder_position_actual - encoder_position_target) < tolerance);
}

/*******************************************************************************
 *
 * @brief	set the target position to a named setpoint
 * 
 * If the specified setpoint name is not known, an error will be
 * printed an the value will not be adjusted.
 * 
 * The pressed argument is provided so this can be bound to a button press.
 * If pressed is false this will do nothing.
 * 
 * @param	sp_name	the name of the desired setpoint
 * 
 * @param	pressed	if the button is pressed or released
 * 
 ******************************************************************************/
void PositionJointControl::selectSetpoint(std::string sp_name, bool pressed)
{
	if (pressed)
	{
		std::map<std::string,float>::iterator ittr = setpoints.find(sp_name);
		if (ittr != setpoints.end())
		{
			setPosition(ittr->second);
		}
		else
		{
			Advisory::pcaution("%s could not select unknown setpoint of %s", 
				getName().c_str(), sp_name.c_str());
		}
	}
}

/**********************************************************************
 *
 * This method is used to initialize the log files, it is called from
 * teleop init and auton init methods, it should log the row of column
 * headers to the file.
 *
 **********************************************************************/
void PositionJointControl::logHeaders(void)
{
	gsu::DataBuffer* line_buffer = data_logger.getEmptyBuffer();
    if (line_buffer == nullptr)
    {
       return;
    }

    char* line_ptr = (char*)(line_buffer->data);
    uint16_t buffer_size = line_buffer->getBufferSize();
    uint16_t remaining_bytes = buffer_size;

     line_ptr += snprintf(line_ptr, remaining_bytes, "%s", "time,");
     remaining_bytes = buffer_size - (line_ptr - (char*)(line_buffer->data));

    line_ptr += snprintf(line_ptr, remaining_bytes, "%s,%s,%s,%s,%s,%s,%s,%s\n",
		"is_closed_loop",
		"motor_percent_target",  
		"motor_percent_command", 
		"motor_percent_actual",
		"encoder_position_target",  
		"encoder_position_actual",  
		"encoder_position_rotation",  
		"encoder_position_raw"
    );

//    remaining_bytes = buffer_size - (line_ptr - (char*)(line_buffer->data));
    line_ptr[buffer_size-1] = '\0'; // if too much is put into the buffer, it may not be null terminated

    data_logger.putFullBuffer(line_buffer);
}

/*******************************************************************************
 *
 * This method is used to write data to the log files, it is called from
 * the end of doPeriodic(), it should log a row of data that represents the
 * current pass through doPeriodic().
 *
 ******************************************************************************/
void PositionJointControl::logData(void)
{
	gsu::DataBuffer* line_buffer = data_logger.getEmptyBuffer();
    if (line_buffer == nullptr)
    {
        return;
    }

    char* line_ptr = (char*)(line_buffer->data);
    uint16_t buffer_size = line_buffer->getBufferSize();
    uint16_t remaining_bytes = buffer_size;

    line_ptr += snprintf(line_ptr, remaining_bytes, "%s,", gsi::Time::getTimeString(gsi::Time::FORMAT_YMDHMSu).c_str());
    remaining_bytes = buffer_size - (line_ptr - (char*)(line_buffer->data));

    line_ptr += snprintf(line_ptr, remaining_bytes, "%d.%f,%f,%f,%f,%f,%f,%f\n",
		is_closed_loop,
		motor_percent_target,  
		motor_percent_command, 
		motor_percent_actual,
		encoder_position_target,  
		encoder_position_actual,  
		encoder_position_rotation,  
		encoder_position_raw
    );

//    remaining_bytes = buffer_size - (line_ptr - (char*)(line_buffer->data));
   line_ptr[buffer_size-1] = '\0';

    data_logger.putFullBuffer(line_buffer);
}

/*******************************************************************************
 *
 * This method is called from the robot base at a regular interval to send
 * desired information to the drivers console.
 * 
 * This method should only send class variable data, it should not dereference
 * pointers to objects that may not have been initialized.
 * 
 ******************************************************************************/
void PositionJointControl::publish()
{
	SmartDashboard::PutBoolean(getName() + " closed loop: ", is_closed_loop);

	SmartDashboard::PutNumber(getName() + " percent target: ", motor_percent_target);
	SmartDashboard::PutNumber(getName() + " percent command: ", motor_percent_command);
	SmartDashboard::PutNumber(getName() + " percent actual: ", motor_percent_actual);

	SmartDashboard::PutNumber(getName() + " position raw: ", encoder_position_raw);
	SmartDashboard::PutNumber(getName() + " position rotation: ", encoder_position_rotation);
	SmartDashboard::PutNumber(getName() + " position actual: ", encoder_position_actual);
	SmartDashboard::PutNumber(getName() + " position target: ", encoder_position_target);

	SmartDashboard::PutBoolean(getName() + " upper limit: ", upper_limit_pressed);
	SmartDashboard::PutBoolean(getName() + " lower limit: ", lower_limit_pressed);
}

/*******************************************************************************
 *
 * This method will be called once a period to do anything that is needed,
 * in this case it just sets the motor power to the current value.
 * 
 * In general, doPeriodic() should: make sure the hardware was initialized, 
 * read inputs, clear user inputs when disabled, process data, write outputs, 
 * and log the data. It makes it easier for everyone to understand the code 
 * if all doPeriodic() methods follow this general outline. 
 * 
 ******************************************************************************/
void PositionJointControl::doPeriodic()
{
	//
	// Check the Hardware
	//
	if(hardware_is_ready == false)
	{
		Advisory::pinfo("%s PositionJointControl is not ready", getName());
		return;
	}

	//
	// Get Inputs
	//
	motor->doUpdate();

    if (upper_limit != nullptr)
	{
		upper_limit_pressed = upper_limit->isPressed();
	}
	else  // with some motors, the limit switch may be connected to the motor
	{
		upper_limit_pressed = motor->isUpperLimitPressed();
	}

    if (lower_limit != nullptr)
	{
		lower_limit_pressed = lower_limit->isPressed();
	}
	else  // with some motors, the limit switch may be connected to the motor
	{
		lower_limit_pressed = motor->isLowerLimitPressed();
	}

	motor_percent_actual = motor->getPercent();

	if (use_absolute_angles)
	{
		encoder_position_raw = encoder->getAbsoluteRawPosition();
		encoder_position_rotation = encoder->getAbsoluteRotationCenteredPosition();
		encoder_position_actual = encoder->getAbsoluteCenteredPosition();
	}
	else
	{
		encoder_position_raw = encoder->getRawPosition();
		encoder_position_rotation = encoder->getRotationPosition();
		encoder_position_actual = encoder->getPosition();
	}

	//
	// Make sure the motor doesn't jump when enabled
	//
	if (getPhase() == DISABLED)
	{
		motor_percent_target = 0.0;
		motor_percent_command = 0.0;

		encoder_position_target = encoder_position_actual;
	}

	//
	// Do Processing
	//
	if (is_closed_loop)// Command the position
	{
		// make sure the motor doesn't take off if switched to open loop
		motor_percent_target = 0.0;

		// if the limit is pressed, then don't go any further in that direction
		if ((upper_limit_pressed) && (encoder_position_target >= encoder_position_actual))
		{
			encoder_position_target = encoder_position_actual;
		}

		if ((lower_limit_pressed) && (encoder_position_target <= encoder_position_actual))
		{
			encoder_position_target = encoder_position_actual;
		} 

		motor_percent_command = pid->calculateCommand(encoder_position_target, encoder_position_actual);
	}
	else
	{
		// make sure the motor doesn't jump if switched to closed loop
		encoder_position_target = encoder_position_actual;

		// The step filter will move the previous command toward the target in small steps
		// to prevent rapid acceleration and deceleration to help make hardware last longer
		motor_percent_command = gsu::Filter::step(motor_percent_command, 0.05, motor_percent_target);

		// limiting values between 0.0 and another value prevents the motor from being commanded 
		// in the direction of a pressed limit switch
		if (upper_limit_pressed)
		{
			motor_percent_command = gsu::Filter::limit(motor_percent_command, 0.0, motor_max_percent);
		}

		if (lower_limit_pressed)
		{
			motor_percent_command = gsu::Filter::limit(motor_percent_command, motor_min_percent, 0.0);
		} 
	}

	//
	// Set Outputs
	//
	motor->setPercent(motor_percent_command);

	//
	// Log data
	//
	logData();
}

#if 0
// =============================================================================
// =============================================================================
// =============================================================================

/*******************************************************************************
 *
 ******************************************************************************/
MSPosJointSetPower::MSPosJointSetPower(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (PositionJointControl *)control;
	power = 0.0;
	
	xml->QueryFloatAttribute("power", &power);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSPosJointSetPower::init(void)
{
	parent_control->setAnalog(PositionJointControl::CMD_MOVE_ANALOG, power);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSPosJointSetPower::update(void)
{
 	return next_step;
}

// =============================================================================
// =============================================================================
// =============================================================================

/*******************************************************************************
 *
 ******************************************************************************/
MSPosJointSetPosition::MSPosJointSetPosition(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	parent_control = (PositionJointControl *)control;
	target_position = 0.0;
	target_tolerance = 0.1;
	wait_for_position = false;

	xml->QueryFloatAttribute("position", &target_position);
	xml->QueryFloatAttribute("tolerance", &target_tolerance);
	xml->QueryBoolAttribute("wait", &wait_for_position);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSPosJointSetPosition::init(void)
{
	parent_control->setPosition(target_position);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSPosJointSetPosition::update(void)
{
	if (wait_for_position)
	{
		if (parent_control->isAtTarget(target_tolerance))
		{
			return next_step;
		}
		else
		{
			return this;
		}
	}
	else
	{
 		return next_step;
	}
}

/*******************************************************************************
 *
 ******************************************************************************/
MSPosJointLimelight::MSPosJointLimelight(std::string type, tinyxml2::XMLElement *xml, void *control)
	: MacroStepSequence(type, xml, control)
{
	Advisory::pinfo("MSPosJointLimelight Const");
	parent_control = (PositionJointControl *)control;
	limelight_control = 1;
	target_position = 0.0;
	target_tolerance = 0.1;
	wait_for_position = false;
	parent_control->getLimelightHardwareSettings(heightDifference, calibratedDistance, piecewiseCurves);
	xml->QueryBoolAttribute("limelight_control", &limelight_control);
	xml->QueryFloatAttribute("position", &target_position);
	xml->QueryBoolAttribute("wait", &wait_for_position);
}

/*******************************************************************************
 *
 ******************************************************************************/
void MSPosJointLimelight::init(void)
{
	Advisory::pinfo("MSPosJointLimelight init");
	parent_control->setPosition(target_position);
}

/*******************************************************************************
 *
 ******************************************************************************/
MacroStep * MSPosJointLimelight::update(void)
{
	Advisory::pinfo("MSPosJointLimelight update");

	// Fix Me. This is not really intended to work yet, just an example that compiles.

	double target_visible;
	double targetOffsetAngle_Horizontal;
	double targetOffsetAngle_Vertical;
	double targetArea;
	double targetSkew;

 	parent_control->getLimelightData(target_visible,
					targetOffsetAngle_Horizontal,
					targetOffsetAngle_Vertical,
					targetArea,
		 			targetSkew);
	distance_offset = calibratedDistance - heightDifference/tan(targetOffsetAngle_Vertical/RAD_TO_DEG);
	target_position = piecewiseCurves.evaluate(distance_offset);
	parent_control->setPosition(target_position);

/*
	if (wait_for_position)
	{
		if (parent_control->isAtTarget(target_tolerance))
		{
			return next_step;
		}
		else
		{
			return this;
		}
	}
	else
	{
 		return next_step;
	}
	*/
 	return next_step;

}
#endif